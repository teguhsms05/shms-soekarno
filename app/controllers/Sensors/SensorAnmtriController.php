<?php

class SensorAnmtriController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 3)->first();
        return View::make('page/sensor_anmtri')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showCompareSensors()
    {
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensorType = SensorType::where('id', '=', 3)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 3)->lists('code', 'table_name');
        return View::make('page/sensor_anmtri_compare')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'from' => $from,
            'to' => $to,
            'fields' => array(
                'speed' => 'Speed',
                'direction_azimuth' => 'Wind Direction',
                'direction_elevation' => 'Angle of Attack',
            )
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_anmtri_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, speed, direction_azimuth, direction_elevation'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $sensorDataZ = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            $tempSpeed = $row->speed;
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($tempSpeed)));
            array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($row->direction_azimuth)));
            array_push($sensorDataZ, array($tempDatetime, AppUtil::reformatNumber($row->direction_elevation)));
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($tempSpeed);
            $aaDataRow[2] = AppUtil::reformatNumber($row->direction_azimuth);
            $aaDataRow[3] = AppUtil::reformatNumber($row->direction_elevation);
            array_push($aaData, $aaDataRow);
        }

        $sensor = Sensor::where('table_name', '=', $id)->first();

        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold_1' => $sensor->threshold_1,
                'threshold_2' => $sensor->threshold_2,
                'threshold_3' => $sensor->threshold_3,
                'code' => 'Wind Velocity',
                'unitOfCount' => 'm/s',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => 3,
                'code' => 'Azimuth Direction',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataY,
            ),
            'z' => array(
                'mode' => 'spline',
                'color' => 'blue',
                'negativeColor' => 'blue',
                'threshold' => 3,
                'code' => 'Angle of Attack',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataZ,
            ),
            'aaData' => $aaData
        ));
    }

    public function getRealtimeData()
    {
        $anmtri['anmtri01'] = DB::table('anmtri01')->orderBy('datetime', 'desc')->first();
        $anmtri['anmtri02'] = DB::table('anmtri02')->orderBy('datetime', 'desc')->first();
        $color1 = 'rgba(0, 255, 0, 0.1)';
        $color2 = 'rgba(255, 255, 0, 0.1)';
        $color3 = 'rgba(255, 0, 0, 0.1)';
        foreach ($anmtri as $key => $i) {
            if ($anmtri[$key]) {
                $properties = Sensor::where('table_name', '=', $key)->first();
                $i->color = $color1;
                if ($i->speed < $properties->threshold_1) {
                    $i->color = $color1;
                } elseif ($i->speed >= $properties->threshold_1 && $i->speed < $properties->threshold_2) {
                    $i->color = $color2;
                } elseif ($i->speed >= $properties->threshold_3) {
                    $i->color = $color3;
                }
            }
        }
        return Response::json(array(
            'anmtri' => $anmtri
        ));
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        $idxMeasurement = 0;
        switch ($field) {
            case "speed" :
                $idxMeasurement = 0;
                break;
            case "direction_azimuth" :
                $idxMeasurement = 1;
                break;
            case 'direction_elevation' :
                $idxMeasurement = 2;
                break;
        }

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Speed',
            'unitOfCount' => 'm/s'
        );
        $measurement[1] = array(
            'code' => 'Wind Direction',
            'unitOfCount' => '°'
        );
        $measurement[2] = array(
            'code' => 'Angle of Attack',
            'unitOfCount' => '°'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[$idxMeasurement]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[$idxMeasurement]['code'] . ' (' . $measurement[$idxMeasurement]['unitOfCount'] . ') ',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {
            $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();

            $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                ->whereBetween('datetime', array($from, $to))
                ->orderBy('datetime', 'asc')->get();
            $sensorDataTemp = array();

            foreach ($sensorDataOriginal as $row) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($row->$field)));
            }

            $series = array(
                'name' => $sensorTemp->code . ' [ ' . $measurement[$idxMeasurement]['code'] . ' ]',
                'type' => 'spline',
                'data' => $sensorDataTemp,
                'tooltip' => array(
                    'valueSuffix' => ' ' . $measurement[$idxMeasurement]['unitOfCount']
                ),
                'color' => $myColor[$key]
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));
    }

}