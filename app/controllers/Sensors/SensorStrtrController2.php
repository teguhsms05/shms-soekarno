<?php

class SensorStrtrController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 7)->first();
        $deck = array('S1', 'S2', 'S6', 'S9', 'N2', 'N3', 'N8', 'N11');
        return View::make('page/sensor_strtr')->with(array(
            'sensorType' => $sensorType,
            'deck' => $deck
        ));
    }

    public function showCompareSensors()
    {
        $sensorType = SensorType::where('id', '=', 7)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 7)->lists('code', 'table_name');

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_strtr_compare')->with(array(
            'sensorType' => $sensorType,
            'sensorsStrtr' => $sensors,
            'from' => $from,
            'to' => $to,
            'tilt' => false
        ));
    }

    public function showPylonSouthView()
    {
        $sensorType = SensorType::where('id', '=', 7)->first();
        // The order depends on the div position
        $sensorsStrtr = array(
            'strtr38' => "STRTR38",
            'strtr36' => "STRTR36",
            'strtr37' => "STRTR37",
            'strtr35' => "STRTR35",
            'strtr46' => "STRTR46",
            'strtr44' => "STRTR44",
            'strtr45' => "STRTR45",
            'strtr43' => "STRTR43"
        );

        // The order depends on the div position
        $sensorsTilt = array(
            'tilt22' => 'TILT22',
            'tilt21' => 'TILT21',
            'tilt20' => 'TILT20',
            'tilt19' => 'TILT19'
        );

        $fields = array(
            'x' => 'x',
            'hires' => 'y',
            'z' => 'z'
        );

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_strtr_pylon_south_view')->with(array(
            'sensorType' => $sensorType,
            'sensorsStrtr' => $sensorsStrtr,
            'sensorsTilt' => $sensorsTilt,
            'fields' => $fields,
            'tilt' => true,
            'from' => $from,
            'to' => $to
        ));
    }

    //-------------DEV FUNCTION, BY: MUHAMMAD F. MUDJIONO. 15/04/2016---------//
    public function showPylonEastView()
    {
        $sensorType = SensorType::where('id', '=', 7)->first();
        $sensorsStrtr = Sensor::whereIn('table_name', array('strtr34', 'strtr40', 'strtr42', 'strtr48'))
            ->orderBy('code', 'asc')->lists('code','table_name');
        $sensorsTilt = Sensor::whereIn('table_name', array('tilt20','tilt22'))
            ->orderBy('code', 'asc')->lists('code','table_name');
        $fields = array(
            'x' => 'x',
            'hires' => 'y',
            'z' => 'z'
        );

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_strtr_pylon_east_view')->with(array(
            'sensorType' => $sensorType,
            'sensorsStrtr' => $sensorsStrtr,
            'sensorsTilt' => $sensorsTilt,
            'fields' => $fields,
            'from' => $from,
            'to' => $to,
            'tilt' => true
        ));
    }

    public function showPylonWestView()
    {
        $sensorType = SensorType::where('id', '=', 7)->first();
        $sensorsStrtr = Sensor::whereIn('table_name', array('strtr33', 'strtr39', 'strtr41', 'strtr47'))
            ->orderBy('code', 'asc')->lists('code','table_name');
        $sensorsTilt = Sensor::whereIn('table_name', array('tilt20','tilt22'))
            ->orderBy('code', 'asc')->lists('code','table_name');
        $fields = array(
            'x' => 'x',
            'hires' => 'y',
            'z' => 'z'
        );

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_strtr_pylon_west_view')->with(array(
            'sensorType' => $sensorType,
            'sensorsStrtr' => $sensorsStrtr,
            'sensorsTilt' => $sensorsTilt,
            'fields' => $fields,
            'tilt' => false,
            'from' => $from,
            'to' => $to
        ));
    }

    //------------------------------------------------------------------------//
    //-------------DEV FUNCTION, BY: MUHAMMAD F. MUDJIONO. 18/04/2016---------//
    //------------------------------------------------------------------------//
    public function showDeck($section)
    {
        $section = Section::where('code', '=', $section)->first();
        $sensors = Sensor::where('section_id', '=', $section->id)
            ->where('sensor_type_id', '=', 7)
            ->orderBy('code', 'asc')->get();
        $sensorType = SensorType::where('id', '=', 7)->first();
        $sensorsSelect2 = array();
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        foreach($sensors as $sensor){
            $sensorsSelect2[$sensor -> table_name] = $sensor->code;
        }
        return View::make('page/sensor_strtr_deck')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensor,
            'section' => $section,
            'sensorsStrtr' => $sensorsSelect2,
            'from' => $from,
            'to' => $to,
            'tilt' => false
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_strtr_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, value'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $aaData = array();
        $sensor = Sensor::where('table_name', '=', $id)->first();
        foreach ($sensorDataOriginal as $row) {
            $offsetRawValue = $sensor->constant_1;
            $rawValue = bcsub($row->value, $offsetRawValue, 5);
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($rawValue)));
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($rawValue);
            array_push($aaData, $aaDataRow);
        }
        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold_1' => $sensor->threshold_1,
                'threshold_2' => $sensor->threshold_2,
                'threshold_3' => $sensor->threshold_3,
                'code' => 'Strain',
                'unitOfCount' => 'μStrain',
                'timeseries' => $sensorDataX,
            ),
            'aaData' => $aaData
        ));
    }

    public function getPylonSouthViewData()
    {
        $bucket = array();
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $tmpTilt = array('19', '20', '21', '22');
        $tilt = array();
        foreach ($tmpTilt as $i) {
            $tableName = 'tilt' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                array_push($bucket, $constant->constant_4);
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->hires = bcsub($tmp->hires, $constant->constant_4, 5);
                $tmp->x = bcsub($tmp->x, $constant->constant_1, 5);
                $tmp->y = bcsub($tmp->y, $constant->constant_2, 5);
                $tmp->z = bcsub($tmp->z, $constant->constant_3, 5);
            } else {
                $tmp = array();
            }
            $tilt[$i] = $tmp;
        }
        $tmpStrtr = array('35', '37', '36', '38', '43', '45', '44', '46');
        $strtr = array();
        foreach ($tmpStrtr as $i) {
            $tableName = 'strtr' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->value = bcsub($tmp->value, $constant->constant_1, 2);
            } else {
                $tmp = array();
            }
            $strtr[$i] = $tmp;

        }
        

        return AppUtil::jsonPrettyPrint(array(
            'strtr' => $strtr,
            'tilt' => $tilt
        ));
    }

    public function getPinColor($sensor, $tableName)
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $properties = Sensor::where('table_name', '=', $tableName)->first();
        $color = $color1;
        if (isset($sensor->value)) {
            $offsetRawValue = $properties->constant_1;
            $rawValue = bcsub($sensor->value, $offsetRawValue, 5);
            if ($rawValue < $properties->threshold_1) {
                $color = $color1;
            } elseif ($rawValue >= $properties->threshold_1 && $rawValue < $properties->threshold_2) {
                $color = $color2;
            } elseif ($rawValue >= $properties->threshold_3) {
                $color = $color3;
            }
        } else {
            $offsetY = $properties->constant_4;
            $y = bcsub($sensor->hires, $offsetY, 5);
            if ($y >= $properties->threshold_4) {
                $color = $color3;
            }
        }
        return $color;
    }

    public function getPylonWestViewData()
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $tmpTilt = array('20', '22');
        $tilt = array();
        foreach ($tmpTilt as $i) {
            $tableName = 'tilt' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->hires = bcsub($tmp->hires, $constant->constant_4, 5);
                $tmp->x = bcsub($tmp->x, $constant->constant_1, 5);
                $tmp->y = bcsub($tmp->y, $constant->constant_2, 5);
                $tmp->z = bcsub($tmp->z, $constant->constant_3, 5);
            } else {
                $tmp = array();
            }
            $tilt[$i] = $tmp;
        }
        $tmpStrtr = array('33','39','41','47');
        $strtr = array();
        foreach ($tmpStrtr as $i) {
            $tableName = 'strtr' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->value = bcsub($tmp->value, $constant->constant_1, 2);
            } else {
                $tmp = array();
            }
            $strtr[$i] = $tmp;
        }
        return AppUtil::jsonPrettyPrint(array(
            'tilt' => $tilt,
            'strtr' => $strtr
        ));
    }

    public function getPylonEastViewData()
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $tmpTilt = array('20', '22');
        $tilt = array();
        foreach ($tmpTilt as $i) {
            $tableName = 'tilt' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->hires = bcsub($tmp->hires, $constant->constant_4, 5);
                $tmp->x = bcsub($tmp->x, $constant->constant_1, 5);
                $tmp->y = bcsub($tmp->y, $constant->constant_2, 5);
                $tmp->z = bcsub($tmp->z, $constant->constant_3, 5);
            } else {
                $tmp = array();
            }
            $tilt[$i] = $tmp;
        }
        $tmpStrtr = array('34', '40', '42', '48');
        $strtr = array();
        foreach ($tmpStrtr as $i) {
            $tableName = 'strtr' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->value = bcsub($tmp->value, $constant->constant_1, 2);
            } else {
                $tmp = array();
            }
            $strtr[$i] = $tmp;
        }
        return AppUtil::jsonPrettyPrint(array(
            'tilt' => $tilt,
            'strtr' => $strtr
        ));
    }

    //CREATED BY MUHAMMAD F. MUDJIONO. 25/04/2016

    public function getDeckData($section)
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $section = Section::where('code', '=', $section)->first();
        $sensors = Sensor::where('section_id', '=', $section->id)
            ->where('sensor_type_id', '=', 7)
            ->orderBy('code', 'asc')->get();
        $x = 0;
        foreach ($sensors as $i) {
            $tmp = DB::table($i->table_name)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $i->table_name);
                $tmp->code = $i->code;
                $tmp->value = bcsub($tmp->value, $i->constant_1, 5);
            } else {
                $tmp = array();
            }
            $strtr[$x] = $tmp;
            $x++;
        }
        return Response::json(array(
            'strtr' => $strtr
        ));
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Inclination',
            'unitOfCount' => '°'
        );

        $measurementStrtr = array();
        $measurementStrtr[0] = array(
            'code' => 'Strain',
            'unitOfCount' => 'μStrain'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLinesStrtr = array();

        $plotLinesStrtr[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxisStrtr = array(
            'labels' => array(
                'format' => '{value} ' . $measurementStrtr[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurementStrtr[0]['code'] . ' (' . $measurementStrtr[0]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLinesStrtr
        );


        array_push($yAxisRs, $yAxisStrtr);

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[0]['code'] . ' (' . $measurement[0]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines,
            'opposite' => 'true'
        );

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {

            if(AppUtil::removeNumeric($sensor) == 'strtr'){
                $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();
                $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, value'))
                    ->whereBetween('datetime', array($from, $to))
                    ->orderBy('datetime', 'asc')->get();
                $sensorDataTemp = array();
                foreach ($sensorDataOriginal as $sensorData) {
                    $offsetRawValue = $sensorTemp->constant_1;
                    $rawValue = bcsub($sensorData->value, $offsetRawValue, 5);
                    $tempDatetime = AppUtil::datetimeMicrotime($sensorData->my_time);
                    array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($rawValue)));
                }
                $series = array(
                    'name' => $sensorTemp->code,
                    'type' => 'spline',
                    'data' => $sensorDataTemp,
//                    'tooltip' => array(
//                        'valueSuffix' => ' ' . $measurementStrtr[0]['unitOfCount']
//                    ),
                    'yAxis' => 0
                );
                array_push($seriesRs, $series);
            }
            else{
                $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();
                $myConstant = 0;
                switch($field){
                    case "x": $myConstant = $sensorTemp->constant_1;
                        break;
                    case "y": $myConstant = $sensorTemp->constant_2;
                        break;
                    case "z": $myConstant = $sensorTemp->constant_3;
                        break;
                    case "hires": $myConstant = $sensorTemp->constant_4;
                        break;
                }
                $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                    ->whereBetween('datetime', array($from, $to))
                    ->orderBy('datetime', 'asc')->get();
                $sensorDataTemp = array();

                foreach ($sensorDataOriginal as $row){
                    $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                    array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber(bcsub($row->$field, $myConstant, 5))));
                }

                $series = array(
                    'name' => $sensorTemp->code . ' [ ' . ($field == "hires" ? "y" : $field) . ' ]',
                    'type' => 'spline',
                    'data' => $sensorDataTemp,
//                    'tooltip' => array(
//                        'valueSuffix' => ' ' . $measurement[0]['unitOfCount']
//                    ),
                    'yAxis' => 1,
                    'color' => $myColor[$key]
                    //'negativeColor' => 'lime',
                    //'threshold' => 0
                );
                array_push($seriesRs, $series);
            }
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));

    }
}