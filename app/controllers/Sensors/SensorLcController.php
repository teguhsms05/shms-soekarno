<?php

class SensorLcController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 5)->first();
        return View::make('page/sensor_lc_cable_stress')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showCableStress()
    {
        $sensorType = SensorType::where('id', '=', 5)->first();
        return View::make('page/sensor_lc')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showCompareSensors()
    {
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensorType = SensorType::where('id', '=', 5)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 5)->lists('code', 'table_name');
        return View::make('page/sensor_lc_compare')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'fields' => array(
                'value' => 'Cable Force'
            ),
            'from' => $from,
            'to' => $to
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_lc_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, value, value_mpa'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($row->value, 2)));

            $cableStress = ($row->value * 1000) / 151.9;
            array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($row->value_mpa, 2)));

            $aaDataRow[0] = $row->my_time;

            $aaDataRow[1] = AppUtil::reformatNumber($row->value, 2);
            $aaDataRow[2] = AppUtil::reformatNumber($row->value_mpa, 2);
            array_push($aaData, $aaDataRow);
        }

        $sensor = Sensor::where('table_name', '=', $id)->first();

        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold_1' => $sensor->threshold_1,
                'threshold_2' => $sensor->threshold_2,
                'threshold_3' => $sensor->threshold_3,
                'code' => 'Cable Force',
                'unitOfCount' => 'kN',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'blue',
                'negativeColor' => 'blue',
                'code' => 'Cable Stress',
                'unitOfCount' => 'MPa',
                'timeseries' => $sensorDataY,
            ),
            'aaData' => $aaData
        ));
    }

    public function getRealtimeData()
    {
        $color1_th = 'rgba(3,197,3, 1)'; //#00FF00
        $color2_th = 'rgba(255,219,0, 1)'; //#ffe700
        $color3_th = '#ff0000'; //#FF0000
        $color1 = 'rgba(3,197,3, 1)'; //#00FF00
        $color2 = 'rgba(255,219,0, 1)'; //#ffe700
        $color3 = '#ff0000'; //#FF0000

        $sensorType = SensorType::where('id', '=', 5)->first();
        $sensors = Sensor::where('sensor_type_id', '=', $sensorType->id)->get();
        $result = array(
            'graphcf_1' => array(),
            'graphcf_2' => array(),

            'graphcs_1' => array(),
            'graphcs_2' => array(),

            'aaData' => array(),

            'graphcf_1_th1' => array(),
            'graphcf_1_th2' => array(),
            'graphcf_1_th3' => array(),

            'graphcf_2_th1' => array(),
            'graphcf_2_th2' => array(),
            'graphcf_2_th3' => array(),

            'graphcs_th_1' => 0,
            'graphcs_th_2' => 0,
            'graphcs_th_3' => 0,

            'minusSymbol' => 0
        );

        $arrCable = array();
        for ($i = 11; $i >= 1; $i--) {
            $arrCable['SS' . AppUtil::addLeadingZeros($i)] = array('', '');
        }
        for ($i = 1; $i <= 11; $i++) {
            $arrCable['NS' . AppUtil::addLeadingZeros($i)] = array('', '');
        }

        $arrCable['SS03'] = array('LC05', 'LC06');
        $arrCable['SS07'] = array('LC03', 'LC04');
        $arrCable['SS10'] = array('LC01', 'LC02');
        $arrCable['NS03'] = array('LC07', 'LC08');
        $arrCable['NS07'] = array('LC09', 'LC10');
        $arrCable['NS10'] = array('LC11', 'LC12');

        $tempCF = array();
        $tempCS = array();

        foreach ($sensors as $row) {
            $sensorDataObj = new Lc($row->table_name);
            $sensorData = $sensorDataObj->orderBy('datetime', 'desc')->first();
            $tempValue = 0;
            if ($sensorData) {
                $tempValue = AppUtil::reformatNumber($sensorData->value);
            }
            $tempDatetime = AppUtil::setDefaultDatetime();
            $tempColor = $color1;

            if ($sensorData) {
                $tempDatetime = strtotime($sensorData->datetime) * 1000;
                $tempValue = $sensorData->value;
                if ($tempValue <= $row->threshold_1) {
                    $tempColor = $color1;
                }
                if ($tempValue > $row->threshold_1 && $tempValue <= $row->threshold_2) {
                    $tempColor = $color2;
                }
                if ($tempValue > $row->threshold_2) {
                    $tempColor = $color3;
                }
            }

            $tempCF[$row->code] = array(
                'sensor_id' => $row->id,
                'name' => $row->code,
                'y' => $tempValue,
                'datetime' => $tempDatetime,
                'color' => $tempColor,
                'threshold_1' => $row->threshold_1,
                'threshold_2' => $row->threshold_2,
                'threshold_3' => $row->threshold_3
            );

            $myCS = AppUtil::reformatNumber((($tempValue * 1000) / 151.92), 2);
            $tempColorCS = $color1;

            //$row->threshold_4 = 500;
            $result['graphcs_th_1'] = $row->constant_2;
            $result['graphcs_th_2'] = $row->constant_3;

            if ($myCS > $result['graphcs_th_1']) {
                $tempColorCS = $color2;
            }
            if ($myCS >= $result['graphcs_th_2'] && $myCS <= $result['graphcs_th_1']) {
                $tempColorCS = $color3;
            }

            $tempCS[$row->code] = array(
                'sensor_id' => $row->id,
                'name' => $row->code,
                'y' => $myCS,
                'datetime' => $tempDatetime,
                'color' => $tempColorCS,
                'threshold_1' => $result['graphcs_th_1'],
                'threshold_2' => $result['graphcs_th_2'],
                'threshold_3' => $result['graphcs_th_3']
            );

            $aaDataRow[0] = $row->code;
            $aaDataRow[1] = round($tempValue, 2);
            $aaDataRow[2] = round($myCS, 2);
            array_push($result['aaData'], $aaDataRow);
        }

        foreach ($arrCable as $key => $value) {

            /* Start of Odd Cable */

            $tempResultCF[0] = array(
                'sensor' => '',
                'name' => $key,
                'y' => null,
                'datetime' => '',
                'color' => '',
                'threshold_1' => '',
                'threshold_2' => '',
                'threshold_3' => ''
            );

            $tempResultCS[0] = array(
                'sensor' => '',
                'name' => $key,
                'y' => null,
                'datetime' => '',
                'color' => '',
                'threshold_1' => '',
                'threshold_2' => '',
                'threshold_3' => ''
            );

            $tempResultCFTh1[0] = array(
                'name' => $key,
                'y' => null
            );
            $tempResultCFTh2[0] = array(
                'name' => $key,
                'y' => null
            );
            $tempResultCFTh3[0] = array(
                'name' => $key,
                'y' => null
            );

            if ($arrCable[$key][0] != '') {
                $tempResultCF[0] = $tempCF[$arrCable[$key][0]];
                $tempResultCF[0]['name'] = $key;
                $tempResultCF[0]['sensor'] = $tempCF[$arrCable[$key][0]]['name'];
                $tempResultCF[0]['position'] = 'Klabat';

                $tempResultCS[0] = $tempCS[$arrCable[$key][0]];
                $tempResultCS[0]['name'] = $key;
                $tempResultCS[0]['sensor'] = $tempCS[$arrCable[$key][0]]['name'];
                $tempResultCS[0]['position'] = 'Klabat';

                $tempResultCFTh1[0] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][0]]['threshold_1'] - $result['minusSymbol'],
                    'color' => $color1_th
                );
                $tempResultCFTh2[0] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][0]]['threshold_2'] - $result['minusSymbol'],
                    'color' => $color2_th
                );
                $tempResultCFTh3[0] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][0]]['threshold_3'] - $result['minusSymbol'],
                    'color' => $color3_th
                );
            }

            /* End of Odd Cable */

            /* Start of Even Cable */

            $tempResultCF[1] = array(
                'sensor' => '',
                'name' => $key,
                'y' => null,
                'datetime' => '',
                'color' => '',
                'threshold_1' => '',
                'threshold_2' => '',
                'threshold_3' => ''
            );

            $tempResultCS[1] = array(
                'sensor' => '',
                'name' => $key,
                'y' => null,
                'datetime' => '',
                'color' => '',
                'threshold_1' => '',
                'threshold_2' => '',
                'threshold_3' => ''
            );

            $tempResultCFTh1_1[1] = array(
                'name' => $key,
                'y' => null
            );
            $tempResultCFTh2_1[1] = array(
                'name' => $key,
                'y' => null
            );
            $tempResultCFTh3_1[1] = array(
                'name' => $key,
                'y' => null
            );

            if ($arrCable[$key][1] != '') {
                $tempResultCF[1] = $tempCF[$arrCable[$key][1]];
                $tempResultCF[1]['name'] = $key;
                $tempResultCF[1]['sensor'] = $tempCF[$arrCable[$key][1]]['name'];
                $tempResultCF[1]['position'] = 'Bunaken';

                $tempResultCS[1] = $tempCS[$arrCable[$key][1]];
                $tempResultCS[1]['name'] = $key;
                $tempResultCS[1]['sensor'] = $tempCS[$arrCable[$key][1]]['name'];
                $tempResultCS[1]['position'] = 'Bunaken';

                $tempResultCFTh1_1[1] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][1]]['threshold_1'] - $result['minusSymbol'],
                    'color' => $color1_th
                );
                $tempResultCFTh2_1[1] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][1]]['threshold_2'] - $result['minusSymbol'],
                    'color' => $color2_th
                );
                $tempResultCFTh3_1[1] = array(
                    'name' => $key,
                    'y' => $tempCF[$arrCable[$key][1]]['threshold_3'] - $result['minusSymbol'],
                    'color' => $color3_th
                );
            }

            /* End of Even Cable */

            array_push($result['graphcf_1'], $tempResultCF[0]);
            array_push($result['graphcs_1'], $tempResultCS[0]);

            array_push($result['graphcf_2'], $tempResultCF[1]);
            array_push($result['graphcs_2'], $tempResultCS[1]);

            array_push($result['graphcf_2_th1'], $tempResultCFTh1_1[1]);
            array_push($result['graphcf_2_th2'], $tempResultCFTh2_1[1]);
            array_push($result['graphcf_2_th3'], $tempResultCFTh3_1[1]);

            array_push($result['graphcf_1_th1'], $tempResultCFTh1[0]);
            array_push($result['graphcf_1_th2'], $tempResultCFTh2[0]);
            array_push($result['graphcf_1_th3'], $tempResultCFTh3[0]);
        }

        return Response::json($result);
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Cable Force',
            'unitOfCount' => 'kN'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[0]['code'] . ' ' . $measurement[0]['unitOfCount'],
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {
            $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();

            $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                ->whereBetween('datetime', array($from, $to))
                ->orderBy('datetime', 'asc')->get();
            $sensorDataTemp = array();

            foreach ($sensorDataOriginal as $row) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($row->$field)));
            }

            $series = array(
                'name' => $sensorTemp->code . '[ ' . $measurement[0]['unitOfCount'] . ' ]',
                'type' => 'spline',
                'data' => $sensorDataTemp,
                'tooltip' => array(
                    'valueSuffix' => ' ' . $measurement[0]['unitOfCount']
                ),
                'color' => $myColor[$key]
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));
    }

}