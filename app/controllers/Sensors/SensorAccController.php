<?php

class SensorAccController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 1)->first();
        return View::make('page/sensor_acc')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showCompareSensors()
    {
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensorType = SensorType::where('id', '=', 1)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 1)->lists('code', 'table_name');
        return View::make('page/sensor_acc_compare')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'fields' => array(
                'x' => 'x',
                'y' => 'y',
                'z' => 'z'
            ),
            'from' => $from,
            'to' => $to
        ));
    }

    public function show($id)
    {
//        $tableName = $id . '_fft';
        $tableName = $id;
        if ($tableName == 'acc03') {
            $tableName = 'acc01';
        };

//        echo "<script>console.log('$tableName');</script>";
        $lastData = DB::table($tableName)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-24 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_acc_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    function getData($id)
    {
//        echo "<script>console.log('$id');</script>";
        /* A time to start the query */
        $from = Input::get('from');
        /* A time to end the query */
        $to = Input::get('to');

        /* All Measurements on this type of sensor */
        $measurements = array(
            array(
                'fieldName' => 'x',
                'code' => 'X Acceleration (mg)',
                'unitOfCount' => 'mg',
                'data' => array()
            ),
            array(
                'fieldName' => 'y',
                'code' => 'Y Acceleration (mg)',
                'unitOfCount' => 'mg',
                'data' => array()
            ),
            array(
                'fieldName' => 'z',
                'code' => 'Z Acceleration (mg)',
                'unitOfCount' => 'mg',
                'data' => array()
            ),
        );

        /* Color to define on Highchart Js Plugins */
        $myColor = array('', '', '', '', '', '', '', '', '', '');

        /* Y Axis on Highchart Js Plugins */
        $yAxisRs = array();

        /* Series on Highchart Js Plugins */
        $seriesRs = array();

        /* Columns on JQuery DataTables Plugins */
        $aoColumnsRs = array(
            array('sTitle' => 'Datetime'),
            array('sTitle' => $measurements[0]['code']),
            array('sTitle' => $measurements[1]['code']),
            array('sTitle' => $measurements[2]['code']),
        );

        /* Rows on Jquery Datatable Plugins */
        $aaDataRs = array();

        /*
         *
         * PlotLines to fill on Highchart Js Plugins
         * usability of plotlines is for drawing the threshold of each measurement on the chart.
         *
         * */
        $plotLines = array(
            array(
                'label' => array(
                    'text' => '',
                    'x' => 0,
                    'style' => array(
                        'color' => ''
                    )
                ),
                'color' => '',
                'width' => 0,
                'value' => '0',
                'dashStyle' => 'longdashdot'
            ),
            array(
                'label' => array(
                    'text' => '',
                    'x' => 0,
                    'style' => array(
                        'color' => ''
                    )
                ),
                'color' => '',
                'width' => 0,
                'value' => '0',
                'dashStyle' => 'longdashdot'
            ),
            array(
                'label' => array(
                    'text' => '',
                    'x' => 0,
                    'style' => array(
                        'color' => ''
                    )
                ),
                'color' => '',
                'width' => 0,
                'value' => '0',
                'dashStyle' => 'longdashdot'
            )
        );

        /*
         *
         * Y-Axis to fill on Highchart Js Plugins
         * usability of Y-Axis is for showing the unit of count of the measurement on chart.
         *
         * */
        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurements[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => 'Acceleration (mg)',
//                'text' => $measurements[0]['code'] . ' (' . $measurements[0]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        array_push($yAxisRs, $yAxis);

        /* A table name to query */
        if ($id=='acc03')
            $id='acc01';
        $tableName = $id;
//        $tableName = $id . '_fft';

        /* A table properties from 'Sensor' Model */
        $sensorTemp = Sensor::where('table_name', '=', $id)->first();

        /* Query to '$tableName' table, the result is the rows of data to show on chart and table */
        $sensorDataOriginal = DB::table($tableName)->select(DB::raw('CONCAT(datetime) as my_time, x, y, z'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        /* Manipulate each rows of the result from the query */
        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            foreach ($measurements as &$measurement) {
                array_push($measurement['data'], array($tempDatetime, AppUtil::reformatNumber($row->$measurement['fieldName'])));
            }
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($row->x);
            $aaDataRow[2] = AppUtil::reformatNumber($row->y);
            $aaDataRow[3] = AppUtil::reformatNumber($row->z);
            array_push($aaDataRs, $aaDataRow);
        }

        /* Fill the series of highchart with manipulated rows of data for each measurement settings */
        foreach ($measurements as $row) {
            $series = array(
                'name' => $row['code'],
                'type' => 'spline',
                'data' => $row['data'],
                'tooltip' => array(
                    'valueSuffix' => ' ' . $row['unitOfCount']
                )
                //'color' => $myColor[],
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);

        }

        /* return values for highchart and datatables plugins */
        return array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs,
            'aoColumns' => $aoColumnsRs,
            'aaData' => $aaDataRs
        );
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Natural Frequency',
            'unitOfCount' => 'Hz'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[0]['code'] . ' (' . $measurement[0]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {
            $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();

            $sensorDataOriginal = DB::table($sensor . '_fft')->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                ->whereBetween('datetime', array($from, $to))
                ->orderBy('datetime', 'asc')->get();
            $sensorDataTemp = array();

            foreach ($sensorDataOriginal as $row) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($row->$field)));
            }

            $series = array(
                'name' => $sensorTemp->code . ' [ ' . $measurement[0]['code'] . ' - ' . $field . ' ]',
                'type' => 'spline',
                'data' => $sensorDataTemp,
                'tooltip' => array(
                    'valueSuffix' => ' ' . $measurement[0]['unitOfCount']
                ),
                'color' => $myColor[$key]
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));
    }

}