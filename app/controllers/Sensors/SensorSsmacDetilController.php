<?php

class SensorSsmacDetilController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 6)->first();
        return View::make('page/sensor_ssmac')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id."_header")->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/ssmac_detail2')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id."_header")->select(DB::raw('CONCAT(datetime) as my_time, x, y, z, id'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $sensorDataZ = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            //if ($row->x || $row->y || $row->z) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($row->x, 8)));
            array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($row->y, 8)));
            array_push($sensorDataZ, array($tempDatetime, AppUtil::reformatNumber($row->z, 8)));
                $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($row->x, 8);
            $aaDataRow[2] = AppUtil::reformatNumber($row->y, 8);
            $aaDataRow[3] = AppUtil::reformatNumber($row->z, 8);
            $aaDataRow[4] = "<a href=".URL::to('sensor/ssmac-detil/ssmac01').">Detil</a>";//'<a href="'{{URL::to("sensor/ssmac01-detail/")}}.$row->my_time.'">Detil</a>';
                array_push($aaData, $aaDataRow);
            //}
        }

        $sensor = Sensor::where('table_name', '=', $id)->first();

        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold' => $sensor->threshold_1,
                'code' => 'X-Axes Acceleration',
                'unitOfCount' => 'g',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => $sensor->threshold_2,
                'code' => 'Y-Axes Acceleration',
                'unitOfCount' => 'g',
                'timeseries' => $sensorDataY,
            ),
            'z' => array(
                'mode' => 'spline',
                'color' => 'blue',
                'negativeColor' => 'blue',
                'threshold' => $sensor->threshold_3,
                'code' => 'Z-Axes Acceleration',
                'unitOfCount' => 'g',
                'timeseries' => $sensorDataZ,
            ),
            'aaData' => $aaData
        ));
    }

}