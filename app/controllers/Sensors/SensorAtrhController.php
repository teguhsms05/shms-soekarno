<?php

class SensorAtrhController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 4)->first();
        return View::make('page/sensor_atrh')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showCompareSensors()
    {
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensorType = SensorType::where('id', '=', 4)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 4)->lists('code', 'table_name');
        return View::make('page/sensor_atrh_compare')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'fields' => array(
                'temperature' => 'Temperature',
                'humidity' => 'Humidity',
            ),
            'from' => $from,
            'to' => $to
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_atrh_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, temperature, humidity'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            //if ($row->temperature || $row->humidity) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($row->temperature)));
                array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($row->humidity)));
                $aaDataRow[0] = $row->my_time;
                $aaDataRow[1] = AppUtil::reformatNumber($row->temperature);
                $aaDataRow[2] = AppUtil::reformatNumber($row->humidity);
                array_push($aaData, $aaDataRow);
            //}
        }

        $sensor = Sensor::where('table_name', '=', $id)->first();

        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold' => $sensor->threshold_1,
                'code' => 'Temperature',
                'unitOfCount' => '°C',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => $sensor->threshold_2,
                'code' => 'Humidity',
                'unitOfCount' => '%',
                'timeseries' => $sensorDataY,
            ),
            'aaData' => $aaData
        ));
    }

    public function getRealtimeData()
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $atrh = array();
        for ($i = 1; $i <= 4; $i++) {
            $tableName = 'atrh0' . $i;
            $temp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($temp) {
                $properties = Sensor::where('table_name', '=', $tableName)->first();
                $temp->color = $color1;
                if ($temp->temperature >= $properties->threshold_1) {
                    $temp->color = $color3;
                }
                array_push($atrh, $temp);
            }
        }
        return Response::json(array(
            'atrh' => $atrh
        ));
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        $idxMeasurement = 0;
        switch ($field) {
            case "temperature" :
                $idxMeasurement = 0;
                break;
            case "humidity" :
                $idxMeasurement = 1;
                break;
        }

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Temperature',
            'unitOfCount' => '°C'
        );
        $measurement[1] = array(
            'code' => 'Humidity',
            'unitOfCount' => '%'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[$idxMeasurement]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[$idxMeasurement]['code'] . ' (' . $measurement[$idxMeasurement]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        //return Response::json($yAxis);

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {
            $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();

            $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                ->whereBetween('datetime', array($from, $to))
                ->orderBy('datetime', 'asc')->get();
            $sensorDataTemp = array();

            foreach ($sensorDataOriginal as $row) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($row->$field)));
            }

            $series = array(
                'name' => $sensorTemp->code . '[ ' . $measurement[$idxMeasurement]['code'] . ' ]',
                'type' => 'spline',
                'data' => $sensorDataTemp,
                'tooltip' => array(
                    'valueSuffix' => ' ' . $measurement[$idxMeasurement]['unitOfCount']
                ),
                'color' => $myColor[$key]
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));
    }

}