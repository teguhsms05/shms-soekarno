<?php

class SensorTiltController extends BaseController
{
    public function index()
    {
        $sensorType = SensorType::where('id', '=', 9)->first();
        $deck = array('S2', 'S4', 'S8', 'N1', 'N3', 'N6', 'N10');
        $pier = array('N12', 'S10');
        $bridgeSide = array('west', 'east');
        return View::make('page/sensor_tilt')->with(array(
            'sensorType' => $sensorType,
            'deck' => $deck,
            'pier' => $pier,
            'bridgeSide' => $bridgeSide
        ));
    }

    public function showDeck($section)
    {
        $section = Section::where('code', '=', $section)->first();
        $sensors = Sensor::where('section_id', '=', $section->id)
            ->where('sensor_type_id', '=', 9)
            ->orderBy('code', 'asc')->get();
        $sensorType = SensorType::where('id', '=', 9)->first();
        $sensorsSelect2 = array();
        foreach ($sensors as $sensor) {
            $sensorsSelect2[$sensor->table_name] = $sensor->code;
        }

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_tilt_deck')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'section' => $section,
            'from' => $from,
            'to' => $to,
            'sensorsSelect2' => $sensorsSelect2,
            'fields' => array(
                'hires' => 'y',
                'x' => 'x',
                'z' => 'z'
            )
        ));
    }

    public function getSectionRealtimeData($section)
    {
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $section = Section::where('code', '=', $section)->first();
        $sensors = Sensor::where('section_id', '=', $section->id)
            ->where('sensor_type_id', '=', 9)
            ->orderBy('code', 'asc')->get();
        $x = 0;
        foreach ($sensors as $i) {
            $tmp = DB::table($i->table_name)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $i->table_name);
                $tmp->code = $i->code;
                $tilt[$x] = $tmp;
            } else {
                $tilt[$x] = array();
            }
            $x++;
        }
        return Response::json(array(
            'tilt' => $tilt
        ));
    }

    public function getPinColor($sensor, $tableName)
    {
        $color1 = 'lime';
        $color3 = '#ff0000';
        $properties = Sensor::where('table_name', '=', $tableName)->first();
        $color = $color1;
        $offsetY = $properties->constant_4;
        $y = bcsub($sensor->hires, $offsetY, 5);
        if ($y >= $properties->threshold_4) {
            $color = $color3;
        }
        return $color;
    }

    public function showPier($section)
    {
        $section = Section::where('code', '=', $section)->first();
        $sensors = Sensor::where('section_id', '=', $section->id)
            ->where('sensor_type_id', '=', 9)
            ->orderBy('code', 'asc')->get();
        $sensorType = SensorType::where('id', '=', 9)->first();
        $sensorsSelect2 = array();
        foreach ($sensors as $sensor) {
            $sensorsSelect2[$sensor->table_name] = $sensor->code;
        }

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_tilt_pier')->with(array(
            'sensorType' => $sensorType,
            'sensors' => $sensors,
            'section' => $section,
            'from' => $from,
            'to' => $to,
            'sensorsSelect2' => $sensorsSelect2,
            'fields' => array(
                'hires' => 'y',
                'x' => 'x',
                'z' => 'z'
            )
        ));
    }

    public function showPylon()
    {
        $sensorType = SensorType::where('id', '=', 9)->first();

        // The order depends on the div position
        $sensorsTilt = array(
            'tilt22' => 'TILT22',
            'tilt21' => 'TILT21',
            'tilt20' => 'TILT20',
            'tilt19' => 'TILT19'
        );

        $fields = array(
            'x' => 'x',
            'hires' => 'y',
            'z' => 'z'
        );

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_tilt_pylon')->with(array(
            'sensorType' => $sensorType,
            'sensorsSelect2' => $sensorsTilt,
            'fields' => $fields,
            'from' => $from,
            'to' => $to
        ));
    }

    public function showBridgeSideView($side)
    {
        $sensorType = SensorType::where('id', '=', 9)->first();
        switch($side){
            case 'east':
                $sensorsTilt = Sensor::whereIn('table_name', array('tilt01','tilt03','tilt05','tilt07','tilt09','tilt11','tilt13','tilt15','tilt17'))
                    ->orderBy('code', 'asc')->lists('code','table_name');
                break;
            case 'west':
                $sensorsTilt = Sensor::whereIn('table_name', array('tilt02','tilt04','tilt06','tilt08','tilt10','tilt12','tilt14','tilt16','tilt18'))
                    ->orderBy('code', 'asc')->lists('code','table_name');
        }

        $fields = array(
            'hires' => 'y',
            'x' => 'x',
            'z' => 'z'
        );

        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/sensor_tilt_'.$side.'_side')->with(array(
            'sensorType' => $sensorType,
            'sensorsSelect2' => $sensorsTilt,
            'fields' => $fields,
            'side' => $side,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getBridgeSideViewData($side)
    {
        $tmpTilt = array();
        switch ($side) {
            case "east" :
                $tmpTilt = array('01', '03', '05', '07', '09', '11', '13', '15', '17');
                break;
            case "west" :
                $tmpTilt = array('02', '04', '06', '08', '10', '12', '14', '16', '18');
                break;
        }
        $tilt = array();
        foreach ($tmpTilt as $key=>$i) {
            $tableName = 'tilt' . $i;
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tilt[$tableName] = $tmp;
            } else {
                $tilt[$tableName] = array(1, 2, 3);
            }
        }
//        return Response::json(array(
//            'tilt' => $tilt
//        ));

        return AppUtil::jsonPrettyPrint(array(
            'tilt' => $tilt
        ));
    }

    public function showDisplacementBackup()
    {
        $sensorType = SensorType::where('id', '=', 9)->first();
        return View::make('page/sensor_tilt_displacement_detail')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function showDisplacement()
    {
        $lastData = DB::table('displacement')->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Displacement::first();
        return View::make('page/sensor_tilt_displacement_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_tilt_inclination_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, x, hires, z'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $sensorDataZ = array();
        $aaData = array();
        $sensor = Sensor::where('table_name', '=', $id)->first();
        foreach ($sensorDataOriginal as $row) {
            $x = bcsub($row->x, $sensor->constant_1, 2);
            $y = bcsub($row->hires, $sensor->constant_4, 5);
            $z = bcsub($row->z, $sensor->constant_3, 2);
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($x)));
            array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($y, 4)));
            array_push($sensorDataZ, array($tempDatetime, AppUtil::reformatNumber($z)));
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($x);
            $aaDataRow[2] = AppUtil::reformatNumber($y, 5);
            $aaDataRow[3] = AppUtil::reformatNumber($z);
            array_push($aaData, $aaDataRow);
        }
        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold' => $sensor->threshold_1,
                'code' => 'Inclination X',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => $sensor->threshold_2,
                'code' => 'Inclination Y',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataY,
            ),
            'z' => array(
                'mode' => 'spline',
                'color' => 'blue',
                'negativeColor' => 'blue',
                'threshold' => $sensor->threshold_3,
                'code' => 'Inclination Z',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataZ,
            ),
            'aaData' => $aaData
        ));
    }
    public function getPylonData()
    {
        $bucket = array();
        $color1 = 'lime';
        $color2 = 'rgba(255,219,0, 1)';
        $color3 = '#ff0000';
        $tmpTilt = array('19', '20', '21', '22');
        $tilt = array();
        foreach ($tmpTilt as $i) {
            $tableName = 'tilt' . $i;
            $constant = DB::table('sensor')->where('table_name', '=', $tableName)->first();
            $tmp = DB::table($tableName)->orderBy('datetime', 'desc')->first();
            if ($tmp) {
                array_push($bucket, $constant->constant_4);
                $tmp->color = $this->getPinColor($tmp, $tableName);
                $tmp->hires = bcsub($tmp->hires, $constant->constant_4, 5);
                $tmp->x = bcsub($tmp->x, $constant->constant_1, 5);
                $tmp->y = bcsub($tmp->y, $constant->constant_2, 5);
                $tmp->z = bcsub($tmp->z, $constant->constant_3, 5);
            } else {
                $tmp = array();
            }
            $tilt[$i] = $tmp;
        }
        return AppUtil::jsonPrettyPrint(array(
            'tilt' => $tilt
        ));
    }

    public function getDisplacementData()
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table('displacement')->select(DB::raw('CONCAT(datetime) as my_time,
                tilt03,
                tilt04,
                tilt05,
                tilt06,
                tilt07,
                tilt08,
                tilt09,
                tilt10,
                tilt11,
                tilt12,
                tilt13,
                tilt14,
                tilt15,
                tilt16
            '))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($row->tilt03);
            $aaDataRow[2] = AppUtil::reformatNumber($row->tilt04);
            $aaDataRow[3] = AppUtil::reformatNumber($row->tilt05);
            $aaDataRow[4] = AppUtil::reformatNumber($row->tilt06);
            $aaDataRow[5] = AppUtil::reformatNumber($row->tilt07);
            $aaDataRow[6] = AppUtil::reformatNumber($row->tilt08);
            $aaDataRow[7] = AppUtil::reformatNumber($row->tilt09);
            $aaDataRow[8] = AppUtil::reformatNumber($row->tilt10);
            $aaDataRow[9] = AppUtil::reformatNumber($row->tilt11);
            $aaDataRow[10] = AppUtil::reformatNumber($row->tilt12);
            $aaDataRow[11] = AppUtil::reformatNumber($row->tilt13);
            $aaDataRow[12] = AppUtil::reformatNumber($row->tilt14);
            $aaDataRow[13] = AppUtil::reformatNumber($row->tilt15);
            $aaDataRow[14] = AppUtil::reformatNumber($row->tilt16);
            array_push($aaData, $aaDataRow);
        }

        return Response::json(array(
            'aaData' => $aaData
        ));

    }

    public function getDisplacementLastData()
    {
        $result = array(
            'even' => array(),
            'odd' => array()
        );

        $displacement = Displacement::orderBy('datetime', 'desc')->first();

        for ($i = 3; $i <= 16; $i++) {
            $field = 'tilt' . AppUtil::addLeadingZeros($i);
            $y = 0;
            $temp = array(
                'name' => 'Tilt' . AppUtil::addLeadingZeros($i),
                'y' => $y,
                'datetime' => "0000-00-00 00-00-00"
            );
            if ($displacement) {
                if ($displacement->$field != null) {
                    $temp['y'] = $displacement->$field;
                    $temp['datetime'] = $displacement->datetime;
                }
            }
            if ($i % 2 == 0) {
                //insert to even
                $temp['color'] = 'orange';
                array_push($result['even'], $temp);
            } else {
                //insert to odd
                $temp['color'] = 'green';
                array_push($result['odd'], $temp);
            }
        }

        return Response::json($result);
    }

    public function showCompareSensors()
    {
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        $sensorType = SensorType::where('id', '=', 9)->first();
        $sensors = Sensor::where('sensor_type_id', '=', 9)->lists('code', 'table_name');
        return View::make('page/sensor_tilt_compare')->with(array(
            'sensorType' => $sensorType,
            'sensorsSelect2' => $sensors,
            'from' => $from,
            'to' => $to,
            'fields' => array(
                'hires' => 'y',
                'x' => 'x',
                'z' => 'z'
            )
        ));
    }

    public function getCompareSensorsData()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $sensors = Input::get('sensors');
        $field = Input::get('field');

        if (!$sensors) {
            return Response::json(false);
        }

        $measurement = array();
        $measurement[0] = array(
            'code' => 'Inclination',
            'unitOfCount' => '°'
        );

        $myColor = array('#1000FF', 'black', '#FF0006', '#FFB40B', '#2FFF00', '#ff8d92', '#77ffe0', '#FF852B', '#89F5FF', '#414141','#25e398','#3d70d8','#569128','#ebf887','#ebf887','#9c0704','#a46905','#25015e','#958ed4','#78a71d','#b3bd8a','#81ccb2','#aef530','#d9428d','#f0aea3','#3a2e24','#ceb685','#6f111c','#92f334','#9a1681','#ef2674','#544716','#ace2ab','#700e05','#78aa2d','#094a4f');

        $yAxisRs = array();
        $seriesRs = array();

        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[0]['code'] . ' ' . $measurement[0]['unitOfCount'],
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines
        );

        array_push($yAxisRs, $yAxis);

        foreach ($sensors as $key => $sensor) {
            $sensorTemp = Sensor::where('table_name', '=', $sensor)->first();

            $sensorDataOriginal = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, ' . $field))
                ->whereBetween('datetime', array($from, $to))
                ->orderBy('datetime', 'asc')->get();
            $sensorDataTemp = array();

            foreach ($sensorDataOriginal as $row) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                $myConstant = 0;
                switch($field){
                    case "x": $myConstant = $sensorTemp->constant_1;
                        break;
                    case "y": $myConstant = $sensorTemp->constant_2;
                        break;
                    case "z": $myConstant = $sensorTemp->constant_3;
                        break;
                    case "hires": $myConstant = $sensorTemp->constant_4;
                        break;
                }
                $myField = bcsub($row->$field, $myConstant, 5);
                array_push($sensorDataTemp, array($tempDatetime, AppUtil::reformatNumber($myField)));
            }
            $fieldName = $field;
            if($field == 'hires'){
                $fieldName = 'y';
            }
            $series = array(
                'name' => $sensorTemp->code . ' [ ' . $fieldName . ' ]',
                'type' => 'spline',
                'data' => $sensorDataTemp,
                'tooltip' => array(
                    'valueSuffix' => ' ' . $measurement[0]['unitOfCount']
                ),
                'color' => $myColor[$key]
                //'negativeColor' => 'lime',
                //'threshold' => 0
            );
            array_push($seriesRs, $series);
        }

        return AppUtil::jsonPrettyPrint(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));
    }

}