<?php

class SensorAnmbiController extends BaseController
{

    public function index()
    {
        $sensorType = SensorType::where('id', '=', 2)->first();
        return View::make('page/sensor_anmbi')->with(array(
            'sensorType' => $sensorType
        ));
    }

    public function show($id)
    {
        $lastData = DB::table($id)->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $sensor = Sensor::where('code', '=', $id)->first();
        return View::make('page/sensor_anmbi_detail')->with(array(
            'sensor' => $sensor,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData($id)
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table($id)->select(DB::raw('CONCAT(datetime) as my_time, speed, direction_azimuth'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $sensorDataY = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            $tempSpeed = $row->speed;
            array_push($sensorDataX, array($tempDatetime, AppUtil::reformatNumber($tempSpeed)));// m/s to m/s
            array_push($sensorDataY, array($tempDatetime, AppUtil::reformatNumber($row->direction_azimuth)));
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = AppUtil::reformatNumber($tempSpeed);
            $aaDataRow[2] = AppUtil::reformatNumber($row->direction_azimuth);
            array_push($aaData, $aaDataRow);
        }

        $sensor = Sensor::where('table_name', '=', $id)->first();

        return Response::json(array(
            'x' => array(
                'mode' => 'spline',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold_1' => $sensor->threshold_1,
                'threshold_2' => $sensor->threshold_2,
                'threshold_3' => $sensor->threshold_3,
                'code' => 'Wind Velocity',
                'unitOfCount' => 'm/s',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'spline',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => 180,
                'code' => 'Azimuth Direction',
                'unitOfCount' => '°',
                'timeseries' => $sensorDataY,

            ),
            'aaData' => $aaData
        ));
    }

    public function getRealtimeData()
    {
        $anmbi01 = DB::table('anmbi01')->orderBy('datetime', 'desc')->first();
        if ($anmbi01) {
            $color1 = 'rgba(0, 255, 0, 0.1)';
            $color2 = 'rgba(255, 255, 0, 0.1)';
            $color3 = 'rgba(255, 0, 0, 0.1)';
            $properties = Sensor::where('table_name', '=', 'anmbi01')->first();
            if ($anmbi01->speed < $properties->threshold_1) {
                $anmbi01->color = $color1;
            } elseif ($anmbi01->speed >= $properties->threshold_1 && $anmbi01->speed < $properties->threshold_2) {
                $anmbi01->color = $color2;
            } elseif ($anmbi01->speed >= $properties->threshold_3) {
                $anmbi01->color = $color3;
            }
            return Response::json(array(
                'anmbi01' => $anmbi01
            ));
        }
        return Response::json(array());
    }

}