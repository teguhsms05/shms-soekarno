<?php

class UpsController extends BaseController
{

    public function index()
    {
        $lastData = DB::table('ups')->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        return View::make('page/ups')->with(array(
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData()
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table('ups')->select(DB::raw('CONCAT(datetime) as my_time,
            internal_temperature,
            input_voltage,
            output_voltage,
            input_frequency,
            battery_capacity,
            battery_voltage,
            runtime_remaining
        '))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();
        $sensorDataX = array();
        $sensorDataY = array();
        $sensorDataZ = array();
        $sensorDataA = array();
        $sensorDataB = array();
        $sensorDataC = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
            array_push($sensorDataX, array($tempDatetime, $row->internal_temperature));
            array_push($sensorDataY, array($tempDatetime, $row->input_voltage));
            array_push($sensorDataZ, array($tempDatetime, $row->output_voltage));
            array_push($sensorDataA, array($tempDatetime, $row->input_frequency));
            array_push($sensorDataB, array($tempDatetime, $row->battery_capacity));
            array_push($sensorDataC, array($tempDatetime, $row->battery_voltage));
            $aaDataRow[0] = $row->my_time;
            $aaDataRow[1] = $row->internal_temperature;
            $aaDataRow[2] = $row->input_voltage;
            $aaDataRow[3] = $row->output_voltage;
            $aaDataRow[4] = $row->input_frequency;
            $aaDataRow[5] = $row->battery_capacity;
            $aaDataRow[6] = $row->battery_voltage;
            $aaDataRow[7] = $row->runtime_remaining;
            array_push($aaData, $aaDataRow);
        }

        return Response::json(array(
            'x' => array(
                'mode' => 'line',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold' => 3,
                'code' => 'Internal Temperature',
                'unitOfCount' => '°C',
                'timeseries' => $sensorDataX,
            ),
            'y' => array(
                'mode' => 'line',
                'color' => 'green',
                'negativeColor' => 'green',
                'threshold' => 3,
                'code' => 'Input Voltage',
                'unitOfCount' => 'V',
                'timeseries' => $sensorDataY,
            ),
            'z' => array(
                'mode' => 'line',
                'color' => 'brown',
                'negativeColor' => 'brown',
                'threshold' => 3,
                'code' => 'Output Voltage',
                'unitOfCount' => 'V',
                'timeseries' => $sensorDataZ,
            ),
            'a' => array(
                'mode' => 'line',
                'color' => 'orange',
                'negativeColor' => 'orange',
                'threshold' => 3,
                'code' => 'Input Frequency',
                'unitOfCount' => 'Hz',
                'timeseries' => $sensorDataA,
            ),
            'b' => array(
                'mode' => 'line',
                'color' => 'blue',
                'negativeColor' => 'blue',
                'threshold' => 3,
                'code' => 'Battery Capacity',
                'unitOfCount' => '%',
                'timeseries' => $sensorDataB,
            ),
            'c' => array(
                'mode' => 'line',
                'color' => 'lime',
                'negativeColor' => 'lime',
                'threshold' => 3,
                'code' => 'Battery Voltage',
                'unitOfCount' => 'V',
                'timeseries' => $sensorDataC,
            ),
            'aaData' => $aaData
        ));
    }

}