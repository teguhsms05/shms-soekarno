<?php

class PowerSourceController extends BaseController
{

    public function index()
    {
        $lastData = DB::table('plngenset')->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        $entity = array(
            'code' => 'Power Source',
            'desc' => ''
        );
        return View::make('page/power_source')->with(array(
            'entity' => $entity,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData()
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table('plngenset')->select(DB::raw('CONCAT(datetime) as my_time, type, status'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $aaData = array();
        $totalData = 0;

        foreach ($sensorDataOriginal as $row) {
            if ($row->type || $row->status) {
                $aaDataRow[0] = $row->my_time;
                $aaDataRow[1] = $row->type;
                $aaDataRow[2] = $row->status;
                array_push($aaData, $aaDataRow);
            }
            $totalData++;
        }

        return Response::json(array(
            'aaData' => $aaData,
            'totalData' => $totalData
        ));
    }

    public function getRealtimeData()
    {
        $genset = Plngenset::where('type', '=', 'GENSET')->orderBy('datetime', 'desc')->first();
        $pln = Plngenset::where('type', '=', 'PLN')->orderBy('datetime', 'desc')->first();
        $totalData = DB::table('plngenset')->count();

        return Response::json(array(
            'genset' => $genset,
            'pln' => $pln,
            'totalData' => $totalData
        ));
    }

}