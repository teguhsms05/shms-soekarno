<?php

class RacktempController extends BaseController
{

    public function index()
    {
        $lastData = DB::table('racktemp')->select('datetime')->orderBy('datetime', 'desc')->first();
        $datetime = $lastData ? $lastData->datetime : date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');
        return View::make('page/racktemp')->with(array(
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData()
    {
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorDataOriginal = DB::table('racktemp')->select(DB::raw('CONCAT(datetime) as my_time, value'))
            ->whereBetween('datetime', array($from, $to))
            ->orderBy('datetime', 'asc')->get();

        $sensorDataX = array();
        $aaData = array();

        foreach ($sensorDataOriginal as $row) {
            if ($row->value) {
                $tempDatetime = AppUtil::datetimeMicrotime($row->my_time);
                array_push($sensorDataX, array($tempDatetime, $row->value));
                $aaDataRow[0] = $row->my_time;
                $aaDataRow[1] = $row->value;
                array_push($aaData, $aaDataRow);
            }
        }

        return Response::json(array(
            'x' => array(
                'mode' => 'line',
                'color' => 'red',
                'negativeColor' => 'red',
                'threshold' => 3,
                'code' => 'Temperature',
                'unitOfCount' => '°C',
                'timeseries' => $sensorDataX,
            ),
            'aaData' => $aaData
        ));
    }

}