<?php

class CompareSensorsController extends BaseController{
    public function index(){
        $anmbi = Sensor::where('sensor_type_id', '=', 2)
            ->lists('code','table_name');
        $anmbiFields = array(
            'speed' => 'Wind Speed',
            'direction_azimuth' => 'Wind Direction'
        );

        $anmtri = Sensor::where('sensor_type_id', '=', 3)
            ->lists('code','table_name');
        $anmtriFields = array(
            'speed' => 'Wind Speed',
            'direction_azimuth' => 'Wind Direction',
            'direction_elevation' => 'Angle of Attack'
        );

        $atrh = Sensor::where('sensor_type_id', '=', 4)
            ->lists('code','table_name');
        $atrhFields = array(
            'temperature' => 'Temperature',
            'humidity' => 'Humidity'
        );

        $lc = Sensor::where('sensor_type_id', '=', 5)
            ->lists('code','table_name');
        $lcFields = array(
            'raw_value' => 'Cable Force',
            'value' => 'Cable Stress'
        );

        $strtr = Sensor::where('sensor_type_id', '=', 7)
            ->lists('code','table_name');

        $temp = Sensor::where('sensor_type_id', '=', 8)
            ->lists('code', 'table_name');

        $tilt = Sensor::where('sensor_type_id', '=', 9)
            ->lists('code','table_name');
        $tiltFields = array(
            'hires'=>'y',
            'x' => 'x',
            'z'=>'z'
        );

        $sensorType = SensorType::where('id', '=', 7)->first();
        $datetime = date('Y-m-d H:i:s');
        $now = new DateTime($datetime);
        $now->modify('+1 second');
        $oneHourAgo = clone $now;
        $oneHourAgo = $oneHourAgo->modify('-1 hour');
        $from = Input::get('from') ? Input::get('from') : $oneHourAgo->format('Y-m-d H:i:s');
        $to = Input::get('to') ? Input::get('to') : $now->format('Y-m-d H:i:s');

        return View::make('page/compare_sensors_all')->with(array(
            'sensorType' => $sensorType,
            'anmbi' => $anmbi,
            'anmbiFields' => $anmbiFields,
            'anmtri' => $anmtri,
            'anmtriFields' => $anmtriFields,
            'atrh' => $atrh,
            'atrhFields' => $atrhFields,
            'lc' => $lc,
            'lcFields' => $lcFields,
            'strtr' => $strtr,
            'tilt' => $tilt,
            'tiltFields' => $tiltFields,
            'temp' => $temp,
            'from' => $from,
            'to' => $to
        ));
    }

    public function getData(){
        $from = Input::get('from');
        $to = Input::get('to');

        $sensorsRs = array();
        $yAxisRs = array();
        $seriesRs = array();

        $measurements = array(
            'speed' => array(
                'code' => 'Wind Speed',
                'unitOfCount' => 'm/s',
                'idx' => false
            ),
            'direction_azimuth' => array(
                'code' => 'Wind Direction',
                'unitOfCount' => '°',
                'idx' => false
            ),
            'direction_elevation' => array(
                'code' => 'Angle of Attack',
                'unitOfCount' => '°',
                'idx' => false
            ),
            'temperature' => array(
                'code' => 'Temperature',
                'unitOfCount' => '°C',
                'idx' => false
            ),
            'humidity' => array(
                'code' => 'Humidity',
                'unitOfCount' => '%',
                'idx' => false
            ),
            'cable_force' => array(
                'code' => 'Cable Force',
                'unitOfCount' => 'kN',
                'idx' => false
            ),
            'cable_stress' => array(
                'code' => 'Cable Stress',
                'unitOfCount' => 'MPa',
                'idx' => false
            ),
            'strain' => array(
                'code' => 'Strain',
                'unitOfCount' => 'μStrain',
                'idx' => false
            ),
            'inclination_x' => array(
                'code' => 'Inclination X',
                'unitOfCount' => '°',
                'idx' => false
            ),
            'inclination_y' => array(
                'code' => 'Inclination Y',
                'unitOfCount' => '°',
                'idx' => false
            ),
            'inclination_z' => array(
                'code' => 'Inclination Z',
                'unitOfCount' => '°',
                'idx' => false
            )


        );

        $sensorTypes = array(
            'anmbi' => array(
                'sensors' => Input::get('anmbi'),
                'fields' => Input::get('anmbiFields'),
                'fieldsList' => array(
                    'speed' => array(
                        'measurement' => 'speed',
                    ),
                    'direction_azimuth' => array(
                        'measurement' => 'direction_azimuth',
                    )
                )
            ),
            'anmtri' => array(
                'sensors' => Input::get('anmtri'),
                'fields' => Input::get('anmtriFields'),
                'fieldsList' => array(
                    'speed' => array(
                        'measurement' => 'speed',
                    ),
                    'direction_azimuth' => array(
                        'measurement' => 'direction_azimuth',
                    ),
                    'direction_elevation' => array(
                        'measurement' => 'direction_elevation',
                    )
                )
            ),
            'atrh' => array(
                'sensors' => Input::get('atrh'),
                'fields' => Input::get('atrhFields'),
                'fieldsList' => array(
                    'temperature' => array(
                        'measurement' => 'temperature',
                    ),
                    'humidity' => array(
                        'measurement' => 'humidity',
                    )
                )
            ),
            'lc' => array(
                'sensors' => Input::get('lc'),
                'fields' => Input::get('lcFields'),
                'fieldsList' => array(
                    'raw_value' => array(
                        'measurement' => 'cable_force',
                        'offset' => function ($value, $properties) {
                            return $value + $properties->constant_1;
                        }
                    ),
                    'value' => array(
                        'measurement' => 'cable_stress',
                        'offset' => function ($value, $properties) {
                            return $value + $properties->constant_2;
                        }
                    )
                )
            ),
            'strtr' => array(
                'sensors' => Input::get('strtr'),
                'fields' => array('value'),
                'fieldsList' => array(
                    'value' => array(
                        'measurement' => 'strain',
                        'offset' => function ($value, $properties) {
                            return (double)bcsub($value, $properties->constant_1, 5);
                        }
                    )
                )
            ),
            'temp' => array(
                'sensors' => Input::get('temp'),
                'fields' => array('temperature'),
                'fieldsList' => array(
                    'temperature' => array(
                        'measurement' => 'temperature',
                    )
                )
            ),
            'tilt' => array(
                'sensors' => Input::get('tilt'),
                'fields' => Input::get('tiltFields'),
                'fieldsList' => array(
                    'x' => array(
                        'measurement' => 'inclination_x',
                        'offset' => function ($value, $properties) {
                            return (double)bcsub($value, $properties->constant_1, 2);
                        }
                    ),
                    'y' => array(
                        'measurement' => 'inclination_y',
                        'offset' => function ($value, $properties) {
                            return (double)bcsub($value, $properties->constant_2, 2);
                        }
                    ),
                    'z' => array(
                        'measurement' => 'inclination_z',
                        'offset' => function ($value, $properties) {
                            return (double)bcsub($value, $properties->constant_3, 2);
                        }
                    ),
                    'hires' => array(
                        'measurement' => 'inclination_y',
                        'offset' => function ($value, $properties) {
                            return (double)bcsub($value, $properties->constant_4, 5);
                        }
                    ),

                )

            )
        );

        $array = array();

        // Loop for the sensor type
        foreach ($sensorTypes as $sensorType => $value) {
            // Continue loop to another sensor type when no sensor selected
            if (!$value['sensors'] || !$value['fields']) {
                continue;
            }
            array_push($array, $sensorType);
            foreach ($value['fields'] as $fieldNameSelected) {
                // Loop for the fields on selected sensor type
                foreach ($value['fieldsList'] as $fieldName => $prop) {
                    // Continue if the selected field(s) and sensor type's not match
                    if ($fieldNameSelected != $fieldName) {
                        continue;
                    }
                    // Push measurement properties to yAxisRs
                    $yAxis = array(
                        'labels' => array(
                            'format' => '{value} ' . $measurements[$prop['measurement']]['unitOfCount'],
                            'style' => array(
                                'color' => 'black'
                            )
                        ),
                        'title' => array(
                            'text' => $measurements[$prop['measurement']]['code'] . ' ' . $measurements[$prop['measurement']]['unitOfCount'],
                            'align' => 'middle',
                            'style' => array(
                                'color' => 'black'
                            )
                        )
                    );
                    $validation = $this->inArray($yAxis,$yAxisRs);
                    if($validation['exist'] == false){
                        array_push($yAxisRs, $yAxis);
                        $measurements[$prop['measurement']]['idx'] = count($yAxisRs) - 1;
                        $validation['idx'] = $measurements[$prop['measurement']]['idx'];
                    }

                    foreach($value['sensors'] as $sensor){
                        $propSensor = DB::table('sensor')
                            ->where('table_name','=',$sensor)
                            ->first();
                        $sensorData = DB::table($sensor)->select(DB::raw('CONCAT(datetime) as my_time, '.$fieldName))
                            ->whereBetween('datetime', array($from, $to))
                            ->orderBy('datetime', 'asc')->get();
                        if (!$sensorData) {
                            continue;
                        }
                        $sensorDataTemp = array();
                        foreach ($sensorData as $item) {
//                            $offsetRawValue = $sensorTemp->constant_1;
                            $rawValue = isset($prop['offset']) ? $prop['offset']($item->$fieldName, $propSensor) : $item->$fieldName;
                            $tempDatetime = AppUtil::datetimeMicrotime($item->my_time);
                            array_push($sensorDataTemp, array($tempDatetime, $rawValue));
                        }

                        $series = array(
                            'name' => $propSensor->code.' ['.$measurements[$prop['measurement']]['code'].']',
                            'type' => 'spline',
                            'data' => $sensorDataTemp,
                            'yAxis' => $validation['idx']
                        );
                        array_push($seriesRs, $series);
                    }
                }
            }
        }

        return Response::json(array(
            'yAxis' => $yAxisRs,
            'series' => $seriesRs
        ));

    }

    public function inArray($needle, $haystack){
        $result = array(
            'exist' => false,
            'idx' => null
        );

        foreach($haystack as $key => $item){
            if($needle == $item){
                $result['exist'] = true;
                $result['idx'] = $key;
            }
        }

        return $result;
    }
}