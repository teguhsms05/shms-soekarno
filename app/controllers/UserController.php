<?php

class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = User::orderBy('id', 'desc')->paginate(10);
        return View::make('page.setting.user')->with(array(
            'user' => $user
        ));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('page.setting.user_form')->with(array());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $username = Input::get('username');
        $password = Input::get('password');
        $role = Input::get('role');

        $rules = array(
            'username' => 'required|unique:user',
            'password' => 'required|min:5|confirmed',
            'role' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->getMessageBag()->any()) {
            return Redirect::to('setting/user/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = new User;
            $user->username = $username;
            $user->email = $username;
            $user->password = Hash::make($password);
            $user->role = $role;
            $user->save();
            Session::flash('alert_message', '<strong>Success</strong> create the data.');
            Session::flash('alert_message_type', 'success');
            return Redirect::to('setting/user');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return Response::json(array(
            'user' => $user
        ));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return View::make('page.setting.user_form')->with(array(
            'user' => $user
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $username = Input::get('username');
        $password = Input::get('password');
        $role = Input::get('role');

        $rules = array(
            'username' => 'required|unique:user,username,' . $id,
            'password' => 'required|min:5|confirmed',
            'role' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->getMessageBag()->any()) {
            return Redirect::to('setting/user/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $user = User::findOrFail($id);
            $user->username = $username;
            $user->email = $username;
            $user->password = Hash::make($password);
            $user->role = $role;
            $user->save();
            Session::flash('alert_message', '<strong>Success</strong> edit the data.');
            Session::flash('alert_message_type', 'success');
            return Redirect::to('setting/user');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $affectedRows = User::where('id', '=', $id)->delete();
        Session::flash('alert_message', '<strong>Success</strong> delete the data.');
        Session::flash('alert_message_type', 'success');
        return Redirect::to('setting/user');
    }

}