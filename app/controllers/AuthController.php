<?php

class AuthController extends BaseController
{

    public function index()
    {
        if (Auth::check()) {
            return Redirect::to('');
        }
        return View::make('login')->with(array(
            'test' => ''
        ));
    }

    public function doLogin()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'username' => 'required', // make sure the username is an actual username
            'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)// send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );
            $remember = "false";
            if (Input::get('remember')) {
                $remember = "true";
            }
            // attempt to do the login
            if(Auth::attempt($userdata, $remember)) {
                // validation successful!
                // redirect them to the secure section or whatever
                if (Auth::user()->role == "ADMIN" || Auth::user()->role == "SUPER") {
                    return Redirect::to('/');
                } else {
                    return Redirect::to('/');
                }
            }
            // validation not successful, send back to form
            $messages = $validator->errors();
            $messages->add('username', 'Wrong username and password.');
            return Redirect::to('login')->withErrors($messages)->withInput(Input::except('password'));
        }
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('logout'); // redirect the user to the login screen
    }
}
