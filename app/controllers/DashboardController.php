<?php

class DashboardController extends BaseController
{

    public function index()
    {
        return View::make('page/dashboard')->with(array(
            'test' => ''
        ));
    }

    public function getSidebarData()
    {
        /*
        $parameters = Parameter::all();
        foreach ($parameters as $parameter) {
            $sensors = Sensor::select('section_id')->where('parameter_id', $parameter->id)->distinct()->get();
            $sections = array();
            foreach ($sensors as $sensor) {
                $section = array(
                    'id' => $sensor->section_id,
                    'code' => $sensor->section->code,
                    'desc' => $sensor->section->desc
                );
                array_push($sections, $section);
            }
            $parameter->sections = $sections;
        }
        $result = array(
            'parameters' => $parameters
        );
        */
        $sensorTypes = SensorType::all();
        foreach ($sensorTypes as $sensorType) {
            $sensorType->sensors = Sensor::where('sensor_type_id', '=', $sensorType->id)->get();
        }
        $result = array(
            'sensorTypes' => $sensorTypes
        );
        return Response::json($result);
    }

    public function firstData()
    {
        return View::make('page.sensor_first_data')->with(array());
    }
}