<?php

class SettingController extends \BaseController
{

    public function index()
    {
        return View::make('page.setting.index')->with(array(
            'data' => '',
        ));
    }

    public function getSensorData($id)
    {
        $sensor = Sensor::find($id);
        return Response::json(array(
            'sensor' => $sensor
        ));
    }


    //CREATED BY: MUHAMMAD F. MUDJIONO-------------SENSORS PROPERTIES EDIT PAGE
    public function getProperties()
    {
        $sensors = Sensor::all();
        return View::make('page.setting.sensor_properties', array(
            'sensorsProperties' => $sensors,
//            'message'=>false
        ));
    }
    //-------------------------------------------------------------------------

    public function getAcc()
    {
        $sensorList = Sensor::where('sensor_type_id', 1)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 1)->get();
        return View::make('page.setting.threshold_acc')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getAnmbi()
    {
        $sensorList = Sensor::where('sensor_type_id', 2)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 2)->get();
        return View::make('page.setting.threshold_anmbi')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getAnmtri()
    {
        $sensorList = Sensor::where('sensor_type_id', 3)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 3)->get();
        return View::make('page.setting.threshold_anmtri')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getAtrh()
    {
        $sensorList = Sensor::where('sensor_type_id', 4)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 4)->get();
        return View::make('page.setting.threshold_atrh')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getLc()
    {
        $sensorList = Sensor::where('sensor_type_id', 5)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 5)->get();
        return View::make('page.setting.threshold_lc')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getSsmac()
    {
        $sensorList = Sensor::where('sensor_type_id', 6)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 6)->get();
        return View::make('page.setting.threshold_ssmac')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getStrtr()
    {
        $sensorList = Sensor::where('sensor_type_id', 7)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 7)->get();
        return View::make('page.setting.threshold_strtr')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getTemp()
    {
        $sensorList = Sensor::where('sensor_type_id', 8)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 8)->get();
        return View::make('page.setting.threshold_temp')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    public function getTilt()
    {
        $sensorList = Sensor::where('sensor_type_id', 9)->lists('code', 'id');
        $sensors = Sensor::where('sensor_type_id', 9)->get();
        return View::make('page.setting.threshold_tilt')->with(array(
            'sensors' => $sensors,
            'sensorList' => $sensorList
        ));
    }

    //CREATED BY: MUHAMMAD F. MUDJIONO-------------SENSORS PROPERTES EDIT ACTION
    function postUpdateProperties(){
        $desc = Input::get('desc');
        $code = Input::get('code');
        $id = Input::get('id');

        foreach($id as $key=>$row){
            $affectedRow = Sensor::find($row);
            $affectedRow->desc = $desc[$key];
            $affectedRow->code = $code[$key];
            $affectedRow->save();
        }
        return Redirect::to('setting/properties?message=true');
//        return Redirect::action('SettingController@getProperties', array('message' => true));
    }
    //-------------------------------------------------------------------------

    public function postUpdateAcc()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'X threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Y threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Z threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 1)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/acc')->withErrors($messages)->withInput();
    }

    public function postUpdateAnmbi()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Level 1 speed threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Level 2 speed threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Level 3 speed threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 2)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/anmbi')->withErrors($messages)->withInput();
    }

    public function postUpdateAnmtri()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Level 1 speed threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Level 2 speed threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Level 3 speed threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 3)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/anmtri')->withErrors($messages)->withInput();
    }

    public function postUpdateAtrh()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Temperature threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Humidity threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 4)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/atrh')->withErrors($messages)->withInput();
    }

    public function postUpdateLc()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $constant_1 = Input::get('constant_1');
        $constant_2 = Input::get('constant_2');
        $constant_3 = Input::get('constant_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Level 1 threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Level 2 threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Level 3 threshold must be filled');
        }
//        if (($constant_1 == '') && (!is_numeric($constant_1))) {
//            $messages->add('constant_1', 'Constant 1 Must be filled');
//        }
        if (($constant_2 == '') && (!is_numeric($constant_2))) {
            $messages->add('constant_2', 'Constant 2 Must be filled');
        }
        if (($constant_3 == '') && (!is_numeric($constant_2))) {
            $messages->add('constant_3', 'Constant 3 Must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->constant_1 = $constant_1;
                $sensor->constant_2 = $constant_2;
                $sensor->constant_3 = $constant_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 5)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->constant_1 = $constant_1;
                    $row->constant_2 = $constant_2;
                    $row->constant_3 = $constant_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/lc')->withErrors($messages)->withInput();
    }

    public function postUpdateSsmac()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'X threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Y threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Z threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 6)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/ssmac')->withErrors($messages)->withInput();
    }

    public function postUpdateStrtr()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $constant_1 = Input::get('constant_1');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Level 1 threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            $messages->add('threshold_2', 'Level 2 threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Level 3 threshold must be filled');
        }
        if (($constant_1 == '') && (!is_numeric($threshold_3))) {
            $messages->add('constant_1', 'Offset must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->constant_1 = $constant_1;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 7)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->constant_1 = $constant_1;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/strtr')->withErrors($messages)->withInput();
    }

    public function postUpdateTemp()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'Temperature threshold must be filled');
        }
        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 8)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/temp')->withErrors($messages)->withInput();
    }

    public function postUpdateTilt()
    {
        $sensorId = Input::get('sensor_id');
        $threshold_1 = Input::get('threshold_1');
        $threshold_2 = Input::get('threshold_2');
        $threshold_3 = Input::get('threshold_3');
        $validator = Validator::make(Input::all(), array());
        $messages = $validator->errors();
        if ($threshold_1 == '' && !is_numeric($threshold_1)) {
            $messages->add('threshold_1', 'X threshold must be filled');
        }
        if (($threshold_2 == '') && (!is_numeric($threshold_2))) {
            //$messages->add('threshold_2', 'Y threshold must be filled');
        }
        if (($threshold_3 == '') && (!is_numeric($threshold_3))) {
            $messages->add('threshold_3', 'Z threshold must be filled');
        }

        if (!$messages->any()) {
            $sensor = Sensor::find($sensorId);
            if ($sensor) {
                $sensor = Sensor::find($sensorId);
                $sensor->threshold_1 = $threshold_1;
                $sensor->threshold_2 = $threshold_2;
                $sensor->threshold_3 = $threshold_3;
                $sensor->save();
            } else {
                $sensors = Sensor::where('sensor_type_id', 9)->get();
                foreach ($sensors as $row) {
                    $row->threshold_1 = $threshold_1;
                    $row->threshold_2 = $threshold_2;
                    $row->threshold_3 = $threshold_3;
                    $row->save();
                }
            }
        }
        return Redirect::to('setting/tilt')->withErrors($messages)->withInput();
    }


}