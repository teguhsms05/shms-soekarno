<?php

class AppController extends BaseController
{
    public function getDataSectionAll()
    {
        $sectionTemp = Section::all();
        $section = array();
        foreach ($sectionTemp as $row) {
            $state = array('off', 'on', 'warning');
            $randState = rand(0, 2);
            $myState = $this->getSectionStatus($row->id);
            $tempArray = array(
                'id' => $row->id,
                'code' => $row->code,
                'desc' => $row->desc,
                'status' => $state[$myState],
                'position_x' => $row->position_x,
                'position_y' => $row->position_y,
                'position_desc' => $row->position_desc
            );
            array_push($section, $tempArray);
        }
        return Response::json($section);
    }

    public function getSectionStatus($id, $sensorTypeId = 0)
    {
        $state = 0;
        $sensors = Sensor::where('section_id', '=', $id);
        if ($sensorTypeId > 0) {
            $sensors = $sensors->where('sensor_type_id', '=', $sensorTypeId);
        }
        $sensors = $sensors->get();
        $off = 0;
        $on = 0;
        $warning = 0;
        foreach ($sensors as $row) {
            $func = strtolower($row->sensorType->code) . 'StateStatus';
            $state_sensor = $this->$func($row->id);
            switch ($state_sensor) {
                case 0:
                    $off++;
                    break;
                case 1:
                    $on++;
                    break;
                case 2:
                    $warning++;
                    break;
            }
        }
        if ($on == count($sensors)) {
            $state = 1;
        }
        if ($warning > 0) {
            $state = 2;
        }
        return $state;
    }

    public function getDataSectionBySensorType($sensorTypeCode)
    {
        $sectionTemp = Sensor::select('section_id', 'sensor_type_id')
            ->join('sensor_type', 'sensor_type.id', '=', 'sensor.sensor_type_id')
            ->where('sensor_type.code', '=', $sensorTypeCode)
            ->distinct()
            ->get();
        $section = array();
        foreach ($sectionTemp as $row) {
            $state = array('off', 'on', 'warning');
            $randState = rand(0, 2);
            $myState = $this->getSectionStatus($row->section->id, $row->sensor_type_id);
            $tempArray = array(
                'id' => $row->section->id,
                'code' => $row->section->code,
                'desc' => $row->section->desc,
                'status' => $state[$myState],
                'position_x' => $row->section->position_x,
                'position_y' => $row->section->position_y,
                'position_desc' => $row->section->position_desc
            );
            array_push($section, $tempArray);
        }
        return Response::json($section);
    }

    public function getDataSectionDetail($id, $sensorTypeId = "")
    {
        $sensorTypeTemp = array();
        $section = Section::find($id);
        $sensorTemp = Sensor::where('section_id', '=', $id)->get();
        foreach ($sensorTemp as $row) {
            array_push($sensorTypeTemp, $row->sensor_type_id);
        }
        $sensorTypeTemp = array_unique($sensorTypeTemp);
        $sensorType = null;

        if ($sensorTypeId != "") {
            $sensorType = SensorType::where('code', '=', $sensorTypeId)->get();
        } else {
            $sensorType = SensorType::whereIn('id', $sensorTypeTemp)->get();
        }
        foreach ($sensorType as $row) {
            $sensors = array();
            foreach ($sensorTemp as $row2) {
                if ($row->id == $row2->sensor_type_id) {
                    $state = array('off', 'on', 'warning');
                    $func = strtolower($row2->sensorType->code) . 'StateStatus';
                    $myState = $this->$func($row2->id);
                    $randState = rand(0, 2);
                    $sensor = array(
                        'id' => $row2->id,
                        'code' => $row2->code,
                        'desc' => $row2->desc,
                        'state' => $state[$myState],
                        'button' => $this->getButton($myState),
                        'x' => $row2->x,
                        'y' => $row2->y,
                        'sensor_type' => $row2->sensorType
                    );
                    array_push($sensors, $sensor);
                }
            }
            $row->sensors = $sensors;
        }
        $result = array(
            'section' => $section,
            'sensorTypes' => $sensorType
        );
        return Response::json($result);
    }

    public function getButton($state)
    {
        $result = 'default';
        switch ($state) {
            case 0 :
                $result = 'default';
                break;
            case 1 :
                $result = 'success';
                break;
            case 2 :
                $result = 'danger';
                break;
        }
        return $result;
    }

    public function getDataSectionWithParameter($id, $parameter)
    {
        $section = Section::where('code', '=', $id)->first();
        return Response::json($section);
    }

    public function getDataParameter($id)
    {
        $parameter = Parameter::where('code', '=', $id)->first();
        return Response::json($parameter);
    }

    public function getDataSensorType($id)
    {
        $sensorType = SensorType::where('code', '=', $id)->first();
        return Response::json($sensorType);
    }

    public function accStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120; // minutes

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name . '_fft')
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->x || $lastData->y || $lastData->z) {
                if (($lastData->x > $sensor->threshold_1) || ($lastData->y > $sensor->threshold_2) || ($lastData->z > $sensor->threshold_3)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function anmbiStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->speed) {
                if ($lastData->speed > $sensor->threshold_3) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function anmtriStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->speed) {
                if ($lastData->speed > $sensor->threshold_3) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function atrhStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->temperature || $lastData->humidity) {
                if (($lastData->temperature > $sensor->threshold_1) || ($lastData->humidity > $sensor->threshold_2)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function lcStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->value) {
                if (($lastData->value > $sensor->threshold_3)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function ssmacStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120; // minutes

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->x || $lastData->y || $lastData->z) {
                if (($lastData->x > $sensor->threshold_1) || ($lastData->y > $sensor->threshold_2) || ($lastData->z > $sensor->threshold_3)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function strtrStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->value) {
                if (($lastData->value > $sensor->threshold_3)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function tempStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120;

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->temperature) {
                if (($lastData->temperature > $sensor->threshold_1)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function tiltStateStatus($id)
    {
        $state = 0;
        $sensor = Sensor::find($id);
        $isOfflineIn = 120; // minutes

        $now = new DateTime(date('Y-m-d H:i:s'));
        $now->modify('+1 second');
        $isOfflineTime = clone $now;
        $isOfflineTime->modify('-' . $isOfflineIn . ' minute');

        $lastData = DB::table($sensor->table_name)
            ->whereBetween('datetime', array($isOfflineTime->format("Y-m-d H:i:s"), $now->format("Y-m-d H:i:s")))
            ->orderBy('datetime', 'desc')->first();

        if ($lastData) {
            $state = 1;
            if ($lastData->x || $lastData->y || $lastData->z) {
                if (($lastData->x > $sensor->threshold_1) || ($lastData->y > $sensor->threshold_2) || ($lastData->z > $sensor->threshold_3)) {
                    $state = 2;
                }
            }
        }

        return $state;
    }

    public function getCoba()
    {
        $sensorTypeId = 0;
        $state = 1;
        $sensors = Sensor::where('section_id', '=', 19);
        if ($sensorTypeId > 0) {
            $sensors = $sensors->where('sensor_type_id', '=', $sensorTypeId);
        }
        $sensors = $sensors->get();
        $off = 0;
        $on = 0;
        $warning = 0;
        foreach ($sensors as $row) {
            $func = strtolower($row->sensorType->code) . 'StateStatus';
            $state = $this->$func($row->id);
            switch ($state) {
                case 0:
                    $off++;
                    break;
                case 1:
                    $on++;
                    break;
                case 2:
                    $warning++;
                    break;
            }
        }
        if ($on == count($sensors)) {
            $state = 1;
            return 'sama';
        }
        if ($warning > 0) {
            $state = 2;
        }
        return 'akhir' . count($sensors);
    }

}