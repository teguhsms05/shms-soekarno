<?php

class DummyDataController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getInsertAcc()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 1)->get();
        foreach ($sensors as $row) {
            $acc = new Acc($row->table_name . '_fft');
            $acc->datetime = AppUtil::getNow();
            $acc->x = rand(2, 5);
            $acc->y = rand(2, 5);
            $acc->z = rand(2, 5);
            //$acc->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $acc->save();
        }
        return "ACCTRI";
    }

    public function getInsertAnmbi()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 2)->get();
        foreach ($sensors as $row) {
            $anmbi = new Anmbi($row->table_name);
            $anmbi->datetime = AppUtil::getNow();
            $anmbi->speed = rand(0, 40);
            $anmbi->direction_azimuth = rand(0, 360);
            $anmbi->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $anmbi->save();
        }
        return "ANMBI";
    }

    public function getInsertAnmtri()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 3)->get();
        foreach ($sensors as $row) {
            $anmtri = new Anmtri($row->table_name);
            $anmtri->datetime = AppUtil::getNow();
            $anmtri->speed = rand(20, 50);
            $anmtri->direction_azimuth = rand(0, 360);
            $anmtri->direction_elevation = rand(-90, 90);
            $anmtri->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $anmtri->save();
        }
        return "ANMTRI";
    }

    public function getInsertAtrh()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 4)->get();
        foreach ($sensors as $row) {
            $atrh = new Atrh($row->table_name);
            $atrh->datetime = AppUtil::getNow();
            $atrh->temperature = rand(30, 40);
            $atrh->humidity = rand(80, 95);
            $atrh->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $atrh->save();
        }
        return "ATRH";
    }

    public function getInsertLc()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 5)->get();
        foreach ($sensors as $row) {
            $lc = new Lc($row->table_name);
            $lc->datetime = AppUtil::getNow();
            $lc->value = rand(450, 500);
            $lc->raw_value = rand(30, 100);
            //$lc->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $lc->save();
        }
        return "LC";
    }

    public function getInsertSsmac()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 6)->get();
        foreach ($sensors as $row) {
            $ssmac = new Ssmac($row->table_name);
            $ssmac->datetime = AppUtil::getNow();
            $ssmac->x = rand(1, 10000000) * 0.00000001;
            $ssmac->y = rand(1, 10000000) * 0.00000001;
            $ssmac->z = rand(1, 10000000) * 0.00000001;
            $ssmac->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $ssmac->save();
        }
        return "SSMAC";
    }

    public function getInsertStrtr()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 7)->get();
        foreach ($sensors as $row) {
            $strtr = new Strtr($row->table_name);
            $strtr->datetime = AppUtil::getNow();
            $strtr->value = rand(5, 30);
            $strtr->raw_value = rand(5, 30);
            $strtr->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $strtr->save();
        }
        return "STRTR";
    }

    public function getInsertTemp()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 8)->get();
        foreach ($sensors as $row) {
            $temp = new Temp($row->table_name);
            $temp->datetime = AppUtil::getNow();
            $temp->temperature = rand(27, 35);
            $temp->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $temp->save();
        }
        return "TEMP";
    }

    public function getInsertTilt()
    {
        $sensors = Sensor::where('sensor_type_id', '=', 9)->get();
        foreach ($sensors as $row) {
            $tilt = new Tilt($row->table_name);
            $tilt->datetime = AppUtil::getNow();
            $tilt->x = rand(0, 10) * 0.01;
            $tilt->y = rand(0, 10) * 0.01;
            $tilt->z = rand(0, 10) * 0.01;
            $tilt->hires = rand(0, 10000) * 0000.1;
            $tilt->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
            $tilt->save();
        }
        return "TILT";
    }

    public function getInsertRacktemp()
    {
        $racktemp = new Racktemp();
        $racktemp->datetime = AppUtil::getNow();
        $racktemp->value = rand(25, 30);
        $racktemp->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
        $racktemp->save();
        return "RACKTEMP";
    }

    public function getInsertUps()
    {
        $ups = new Ups();
        $ups->datetime = AppUtil::getNow();
        $ups->internal_temperature = rand(20, 50);
        $ups->input_voltage = rand(0, 5);
        $ups->output_voltage = rand(5, 10);
        $ups->input_frequency = rand(20, 50);
        $ups->battery_capacity = rand(20, 50);
        $ups->battery_voltage = rand(20, 50);
        $ups->runtime_remaining = "1hr";
        $ups->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
        $ups->save();
        return "UPS";
    }

    public function getInsertPlngenset()
    {
        $type = array('PLN', 'GENSET');
        $status = array(0, 1);
        $randState = rand(0, 1);

        $ups = new Plngenset();
        $ups->datetime = AppUtil::getNow();
        $ups->type = $type[$randState];
        $ups->status = $status[$randState];
        $ups->reference_id = 'RANDOM' . md5(uniqid(rand(), true));
        $ups->save();
        return "PLNGENSET";
    }

    public function getTest()
    {
        $start = microtime(true);
        $acc = DB::table("acc01_fft");
        $rows = $acc->count();
        $acc = $acc->get();
        $time_elapsed_secs = bcsub(microtime(true), $start, 7);
        return AppUtil::jsonPrettyPrint(array(
            "name" => "Plain Array",
            "memory" => memory_get_usage() . " bytes",
            "time" => $time_elapsed_secs . " seconds",
            "rows" => $rows . " rows",
            "acc" => $acc
        ));
    }

    public function getTest2()
    {
        $start = microtime(true);
        $acc = new Acc("acc01_fft");
        $acc = $acc->get();
        $rows = $acc->count();
        $time_elapsed_secs = bcsub(microtime(true), $start, 7);
        return AppUtil::jsonPrettyPrint(array(
            "name" => "Eloquent Objects",
            "memory" => memory_get_usage() . " bytes",
            "time" => $time_elapsed_secs . " seconds",
            "rows" => $rows . " rows",
            "acc" => $acc
        ));
    }

    public function getTest3()
    {
        $start = microtime(true);
        $acc = new MyModel("acc01_fft");
        $rows = $acc->getCount();
        $acc = $acc->get();
        $time_elapsed_secs = bcsub(microtime(true), $start, 7);
        return AppUtil::jsonPrettyPrint(array(
            "name" => "Plain Objects",
            "memory" => memory_get_usage() . " bytes",
            "time" => $time_elapsed_secs . " seconds",
            "rows" => $rows . " rows",
            "acc" => $acc
        ));
    }

}