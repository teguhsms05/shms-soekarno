<?php

class InitController extends \BaseController
{
    public function getInsertStrtr(){
        $date = Input::get('date');
        $sensors = Sensor::where('sensor_type_id', '=', 7)->get();
        foreach ($sensors as $row) {
            $affectedRow = null;
            if(!$date){
                $affectedRow = DB::table($row->table_name)->first();
            } else {
                $affectedRow = DB::table($row->table_name)
                    ->where('datetime', '=', $date)
                    ->first();
            }
            $row->constant_1 = $affectedRow->raw_value;
            $row->save();
        }
        return AppUtil::jsonPrettyPrint($sensors);
    }

    public function getInsertTilt()
    {
        $date = Input::get('date');
        $sensors = Sensor::where('sensor_type_id', '=', 9)->get();
        foreach ($sensors as $row) {
            $affectedRow = null;
            if(!$date){
                $affectedRow = DB::table($row->table_name)->first();
            }
            else {
                $affectedRow = DB::table($row->table_name)
                    ->where('datetime', '=', $date)
                    ->first();
            }
            $row->constant_1 = $affectedRow->x;
            $row->constant_2 = $affectedRow->y;
            $row->constant_3 = $affectedRow->z;
            $row->constant_4 = $affectedRow->hires;
            $row->save();
        }
        return AppUtil::jsonPrettyPrint($sensors);
    }
}