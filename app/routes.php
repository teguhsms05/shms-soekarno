<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', array('uses' => 'AuthController@index'));
Route::post('login', array('uses' => 'AuthController@doLogin'));

Route::controller('dummy-data', 'DummyDataController');

Route::get('memory-usage', function () {
    ini_set('memory_limit', '512M');
    ini_set('max_execution_time', 3000);
    $mem_usage = memory_get_usage(true);
    $mem_usage = ini_get('memory_limit');

    if ($mem_usage < 1024)
        return $mem_usage . " bytes";
    elseif ($mem_usage < 1048576)
        return round($mem_usage / 1024, 2) . " kilobytes";
    else
        return round($mem_usage / 1048576, 2) . " megabytes";
});

Route::group(array('before' => 'auth'), function () {

    Route::get('logout', array(
        'as' => 'logout',
        'uses' => 'AuthController@doLogout'
    ));

    Route::get('sidebar/data', 'DashboardController@getSidebarData');

    Route::get('', function () {
        return Redirect::to('dashboard');
    });

    Route::get('dashboard', array('uses' => 'DashboardController@index'));

    Route::controller('app', 'AppController');

    Route::get('racktemp/data', 'RacktempController@getData');
    Route::get('racktemp', 'RacktempController@index');

    Route::get('power-source/realtime-data', 'PowerSourceController@getRealtimeData');
    Route::get('power-source/data', 'PowerSourceController@getData');
    Route::get('power-source', 'PowerSourceController@index');

    Route::get('ups/data', 'UpsController@getData');
    Route::get('ups', 'UpsController@index');

    Route::get('sensor/acc/compare-sensors/data', 'SensorAccController@getCompareSensorsData');
    Route::get('sensor/acc/compare-sensors', 'SensorAccController@showCompareSensors');
    Route::get('sensor/acc/{id}/data', 'SensorAccController@getData');
    Route::get('sensor/acc/{id}', 'SensorAccController@show');
    Route::get('sensor/acc', 'SensorAccController@index');

    Route::get('sensor/anmtri/compare-sensors/data', 'SensorAnmtriController@getCompareSensorsData');
    Route::get('sensor/anmtri/compare-sensors', 'SensorAnmtriController@showCompareSensors');
    Route::get('sensor/anmtri/realtime-data', 'SensorAnmtriController@getRealtimeData');
    Route::get('sensor/anmtri/{id}/data', 'SensorAnmtriController@getData');
    Route::get('sensor/anmtri/{id}', 'SensorAnmtriController@show');
    Route::get('sensor/anmtri', 'SensorAnmtriController@index');

    Route::get('sensor/anmbi/compare-sensors/data', 'SensorAnmbiController@getCompareSensorsData');
    Route::get('sensor/anmbi/compare-sensors', 'SensorAnmbiController@showCompareSensors');
    Route::get('sensor/anmbi/realtime-data', 'SensorAnmbiController@getRealtimeData');
    Route::get('sensor/anmbi/{id}/data', 'SensorAnmbiController@getData');
    Route::get('sensor/anmbi/{id}', 'SensorAnmbiController@show');
    Route::get('sensor/anmbi', 'SensorAnmbiController@index');

    Route::get('sensor/atrh/compare-sensors/data', 'SensorAtrhController@getCompareSensorsData');
    Route::get('sensor/atrh/compare-sensors', 'SensorAtrhController@showCompareSensors');
    Route::get('sensor/atrh/realtime-data', 'SensorAtrhController@getRealtimeData');
    Route::get('sensor/atrh/{id}/data', 'SensorAtrhController@getData');
    Route::get('sensor/atrh/{id}', 'SensorAtrhController@show');
    Route::get('sensor/atrh', 'SensorAtrhController@index');

    Route::get('sensor/lc/test3', 'SensorLcController@test3');
    Route::get('sensor/lc/test2', 'SensorLcController@test2');
    Route::get('sensor/lc/test', 'SensorLcController@test');
    Route::get('sensor/lc/compare-sensors/data', 'SensorLcController@getCompareSensorsData');
    Route::get('sensor/lc/compare-sensors', 'SensorLcController@showCompareSensors');
    Route::get('sensor/lc/realtime-data', 'SensorLcController@getRealtimeData');
    Route::get('sensor/lc/{id}/data', 'SensorLcController@getData');
    Route::get('sensor/lc/{id}', 'SensorLcController@show');
    Route::get('sensor/lc', 'SensorLcController@index');
    Route::get('sensor/lc-cs', 'SensorLcController@showCableStress');

    Route::get('sensor/ssmac-detail/{id}/data', 'SensorSsmacDetailController@getData');
    Route::get('sensor/ssmac-detail/{id}', 'SensorSsmacDetailController@show');
    
    Route::get('sensor/ssmac/{id}/data', 'SensorSsmacController@getData');
    Route::get('sensor/ssmac/{id}', 'SensorSsmacController@show');
    Route::get('sensor/ssmac', 'SensorSsmacController@index');
    Route::get('sensor/ssmac/{id}/{ref}', 'SensorSsmacController@show2');

    Route::get('sensor/strtr/compare-sensors/data', 'SensorStrtrController@getCompareSensorsData');

    Route::get('sensor/strtr/pylon/south-view', 'SensorStrtrController@showPylonSouthView');
    Route::get('sensor/strtr/pylon/west-view', 'SensorStrtrController@showPylonWestView');
    Route::get('sensor/strtr/pylon/east-view', 'SensorStrtrController@showPylonEastView');
    Route::get('sensor/strtr/deck/{id}', 'SensorStrtrController@showDeck');

    Route::get('sensor/strtr/pylon/south-view/data', 'SensorStrtrController@getPylonSouthViewData');
    Route::get('sensor/strtr/pylon/west-view/data', 'SensorStrtrController@getPylonWestViewData');
    Route::get('sensor/strtr/pylon/east-view/data', 'SensorStrtrController@getPylonEastViewData');
    Route::get('sensor/strtr/deck/{id}/data', 'SensorStrtrController@getDeckData');

    Route::get('sensor/strtr/compare-sensors/data', 'SensorStrtrController@getCompareSensorsData');
    Route::get('sensor/strtr/compare-sensors', 'SensorStrtrController@showCompareSensors');
    Route::get('sensor/strtr/{id}/data', 'SensorStrtrController@getData');
    Route::get('sensor/strtr/{id}', 'SensorStrtrController@show');
    Route::get('sensor/strtr', 'SensorStrtrController@index');

    Route::get('sensor/temp/compare-sensors/data', 'SensorTempController@getCompareSensorsData');
    Route::get('sensor/temp/compare-sensors', 'SensorTempController@showCompareSensors');
    Route::get('sensor/temp/realtime-data', 'SensorTempController@getRealtimeData');
    Route::get('sensor/temp/{id}/data', 'SensorTempController@getData');
    Route::get('sensor/temp/{id}', 'SensorTempController@show');
    Route::get('sensor/temp', 'SensorTempController@index');

    Route::get('sensor/tilt/section/{id}/realtime-data', 'SensorTiltController@getSectionRealtimeData');
    Route::get('sensor/tilt/deck/{id}', 'SensorTiltController@showDeck');
    Route::get('sensor/tilt/pier/{id}', 'SensorTiltController@showPier');
    Route::get('sensor/tilt/pylon', 'SensorTiltController@showPylon');
    Route::get('sensor/tilt/pylon/data', 'SensorTiltController@getPylonViewData');
    Route::get('sensor/tilt/bridge-side-view/{side}/data', 'SensorTiltController@getBridgeSideViewData');
    Route::get('sensor/tilt/bridge-side-view/{side}', 'SensorTiltController@showBridgeSideView');
    Route::get('sensor/tilt/compare-sensors/data', 'SensorTiltController@getCompareSensorsData');
    Route::get('sensor/tilt/compare-sensors', 'SensorTiltController@showCompareSensors');
    Route::get('sensor/tilt/displacement/data', 'SensorTiltController@getDisplacementDisplacement');
    Route::get('sensor/tilt/displacement', 'SensorTiltController@showDisplacement');
    Route::get('sensor/tilt/displacement/last-data', 'SensorTiltSeController@getDisplacementLastData');
    Route::get('sensor/tilt/{id}/data', 'SensorTiltController@getData');
    Route::get('sensor/tilt/{id}', 'SensorTiltController@show');
    Route::get('sensor/tilt', 'SensorTiltController@index');

    Route::get('first-data', 'DashboardController@firstData');
    Route::controller('init', 'InitController');
    Route::get('compare-sensors', 'CompareSensorsController@index');
    Route::get('compare-sensors/data', 'CompareSensorsController@getData');
});

Route::group(array('before' => array('auth|super')), function () {
    Route::get('setting/user/{id}/destroy', 'UserController@destroy');
    Route::resource('setting/user', 'UserController');

});

Route::group(array('before' => array('auth|adminorsuper')), function () {
    Route::controller('setting', 'SettingController');
});