<?php

class Atrh extends Eloquent
{

    public $sensor_type_id = 4;
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    public function __construct($tableName, array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = $tableName;
    }

}