<?php

class AccModel
{
    protected $table;

    public function __construct($tableName, array $attributes = array())
    {
        $this->table = $tableName;
    }

    public function get()
    {
        return DB::table($this->table)->get();
    }
}