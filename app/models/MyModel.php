<?php

class MyModel
{
    protected $table;

    public function __construct($tableName, array $attributes = array())
    {
        $this->table = $tableName;
    }

    public function get()
    {
        return DB::table($this->table)->get();
    }

    public function getCount()
    {
        return DB::table($this->table)->count();
    }
}