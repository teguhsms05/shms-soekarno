<?php

class SensorType extends Eloquent
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sensor_type';

}