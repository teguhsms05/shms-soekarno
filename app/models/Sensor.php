<?php

class Sensor extends Eloquent
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sensor';

    public function sensorType()
    {
        return $this->belongsTo('SensorType');
    }

    public function section()
    {
        return $this->belongsTo('Section');
    }

    public function parameter()
    {
        return $this->belongsTo('Parameter');
    }

}