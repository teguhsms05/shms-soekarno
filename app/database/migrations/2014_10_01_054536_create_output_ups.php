<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputUps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'ups';
        if (!Schema::hasTable($tableName)) {
            Schema::create('ups', function ($table) {
                $table->bigIncrements('id');
                $table->dateTime('datetime')->index('datetime');
                $table->double('internal_temperature');
                $table->double('input_voltage');
                $table->double('output_voltage');
                $table->double('input_frequency');
                $table->double('battery_capacity');
                $table->double('battery_voltage');
                $table->string('runtime_remaining');
                $table->string('reference_id')->unique();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ups');
    }

}