<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputPlngenset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'plngenset';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->bigIncrements('id');
                $table->dateTime('datetime')->index('datetime');
                $table->string('type');
                $table->boolean('status');
                $table->string('reference_id')->unique();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plngenset');
    }

}