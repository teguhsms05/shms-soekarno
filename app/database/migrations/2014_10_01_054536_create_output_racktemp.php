<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputRacktemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'racktemp';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->bigIncrements('id');
                $table->dateTime('datetime')->index('datetime');
                $table->boolean('value');
                $table->string('reference_id')->unique();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racktemp');
    }

}