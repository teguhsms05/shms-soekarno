<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputAnmtri extends Migration
{

    private $totalSensor = 2;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $tableName = 'anmtri' . $sensorNumber;
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('speed');
                    $table->float('direction_azimuth');
                    $table->float('direction_elevation');
                    $table->string('reference_id')->unique();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            Schema::dropIfExists('anmtri' . $sensorNumber);
        }
    }

}
