<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorType extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'sensor_type';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');
                $table->string('code')->unique();
                $table->text('desc')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor_type');
    }

}
