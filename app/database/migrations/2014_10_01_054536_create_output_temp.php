<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputTemp extends Migration
{

    private $totalSensor = 4;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $tableName = 'temp' . $sensorNumber;
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('temperature');
                    $table->string('reference_id')->unique();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            Schema::dropIfExists('temp' . $sensorNumber);
        }
    }

}
