<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputAcc extends Migration
{

    private $totalSensor = 7;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $baseTable = 'acc' . $sensorNumber;
            $tableName = $baseTable;
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('x');
                    $table->float('y');
                    $table->float('z');
                    $table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('x');
                    $table->float('y');
                    $table->float('z');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_x';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_y';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_z';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            Schema::dropIfExists('acc' . $sensorNumber);
            Schema::dropIfExists('acc' . $sensorNumber . '_fft');
            Schema::dropIfExists('acc' . $sensorNumber . '_fft_x');
            Schema::dropIfExists('acc' . $sensorNumber . '_fft_y');
            Schema::dropIfExists('acc' . $sensorNumber . '_fft_z');
        }
    }

}
