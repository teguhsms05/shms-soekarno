<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutputAtrh extends Migration
{

    private $totalSensor = 4;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $tableName = 'atrh' . $sensorNumber;
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('temperature');
                    $table->float('humidity');
                    $table->string('reference_id')->unique();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        for ($i = 1; $i <= $this->totalSensor; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            Schema::dropIfExists('atrh' . $sensorNumber);
        }
    }

}
