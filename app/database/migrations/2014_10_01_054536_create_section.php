<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSection extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'section';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');
                $table->string('code')->unique();
                $table->text('desc')->nullable();

                $table->integer('position_x')->default(0);
                $table->integer('position_y')->default(0);
                $table->char('position_desc')->default('');

                $table->string('canvas_background')->default('');
                $table->string('canvas_width')->default('');
                $table->string('canvas_height')->default('');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section');
    }

}
