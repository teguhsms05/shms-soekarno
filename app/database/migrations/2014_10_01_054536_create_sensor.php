<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensor extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'sensor';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');
                $table->string('code')->unique();
                $table->text('desc')->nullable();

                $table->string('table_name')->default('');

                $table->integer('sensor_type_id')
                    ->references('id')->on('sensor_type')
                    ->onDelete('cascade');

                $table->integer('section_id')
                    ->references('id')->on('section')
                    ->onDelete('cascade');

                $table->integer('parameter_id')
                    ->references('id')->on('parameter')
                    ->onDelete('cascade');

                $table->integer('position_x')->default(0);
                $table->integer('position_y')->default(0);
                $table->char('position_desc')->default(0);

                $table->double('threshold_1', 8, 5)->default(0);
                $table->double('threshold_2', 8, 5)->default(0);
                $table->double('threshold_3', 8, 5)->default(0);
                $table->double('threshold_4', 8, 5)->default(0);
                $table->double('threshold_5', 8, 5)->default(0);
                $table->double('threshold_6', 8, 5)->default(0);
                $table->double('threshold_7', 8, 5)->default(0);
                $table->double('threshold_8', 8, 5)->default(0);
                $table->double('threshold_9', 8, 5)->default(0);
                $table->double('threshold_10', 8, 5)->default(0);

                $table->double('constant_1', 8, 5)->default(0);
                $table->double('constant_2', 8, 5)->default(0);
                $table->double('constant_3', 8, 5)->default(0);
                $table->double('constant_4', 8, 5)->default(0);
                $table->double('constant_5', 8, 5)->default(0);
                $table->double('constant_6', 8, 5)->default(0);
                $table->double('constant_7', 8, 5)->default(0);
                $table->double('constant_8', 8, 5)->default(0);
                $table->double('constant_9', 8, 5)->default(0);
                $table->double('constant_10', 8, 5)->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor');
    }

}
