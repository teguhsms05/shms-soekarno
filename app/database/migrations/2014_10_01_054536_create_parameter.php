<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameter extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'parameter';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');
                $table->string('code')->unique();
                $table->text('desc')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter');
    }

}
