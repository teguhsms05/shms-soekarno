<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'user';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password')->default('demo123');
                $table->enum('role', array('SUPER', 'ADMIN', 'OPERATOR'))->default('OPERATOR');
                $table->string('name')->default('');
                $table->text('address')->nullable();
                $table->string('phone_number')->default('');
                $table->string('postal_code')->default('');
                $table->enum('gender', array('MALE', 'FEMALE'))->default('MALE');
                $table->string('avatar')->default('');
                $table->nullableTimestamps();
                $table->rememberToken();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }

}
