<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThresholdDisplacement extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'threshold_displacement';
        if (!Schema::hasTable($tableName)) {
            Schema::create($tableName, function ($table) {
                $table->increments('id');

                $table->float('tilt03');
                $table->float('tilt04');

                $table->float('tilt05');
                $table->float('tilt06');

                $table->float('tilt07');
                $table->float('tilt08');

                $table->float('tilt09');
                $table->float('tilt10');

                $table->float('tilt11');
                $table->float('tilt12');

                $table->float('tilt13');
                $table->float('tilt14');

                $table->float('tilt15');
                $table->float('tilt16');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threshold_displacement');
    }

}
