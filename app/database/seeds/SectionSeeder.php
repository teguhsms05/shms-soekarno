<?php

class SectionSeeder extends Seeder
{

    public function run()
    {
        DB::table('section')->truncate();
        $array = array(array(
            'code' => 'S1',
            'desc' => 'SOUTH 1',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 625,
            'position_y' => 184,
            'position_desc' => 'b',
        ), array(
            'code' => 'S2',
            'desc' => 'SOUTH 2',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 687,
            'position_y' => 185,
            'position_desc' => 'b',
        ), array(
            'code' => 'S3',
            'desc' => 'SOUTH 3',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 714,
            'position_y' => 183,
            'position_desc' => 'b',
        ), array(
            'code' => 'S4',
            'desc' => 'SOUTH 4',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 804,
            'position_y' => 192,
            'position_desc' => 'b',
        ), array(
            'code' => 'S5',
            'desc' => 'SOUTH 5',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 845,
            'position_y' => 189,
            'position_desc' => 'b',
        ), array(
            'code' => 'S6',
            'desc' => 'SOUTH 6',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 857,
            'position_y' => 189,
            'position_desc' => 'b',
        ), array(
            'code' => 'S7',
            'desc' => 'SOUTH 7',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 882,
            'position_y' => 191,
            'position_desc' => 't',
        ), array(
            'code' => 'S8',
            'desc' => 'SOUTH 8',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 905,
            'position_y' => 190,
            'position_desc' => 't',
        ), array(
            'code' => 'S9',
            'desc' => 'SOUTH 9',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 918,
            'position_y' => 192,
            'position_desc' => 't',
        ), array(
            'code' => 'S10',
            'desc' => 'SOUTH 10',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 944,
            'position_y' => 208,
            'position_desc' => 'r',
        ), array(
            'code' => 'N1',
            'desc' => 'NORTH 1',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 570,
            'position_y' => 183,
            'position_desc' => 'b',
        ), array(
            'code' => 'N2',
            'desc' => 'NORTH 2',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 542,
            'position_y' => 183,
            'position_desc' => 'b',
        ), array(
            'code' => 'N3',
            'desc' => 'NORTH 3',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 475,
            'position_y' => 184,
            'position_desc' => 'b',
        ), array(
            'code' => 'N4',
            'desc' => 'NORTH 4',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 447,
            'position_y' => 183,
            'position_desc' => 'b',
        ), array(
            'code' => 'N5',
            'desc' => 'NORTH 5',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 394,
            'position_y' => 184,
            'position_desc' => 'b',
        ), array(
            'code' => 'N6',
            'desc' => 'NORTH 6',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 360,
            'position_y' => 189,
            'position_desc' => 'b',
        ), array(
            'code' => 'N7',
            'desc' => 'NORTH 7',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 320,
            'position_y' => 188,
            'position_desc' => 'b',
        ), array(
            'code' => 'N8',
            'desc' => 'NORTH 8',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 309,
            'position_y' => 188,
            'position_desc' => 't',
        ), array(
            'code' => 'N9',
            'desc' => 'NORTH 9',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 283,
            'position_y' => 190,
            'position_desc' => 't',
        ), array(
            'code' => 'N10',
            'desc' => 'NORTH 10',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 258,
            'position_y' => 190,
            'position_desc' => 't',
        ), array(
            'code' => 'N11',
            'desc' => 'NORTH 11',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 243,
            'position_y' => 192,
            'position_desc' => 'b',
        ), array(
            'code' => 'N12',
            'desc' => 'NORTH 12',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 216,
            'position_y' => 207,
            'position_desc' => 'l',
        ), array(
            'code' => 'P0',
            'desc' => 'PYLON 0',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 56,
            'position_y' => 230,
            'position_desc' => 'l',
        ), array(
            'code' => 'P1',
            'desc' => 'PYLON 1',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 56,
            'position_y' => 220,
            'position_desc' => 'l',
        ), array(
            'code' => 'P2',
            'desc' => 'PYLON 2',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 45,
            'position_y' => 190,
            'position_desc' => 'l',
        ), array(
            'code' => 'P3',
            'desc' => 'PYLON 3',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 53,
            'position_y' => 95,
            'position_desc' => 'l',
        ), array(
            'code' => 'P4',
            'desc' => 'PYLON 4',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 53,
            'position_y' => 80,
            'position_desc' => 'l',
        ), array(
            'code' => 'P5',
            'desc' => 'PYLON 5',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 53,
            'position_y' => 60,
            'position_desc' => 'l',
        ), array(
            'code' => 'P6',
            'desc' => 'PYLON 6',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 53,
            'position_y' => 45,
            'position_desc' => 'l',
        ), array(
            'code' => 'P7',
            'desc' => 'PYLON 7',
            'canvas_background' => 'bridge_girder.jpg',
            'canvas_width' => 980,
            'canvas_height' => 290,
            'position_x' => 53,
            'position_y' => 30,
            'position_desc' => 'l',
        ));
        Section::insert($array);
    }

}