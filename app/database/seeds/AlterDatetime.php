<?php

class AlterDatetime extends Seeder
{

    public function run()
    {
        $sensor = Sensor::all();
        foreach ($sensor as $row) {
            DB::statement("ALTER TABLE `" . $row->table_name . "` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
            if (substr($row->table_name, 0, 3) == "acc") {
                DB::statement("ALTER TABLE `" . $row->table_name . "_fft` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
                DB::statement("ALTER TABLE `" . $row->table_name . "_fft_x` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
                DB::statement("ALTER TABLE `" . $row->table_name . "_fft_y` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
                DB::statement("ALTER TABLE `" . $row->table_name . "_fft_z` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
            }
        }
        DB::statement("ALTER TABLE `ups` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
        DB::statement("ALTER TABLE `plngenset` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
        DB::statement("ALTER TABLE `racktemp` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
        DB::statement("ALTER TABLE `displacement` CHANGE COLUMN `datetime` `datetime` DATETIME(3) ;");
    }

}