<?php

class AlterAcc extends Seeder
{

    public function run()
    {
        for ($i = 1; $i <= 7; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $baseTable = 'acc' . $sensorNumber;
            $tableName = $baseTable;
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('x');
                    $table->float('y');
                    $table->float('z');
                    $table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('x');
                    $table->float('y');
                    $table->float('z');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_x';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_y';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
            $tableName = $baseTable . '_fft_z';
            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function ($table) {
                    $table->bigIncrements('id');
                    $table->dateTime('datetime')->index('datetime');
                    $table->float('value');
                    //$table->string('reference_id')->unique();
                });
            }
        }
    }

}
