<?php

class SensorSeeder extends Seeder
{

    public function run()
    {
        DB::table('sensor')->truncate();

        //ACC
        $st = SensorType::find(1);
        $i = 0;
        $sections = [7, 3, 14, 19, 25, 26, 28];
        foreach ($sections as $section) {
            $sensorNumber = AppUtil::addLeadingZeros(++$i);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $section,
                'parameter_id' => $st->id,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 3,
                'threshold_2' => 3,
                'threshold_3' => 3,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0,
            );
            Sensor::insert($array);
        }

        //ANMBI
        $st = SensorType::find(2);
        $sensorNumber = AppUtil::addLeadingZeros(1);
        $array = array(
            'code' => $st->code . $sensorNumber,
            'desc' => $st->desc . ' ' . $sensorNumber,
            'sensor_type_id' => $st->id,
            'section_id' => 30,
            'parameter_id' => $st->id,
            'table_name' => strtolower($st->code . $sensorNumber),
            'threshold_1' => 35,
            'threshold_2' => 50,
            'threshold_3' => 70,
            'threshold_4' => 0,
            'threshold_5' => 0,
            'threshold_6' => 0
        );
        Sensor::insert($array);

        //ANMTRI
        $st = SensorType::find(3);
        $i = 0;
        $sections = [5, 17];
        foreach ($sections as $section) {
            $sensorNumber = AppUtil::addLeadingZeros(++$i);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $section,
                'parameter_id' => $st->id,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 35,
                'threshold_2' => 50,
                'threshold_3' => 70,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }

        //ATRH
        $st = SensorType::find(4);
        $i = 0;
        $sections = [3, 15, 27, 27];
        foreach ($sections as $section) {
            $sensorNumber = AppUtil::addLeadingZeros(++$i);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $section,
                'parameter_id' => $st->id,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 40,
                'threshold_2' => 97,
                'threshold_3' => 0,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }

        //LC
        $st = SensorType::find(5);
        $i = 0;
        $sections = [7, 7, 4, 4, 2, 2, 13, 13, 16, 16, 19, 19];
        foreach ($sections as $section) {
            $sensorNumber = AppUtil::addLeadingZeros(++$i);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $section,
                'parameter_id' => $st->id,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 110,
                'threshold_2' => 130,
                'threshold_3' => 150,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }

        //SSMAC
        $st = SensorType::find(6);
        $sensorNumber = AppUtil::addLeadingZeros(1);
        $array = array(
            'code' => $st->code . $sensorNumber,
            'desc' => $st->desc . ' ' . $sensorNumber,
            'sensor_type_id' => $st->id,
            'section_id' => 23,
            'parameter_id' => $st->id,
            'table_name' => strtolower($st->code . $sensorNumber),
            'threshold_1' => 110,
            'threshold_2' => 0,
            'threshold_3' => 0,
            'threshold_4' => 0,
            'threshold_5' => 0,
            'threshold_6' => 0
        );
        Sensor::insert($array);

        //STRTR
        $st = SensorType::find(7);
        $i = 0;
        $sections = [9, 6, 2, 1, 12, 13, 18, 21];
        foreach ($sections as $section) {
            for ($x = 1; $x <= 4; $x++) {
                $sensorNumber = AppUtil::addLeadingZeros(++$i);
                $array = array(
                    'code' => $st->code . $sensorNumber,
                    'desc' => $st->desc . ' ' . $sensorNumber,
                    'sensor_type_id' => $st->id,
                    'section_id' => $section,
                    'parameter_id' => $st->id,
                    'table_name' => strtolower($st->code . $sensorNumber),
                    'threshold_1' => 23,
                    'threshold_2' => 25,
                    'threshold_3' => 32,
                    'threshold_4' => 0,
                    'threshold_5' => 0,
                    'threshold_6' => 0
                );
                Sensor::insert($array);
            }
        }
        $sections = [24, 28];
        foreach ($sections as $section) {
            for ($x = 1; $x <= 8; $x++) {
                $sensorNumber = AppUtil::addLeadingZeros(++$i);
                $array = array(
                    'code' => $st->code . $sensorNumber,
                    'desc' => $st->desc . ' ' . $sensorNumber,
                    'sensor_type_id' => $st->id,
                    'section_id' => $section,
                    'parameter_id' => $st->id,
                    'table_name' => strtolower($st->code . $sensorNumber),
                    'threshold_1' => 23,
                    'threshold_2' => 25,
                    'threshold_3' => 32,
                    'threshold_4' => 0,
                    'threshold_5' => 0,
                    'threshold_6' => 0
                );
                Sensor::insert($array);
            }
        }

        //TEMP
        $st = SensorType::find(8);
        $i = 0;
        $sections = [6, 6, 18, 18];
        foreach ($sections as $key => $section) {
            $sensorNumber = AppUtil::addLeadingZeros(++$i);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $section,
                'parameter_id' => 4,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 40,
                'threshold_2' => 0,
                'threshold_3' => 0,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }

        //TILT
        $st = SensorType::find(9);
        $arr_code = [1, 2, 17, 18, 19, 20, 21, 22];
        $arr_section = [10, 10, 22, 22, 27, 27, 29, 29];
        foreach ($arr_code as $key => $value) {
            $sensorNumber = AppUtil::addLeadingZeros($arr_code[$key]);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $arr_section[$key],
                'parameter_id' => 8,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 5,
                'threshold_2' => 5,
                'threshold_3' => 5,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }

        $arr_code = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
        $arr_section = [8, 8, 4, 4, 2, 2, 11, 11, 13, 13, 16, 16, 20, 20];
        foreach ($arr_code as $key => $value) {
            $sensorNumber = AppUtil::addLeadingZeros($arr_code[$key]);
            $array = array(
                'code' => $st->code . $sensorNumber,
                'desc' => $st->desc . ' ' . $sensorNumber,
                'sensor_type_id' => $st->id,
                'section_id' => $arr_section[$key],
                'parameter_id' => 8,
                'table_name' => strtolower($st->code . $sensorNumber),
                'threshold_1' => 5,
                'threshold_2' => 5,
                'threshold_3' => 5,
                'threshold_4' => 0,
                'threshold_5' => 0,
                'threshold_6' => 0
            );
            Sensor::insert($array);
        }
    }

}