<?php

class SensorTypeSeeder extends Seeder
{

    public function run()
    {
        DB::table('sensor_type')->truncate();
        $array = array(array(
            'code' => 'ACC',
            'desc' => 'ACCELEROMETER'
        ), array(
            'code' => 'ANMBI',
            'desc' => 'ANEMOMETER BIAXIAL'
        ), array(
            'code' => 'ANMTRI',
            'desc' => 'ANEMOMETER TRIAXIAL'
        ), array(
            'code' => 'ATRH',
            'desc' => 'ATRH'
        ), array(
            'code' => 'LC',
            'desc' => 'LOAD CELL'
        ), array(
            'code' => 'SSMAC',
            'desc' => 'SEISMIC'
        ), array(
            'code' => 'STRTR',
            'desc' => 'STRAIN TRANSDUCER'
        ), array(
            'code' => 'TEMP',
            'desc' => 'TEMPERATURE'
        ), array(
            'code' => 'TILT',
            'desc' => 'TILTMETER'
        )
        );
        SensorType::insert($array);
    }

}