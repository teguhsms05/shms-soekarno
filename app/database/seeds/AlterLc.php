<?php

class AlterLc extends Seeder
{

    public function run()
    {
        for ($i = 1; $i <= 12; $i++) {
            $sensorNumber = AppUtil::addLeadingZeros($i);
            $tableName = 'lc' . $sensorNumber;
            if (Schema::hasTable($tableName)) {
                DB::statement('ALTER TABLE `' . $tableName . '` MODIFY `raw_value` DOUBLE(8,5) NULL;');
            }
        }
    }

}
