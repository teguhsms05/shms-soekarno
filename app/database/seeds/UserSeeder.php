<?php

class UserSeeder extends Seeder
{

    public function run()
    {
        DB::table('user')->truncate();
        $temp = array(
            'email' => 'super@super.com',
            'username' => 'super',
            'password' => Hash::make('super123'),
            'role' => 'SUPER',
            'name' => 'SUPER',
            'avatar' => 'default_avatar.png'
        );
        User::insert($temp);
        $temp = array(
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'password' => Hash::make('admin123'),
            'role' => 'ADMIN',
            'name' => 'ADMIN',
            'avatar' => 'default_avatar.png'
        );
        User::insert($temp);
        $temp = array(
            'email' => 'operator@operator.com',
            'username' => 'operator',
            'password' => Hash::make('operator123'),
            'role' => 'OPERATOR',
            'name' => 'OPERATOR',
            'avatar' => 'default_avatar.png'
        );
        User::insert($temp);
    }

}
