<?php

class ParameterSeeder extends Seeder
{

    public function run()
    {
        DB::table('parameter')->truncate();
        $array = array(array(
            'code' => 'NFR',
            'desc' => 'Natural Frequency'
        ), array(
            'code' => 'WMP',
            'desc' => 'Wind Monitoring of Pylon'
        ), array(
            'code' => 'WMD',
            'desc' => 'Wind Monitoring of Deck'
        ), array(
            'code' => 'TRH',
            'desc' => 'Temperature & Relative Humidity'
        ), array(
            'code' => 'CFO',
            'desc' => 'Cable Force'
        ), array(
            'code' => 'SMO',
            'desc' => 'Seismic Monitoring'
        ), array(
            'code' => 'SSM',
            'desc' => 'Stress Strain Monitoring'
        ), array(
            'code' => 'INC',
            'desc' => 'Inclination'
        ), array(
            'code' => 'DIS',
            'desc' => 'Displacement'
        ));
        Parameter::insert($array);
    }

}