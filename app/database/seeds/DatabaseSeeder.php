<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call('UserSeeder');
        $this->command->info('User seeded!');
        $this->call('SensorTypeSeeder');
        $this->command->info('Sensor type seeded!');
        $this->call('ParameterSeeder');
        $this->command->info('Parameter seeded!');
        $this->call('SectionSeeder');
        $this->command->info('Section seeded!');
        $this->call('SensorSeeder');
        $this->command->info('Sensor seeded!');
        $this->call('AlterAcc');
        $this->command->info('Acc Altered!');
        $this->call('AlterLc');
        $this->command->info('Lc Altered!');
        /*
        $this->call('AlterTilt');
        $this->command->info('Tilt Altered!');
        $this->call('DeleteFromTable');
        $this->command->info('Datetime Altered!');
        */
        $this->call('AlterDatetime');
        $this->command->info('Datetime Altered!');
    }

}
