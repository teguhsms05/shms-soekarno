<?php

/**
 * Created by PhpStorm.
 * User: jimmydumalang
 * Date: 11/6/14
 * Time: 6:06 PM
 */
class AppUtil
{

    public static function getHTTPResponseCode($url)
    {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }

    public static function getDateRangeArray($startDate, $endDate)
    {
        $dates = array();
        $iDateFrom = mktime(1, 0, 0, substr($startDate, 5, 2), substr($startDate, 8, 2), substr($startDate, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($endDate, 5, 2), substr($endDate, 8, 2), substr($endDate, 0, 4));
        if ($iDateTo >= $iDateFrom) {
            array_push($dates, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($dates, date('Y-m-d', $iDateFrom));
            }
        }
        return $dates;
    }

    public static function getPage($url, $method = "GET", $data = array())
    {
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => $method,
                'content' => http_build_query($data),
                'timeout' => 20
            )
        );
        $context = stream_context_create($options);
        $content = @file_get_contents($url, false, $context);
        if (is_string($content)) {
            return $content;
        } else {
            return '';
        }
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function addLeadingZeros($number, $numberOfDigits = 2)
    {
        $num_padded = sprintf("%0" . $numberOfDigits . "d", $number);
        return $num_padded;
    }

    public static function setDefaultDatetime(){
        return strtotime(date('2015-08-10 00:00:00')) * 1000;
    }

    public static function getNow(){
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $result = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
//        $result->modify('+8 hour');
        return $result->format("Y-m-d H:i:s.u");
    }

    public static function datetimeMicrotime($myTime){
        $dateTime = new DateTime($myTime);
        $dateTime->modify('+8 hour');
        $dateTimeUnixTimestamp = $dateTime->format('U');
        $millisecond = substr($myTime, -3);
        $result = ($dateTimeUnixTimestamp * 1000) + $millisecond;
        return $result;
    }

    public static function precision($num){
        return strlen(substr($num, strpos($num, '.') + 1));
    }

    public static function reformatNumber($number, $precision = 8){
        $result = 0;
        if ($number > 0 || $number < 0) {
            $result = substr(number_format($number, $precision + 1, '.', ''), 0, -1);
        } elseif ($number == 0.00) {
            return 0;
        }
        return (float)$result;
    }

    //===========================================================================//
    //===================MUHAMMAD F. MUDJIONO, 18/04/2016========================//
    //===========================================================================//

    public static function jsonPrettyPrint($variable)
    {
        return Response::json($variable, 200, array(), JSON_PRETTY_PRINT);
    }


    public static function removeNumeric($char)
    {
        $words = preg_replace('/[0-9]+/', '', $char);
        return $words;
    }

    public static function returnMeasurement($sensor,$fieldinput){
//        $fields = array_unique($fieldinput);
//        $sensors = array();
//        foreach($inputs as $input){
//            array_push($sensors,self::removeNumeric($input));
//        }
//        $sensors = array_unique($sensors);
//        foreach($sensors as $sensor){
        $measurement = array();
            switch($sensor){
                case 'strtr':
                    $measurement[0] = array(
                        'code' => 'Strain',
                        'unitOfCount' => 'μStrain'
                    );
                    break;
                case 'lc':
                    $measurement[0] = array(
                        'code' => 'Cable Force',
                        'unitOfCount' => 'kN'
                    );
                    break;
                case 'anmbi':
                    foreach($fieldinput as $fields){
                        switch($fields){
                            case 'speed':
                                $measurement[0] = array(
                                    'code' => 'Velocity',
                                    'unitOfCount' => 'm/s'
                                );
                                break;
                            case 'direction_azimuth':
                                $measurement[0] = array(
                                    'code' => 'Direction Azimuth',
                                    'unitOfCount' => '°'
                                );
                                break;
                        }
                        break;
                    }
                case 'anmtri':
                    foreach($fieldinput as $fields){
                        switch($fields){
                            case 'speed':
                                $measurement[0] = array(
                                    'code' => 'Velocity',
                                    'unitOfCount' => 'm/s'
                                );
                                break;
                            case 'direction_azimuth':
                                $measurement[0] = array(
                                    'code' => 'Direction Azimuth',
                                    'unitOfCount' => '°'
                                );
                                break;
                            case 'direction_elevation':
                                $measurement[0] = array(
                                    'code' => 'Angle of attack',
                                    'unitOfCount' => '°'
                                );
                                break;
                        }
                        break;
                    }
                case 'atrh':
                    foreach($fieldinput as $fields){
                        switch($fields){
                            case 'temprature':
                                $measurement[0] = array(
                                    'code' => 'Humidity',
                                    'unitOfCount' => '%'
                                );
                                break;
                            case 'humidity':
                                $measurement[0] = array(
                                    'code' => 'Temprature',
                                    'unitOfCount' => '°C'
                                );
                                break;
                        }
                        break;
                    }
                case 'tilt':
                    $measurement[0] = array(
                        'code' => 'Inclination',
                        'unitOfCount' => '°'
                    );
                    break;
            }
        $plotLines = array();

        $plotLines[0] = array(
            'label' => array(
                'text' => '',
                'x' => 0,
                'style' => array(
                    'color' => ''
                )
            ),
            'color' => '',
            'width' => 0,
            'value' => '0',
            'dashStyle' => 'longdashdot'
        );

        $yAxis = array(
            'labels' => array(
                'format' => '{value} ' . $measurement[0]['unitOfCount'],
                'style' => array(
                    'color' => 'black'
                )
            ),
            'title' => array(
                'text' => $measurement[0]['code'] . ' (' . $measurement[0]['unitOfCount'] . ')',
                'align' => 'middle',
                'style' => array(
                    'color' => 'black'
                )
            ),
            'plotlines' => $plotLines,
            'opposite' => 'true'
        );
        return $yAxis;
    }

    public static function getMeasurement($sensor,$fields){
        $measurement = array();
        switch(self::removeNumeric($sensor)){
            case 'strtr':
                $measurement[0] = array(
                    'code' => 'Strain',
                    'unitOfCount' => 'μStrain'
                );
                break;
            case 'lc':
                $measurement[0] = array(
                    'code' => 'Cable Force',
                    'unitOfCount' => 'kN'
                );
                break;
            case 'anmbi':
                    switch($fields){
                        case 'speed':
                            $measurement[0] = array(
                                'code' => 'Velocity',
                                'unitOfCount' => 'm/s'
                            );
                            break;
                        case 'direction_azimuth':
                            $measurement[0] = array(
                                'code' => 'Direction Azimuth',
                                'unitOfCount' => '°'
                            );
                            break;
                    }
                    break;
            case 'anmtri':
                    switch($fields){
                        case 'speed':
                            $measurement[0] = array(
                                'code' => 'Velocity',
                                'unitOfCount' => 'm/s'
                            );
                            break;
                        case 'direction_azimuth':
                            $measurement[0] = array(
                                'code' => 'Direction Azimuth',
                                'unitOfCount' => '°'
                            );
                            break;
                        case 'direction_elevation':
                            $measurement[0] = array(
                                'code' => 'Angle of attack',
                                'unitOfCount' => '°'
                            );
                            break;
                    }
                    break;
            case 'atrh':
                    switch($fields){
                        case 'temprature':
                            $measurement[0] = array(
                                'code' => 'Humidity',
                                'unitOfCount' => '%'
                            );
                            break;
                        case 'humidity':
                            $measurement[0] = array(
                                'code' => 'Temprature',
                                'unitOfCount' => '°C'
                            );
                            break;
                    }
                    break;
            case 'tilt':
                $measurement[0] = array(
                    'code' => 'Inclination',
                    'unitOfCount' => '°'
                );
                break;
        }
        return $measurement;
    }

    public static function arrayUnique($input){
        $result = array();
        foreach($input as $row){
            if(!in_array($row,$result)){
                array_push($result,$row);
            }
        }
        return $result;
    }
//----------------------------------------------------------------------------//


}