@extends('...template')

@section('css_additional')
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
    <style>
        #my-content-container {
            width: 100%;
            overflow-x: scroll;
        }
        #my-graph, #my-table {
            width: 900px;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
    <li>Compare Sensors</li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

    @include('page.sensor_tilt_realtime_compare')

@stop

@section('javascript_additional')
@include('static/main_js')

<!-- select2 -->
<script src="{{ URL::to('assets/plugins/select2/select2.min.js'); }}"></script>

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>
<script src="{{ URL::to('assets/plugins/underscore/underscore-min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/backbone/backbone-min.js') }}"></script>
<script>

    function initGraph (graph_container, data) {
        Highcharts.setOptions({
                Global: {
                    useUTC: false
                }
            });
        $(graph_container).highcharts({
            credits: {
                enabled: false
            },
            chart: {
                zoomType: 'x'
            },
            legend:{
//                symbolHeight: 5,
                symbolWidth: 3,
                symbolRadius: 50
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                type: 'datetime',
                dateTimeLabelFormats: {
                    millisecond: '%H:%M:%S.%L',
                    second: '%H:%M:%S',
                    minute: '%H:%M',
                    hour: '%H:%M',
                    day: '%e. %b',
                    week: '%e. %b',
                    month: '%b \'%y',
                    year: '%Y'
                },
                crosshair: true
            }],
            yAxis: data.yAxis,
            series: data.series,
            tooltip: {
                shared: true,
                xDateFormat: '%A, %b %e, %H:%M:%S.%L'
            },
            plotOptions: {
                spline: {
//                    marker: {
//                        radius: 5,
//                        symbol: 'circle'
//                    },
                    lineWidth: 0.7,
                    states: {
                        hover: {
                            lineWidth: 0.9
                        }
                    }
                }
            }
        });
    }

    $(document).ready(function(){
        $('.datetime').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: true,
            autoApply: true,
            autoUpdateInput: true,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        });
        $(".select2").select2();
    });

    $("#request-data").submit(function(e) {
        $('#result-container').html('');

        var cont = '';
        var compiled = _.template($('#graph-template').html(), {variable: 'data'})({
            title: 'Compared Data',
            cont: cont
        });

        $('#result-container').append(compiled);

        url = "{{ URL::to("sensor/tilt/compare-sensors/data") }}";
        $.ajax({
            type: "GET",
            data: $('#request-data').serialize(),
            url: url,
            success: function(data)
            {
                if(data != false){
                    initGraph('#my-graph', data);
                    $('#my-box .overlay').remove();
                }else{
                    $.bootstrapGrowl("Sensor(s) can't be empty!", {
                      ele: 'body', // which element to append to
                      type: 'info', // (null, 'info', 'danger', 'success')
                      offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                      align: 'center', // ('left', 'right', or 'center')
                      width: 250, // (integer, or 'auto')
                      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                      allow_dismiss: true, // If true then will display a cross to close the popup.
                      stackup_spacing: 10 // spacing between consecutively stacked growls.
                    });
                    $('#result-container').html('');
                }
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

</script>
@stop