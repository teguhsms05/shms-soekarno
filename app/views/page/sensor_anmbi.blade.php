@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/css/realtime/anmbi.css'); }}">
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Realtime Monitor</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
       <div id="my-content-container">
           <div id="my-content">
               <div id="my-canvas">
                   <a href="{{ URL::to('sensor/anmbi/anmbi01') }}">
                        <div id="my-wind-direction-chart"></div>
                   </a>
                   <div class="my-wind-speed-container" id="my-wind-speed">
                       <div class="title">Wind Speed - ANMBI01</div>
                       <div class="input-group">
                           {{ Form::text('input-speed', 0, array('id' => 'input-speed', 'class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                           <span class="input-group-addon big">m/s</span>
                       </div>
                   </div>
                   <div id="my-datetime" {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                       {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                   </div>
               </div>
           </div>
       </div>
   </div>
    <!-- /.box-body -->
</div><!-- /.box -->

@stop

@section('javascript_additional')

@include('static/main_js')
<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/highcharts-more.js') }}"></script>
<!-- script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script -->

<script>
$(function () {
    Highcharts.setOptions({
        Global: {
            useUTC: false
        }
    });
    var myWindDirection = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: 'my-wind-direction-chart',
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            backgroundColor:'rgba(255, 255, 255, 0.1)'
        },

        title: {
            text: ''// Wind Direction
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{
                backgroundColor: 'rgba(255, 255, 255, 0.1)',
                borderWidth: 0,
                outerRadius: '0%',
                innerRadius: '100%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 360,
            tickWidth: 1,
            tickPosition: 'outside',
            tickLength: 10,
            tickColor: '#999',
            tickInterval:45,
            labels: {
                rotation: 0,
                formatter:function(){
                    if(this.value == 360) { return 'N'; }
                    else if(this.value == 45) { return 'NE'; }
                    else if(this.value == 90) { return 'E'; }
                    else if(this.value == 135) { return 'SE'; }
                    else if(this.value == 180) { return 'S'; }
                    else if(this.value == 225) { return 'SW'; }
                    else if(this.value == 270) { return 'W'; }
                    else if(this.value == 315) { return 'NW'; }
                },
                 style: {
                     fontWeight: 'bold',
                     color: "contrast",
                     fontSize: "11px",
                     fontWeight: "bold"
                 }
            },
             title: {
                 text: 'P7 - ANMBI01',// Middle Text
                 y: -80,
                 style: {
                     color: "contrast",
                     fontSize: "14px"
                 }
             }
        },

        plotOptions: {
            gauge: {
                dial: {
                    radius: '70%',
                    backgroundColor: 'red',
                    borderColor: 'red',
                    borderWidth: 1,
                    baseWidth: 1,
                    topWidth: 10,
                    baseLength: '0%', // of radius
                    rearLength: '0%'
                }
            }
        },

        series: [{
            name: 'Wind Direction',
            data: [0],
            tooltip: {
                valueSuffix: ' °'
            }
        }]
    });

    if (!myWindDirection.renderer.forExport) {
        loadData(myWindDirection);
        setInterval(function () {
            loadData(myWindDirection);
         }, 6000);
    }

    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/".strtolower($sensorType->code)."/realtime-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        return result;
    }

    function loadData(myWindDirection){
        var data = getData();
        if(data.anmbi01){
            myWindDirection.series[0].points[0].update(data.anmbi01.direction_azimuth);
            myWindDirection.yAxis[0].plotLinesAndBands[0].svgElem.attr({
                fill: data.anmbi01.color
            });
            $('#input-speed').val(data.anmbi01.speed);
            $('#input-wind-direction-text').val(data.anmbi01.direction_azimuth);
            $('#my-datetime input').val(data.anmbi01.datetime);
        }
    }
});



</script>

@stop