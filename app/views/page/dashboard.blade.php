@extends('...template')

@section('css_additional')
<style>
.button-container {
padding: 5px;
width:100%;
margin: auto;
}
.sensor-button{
width: 80px;
height: 80px;
margin-right: 5px;
}
.sensor-button img{
width: 100%;
}
</style>
@stop

@section('window_title')
Dashboard
@stop

@section('page_title')
Dashboard
<small></small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Dashboard
@stop

@section('toolbar')
@stop

@section('content')
<div class="button-container">
    <a href="" class="btn btn-default sensor-button" title="All Sensors">
        <i class="fa fa-home fa-4x"></i>
    </a>
    <a href="#sensor-type/ACC" class="btn btn-default sensor-button" title="Accelerometer">
        <img src="{{ URL::to('assets/images/icon/accelerometer.png'); }}"/>
    </a>
    <a href="#sensor-type/ANMBI" class="btn btn-default sensor-button" title="Anemometer Biaxial">
        <img src="{{ URL::to('assets/images/icon/anemometer2D.png'); }}"/>
    </a>
    <a href="#sensor-type/ANMTRI" class="btn btn-default sensor-button" title="Anemometer Triaxial">
        <img src="{{ URL::to('assets/images/icon/anemometer3D.png'); }}"/>
    </a>
    <a href="#sensor-type/ATRH" class="btn btn-default sensor-button" title="ATRH">
        <img src="{{ URL::to('assets/images/icon/atrh.png'); }}"/>
    </a>
    <a href="#sensor-type/LC" class="btn btn-default sensor-button" title="Load Cell">
        <img src="{{ URL::to('assets/images/icon/loadcell.png'); }}"/>
    </a>
    <a href="#sensor-type/SSMAC" class="btn btn-default sensor-button" title="Seismic">
        <img src="{{ URL::to('assets/images/icon/seismic.png'); }}"/>
    </a>
    <a href="#sensor-type/STRTR" class="btn btn-default sensor-button" title="Strain Transducer">
        <img src="{{ URL::to('assets/images/icon/strain.png'); }}"/>
    </a>
    <a href="#sensor-type/TEMP" class="btn btn-default sensor-button" title="Temperature">
        <img src="{{ URL::to('assets/images/icon/temp.png'); }}"/>
    </a>
    <a href="#sensor-type/TILT" class="btn btn-default sensor-button" title="Tiltmeter">
        <img src="{{ URL::to('assets/images/icon/tilt.png'); }}"/>
    </a>
</div>

<div class="box" id="main-box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <div id="canvas-navigator" class="btn-group p-b-5">
                <!--
                <button class="btn btn-box-tool"><i class="fa fa-arrow-left"></i></button>
                -->
                <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></button>
                <ul class="dropdown-menu" role="menu">
                    <!--li><a href="#parameter/NFR">Natural Frequency</a></li>
                    <li><a href="#parameter/WMP">Wind Monitoring of Pylon</a></li>
                    <li><a href="#parameter/WMD">Wind Monitoring of Deck</a></li>
                    <li><a href="#parameter/TRH">Temperature & Relative Humidty</a></li>
                    <li><a href="#parameter/CFO">Cable Force</a></li>
                    <li><a href="#parameter/SMO">Seismic Monitoring</a></li>
                    <li><a href="#parameter/SSM">Stress Strain Monitoring</a></li>
                    <li><a href="#parameter/INC">Inclination</a></li>
                    <li><a href="#parameter/DIS">Displacement</a></li-->
                    <?php
                        $sensorTypes = SensorType::all();
                    ?>
                    @foreach($sensorTypes as $sensorType)
                    <li><a href="#sensor-type/{{ $sensorType->code }}">{{ $sensorType->desc }}</a></li>
                    @endforeach
                </ul>
                <a href="#" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-th"></i></a>
            </div>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div id="canvas-bridge-container">
            <div id="canvas-bridge">
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<div id="content-container"></div>
@stop

@section('javascript_additional')
<script data-main="app/main" src="{{ URL::to('assets/plugins/requirejs/require.js'); }}"></script>
@stop