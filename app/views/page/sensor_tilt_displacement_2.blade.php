@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<style>
    #my-content-container {
        width: 100%;
        overflow-x: scroll;
    }
    #my-graph, #my-table {
        width: 900px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Displacement</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div id="my-graph"></div>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

<script type="text/javascript">
    function initData () {
        $('#my-graph').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        var series_even = this.series[0];
                        var series_odd = this.series[1];
                        console.log(getData());
                        setInterval(function() {
                            data = getData();
                            series_even.setData(data.even, true);
                            series_odd.setData(data.odd, true);
                        }, 1500);
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Displacement (cm)'
                }
            },
            tooltip: {
                formatter: function(){
                    return '<b>' + this.point.name + '</b><br/>' +
                    '<span>Displacement </span>: <b>' + Highcharts.numberFormat(this.point.y, 2) + ' </b>cm<br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S.%L', this.point.datetime);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Displacement Even Tilt',
                data: getData().even
            },
            {
                name: 'Displacement Odd Tilt',
                data: getData().odd
            },
            ]
        });
    }


    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/tilt/displacement-last-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        console.log(result)
        return result;
    }

    $(document).ready(function(){
        initData();
    });
</script>
@stop