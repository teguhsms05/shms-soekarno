@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/css/realtime/lc.css'); }}">
<style>
    .tengah {
        margin-left: auto;
        margin-right: auto;
        width: 800px;
    }
</style>
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Cable Stress</h3>
        <div class="box-tools pull-right">
            <div id="canvas-navigator" class="btn-group p-b-5">
                <a href="{{ URL::to('sensor/lc-cs'); }}" id="btn-side-view" class="btn btn-box-tool btn-default">Show Cable Force Graph</a>
            </div>
        </div><!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div class="box-body" style="height: 700px">
                <div class="tengah"><img src="{{ URL::to('assets/images/bridge_side_1.png'); }}" class="tengah"></div>
                
                <div id="my-graph"></div>
            </div>
            
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sensor</th>
                        <th>Cable Force (kN)</th>
                        <th>Cable Stress (MPa)</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>
<script src="https://highcharts.github.io/pattern-fill/pattern-fill.js"></script>

<script type="text/javascript">
    var gfxPath = 'https://raw.githubusercontent.com/highslide-software/pattern-fill/master/graphics/';
    function initData (data) {
        Highcharts.setOptions({
                Global: {
                    useUTC: false
                }
        });
        Highcharts.seriesTypes.bar.prototype.pointAttrToOptions.dashstyle = 'dashStyle';
        $('#my-graph').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                animation: Highcharts.svg, // don't animate in old IE
                events: {
                    load: function () {
                        data = getData();
						
                        var klabat_th3 = this.series[0];
                        var klabat_th2 = this.series[1];
                        var klabat_th1 = this.series[2];
                        var bunaken_th3 = this.series[3];
                        var bunaken_th2 = this.series[4];
                        var bunaken_th1 = this.series[5];
                        var klabat = this.series[6];
                        var bunaken = this.series[7];
                        var myTable = $('#my-table').DataTable({
                            "sDom": "<'row'<'col-sm-12'f>r>" + "t" + "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "bPaginate": false,
                            "bInfo": false,
                            "bFilter": false,
                            "order": [[ 0, "asc" ]],
                            "aaData": data.aaData
                        });

                        klabat.setData(data.graphcs_1, true);
                        klabat_th1.setData(data.graphcf_1_th1, true);
                        klabat_th2.setData(data.graphcf_1_th2, true);
                        klabat_th3.setData(data.graphcf_1_th3, true);

                        bunaken.setData(data.graphcs_2, true);
                        bunaken_th1.setData(data.graphcf_2_th1, true);
                        bunaken_th2.setData(data.graphcf_2_th2, true);
                        bunaken_th3.setData(data.graphcf_2_th3, true);

                        setInterval(function() {
                            data = getData();

                            klabat.setData(data.graphcs_1, true);
                            klabat_th1.setData(data.graphcf_1_th1, true);
                            klabat_th2.setData(data.graphcf_1_th2, true);
                            klabat_th3.setData(data.graphcf_1_th3, true);

                            bunaken.setData(data.graphcs_2, true);
                            bunaken_th1.setData(data.graphcf_2_th1, true);
                            bunaken_th2.setData(data.graphcf_2_th2, true);
                            bunaken_th3.setData(data.graphcf_2_th3, true);

                            myTable.clear();
                            myTable.rows.add(data.aaData);
                            myTable.draw();
                        }, 60000);
                    }
                },
                backgroundColor:'rgba(255, 255, 255, 0.0)'
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'category'
            },
            yAxis: [{
                labels: {
                    format: '{value} MPa',
                    style: {
                        color: 'black'
                    }
                },
                max: 1200,
                title: {
                    text: 'Cable Stress (MPa)',
                    align: 'middle'
                },
                <?php
                $sens1 = DB::table("sensor")->select()->where("sensor_type_id", "=", "5")->first();
                ?>
                plotLines: [{
                    value: <?php echo $sens1->constant_3;?>,
                    color: 'red',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '55% fu = <?php echo $sens1->constant_3;?> MPa'
                    }
                },{
                    value: <?php echo $sens1->constant_2;?>,
                    color: 'orange',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '45% fu = <?php echo $sens1->constant_2;?> MPa'
                    }
                }]
                
            }],
            tooltip: {
                shared: false,
                formatter: function(){
                    if(this.point.sensor != null){
                        return '<b>' + this.point.sensor + ' - ' + this.point.position + '</b><br/>' +
                        '<span>Cable Stress</span>: <b>' + Highcharts.numberFormat(this.point.y, 2) + ' </b>MPa<br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S.%L', this.point.datetime) + '<br/>' +
                        'Threshold 1: ' + this.point.threshold_1+ '<br/>' +
                        'Threshold 2: ' + this.point.threshold_2;
                    }else{
                        return Highcharts.numberFormat(this.point.y, 2) + ' </b>MPa<br/>';
                    }
                }
            },
            plotOptions: {
                column: {
                    grouping: true,
                    borderWidth: 1
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [
            {
                name: 'Threshold 3 - Klabat',
                yAxis: 0,
                type: 'scatter',
                pointPlacement: -0.2,
                marker: {
                    symbol: 'circle',
                    radius: 5
                }
            },
            {
                 name: 'Threshold 2 - Klabat',
                 yAxis: 0,
                 type: 'scatter',
                 pointWidth: 1,
                 pointPlacement: -0.2,
                 marker: {
                     symbol: 'circle',
                    radius: 5
                 }
            },
            {
                  name: 'Threshold 1 - Klabat',
                  yAxis: 0,
                  type: 'scatter',
                  pointWidth: 1,
                  pointPlacement: -0.2,
                  marker: {
                      symbol: 'circle',
                      radius: 5
                  }
            },
            {
                name: 'Threshold 3 - Bunaken',
                yAxis: 0,
                type: 'scatter',
                pointWidth: 1,
                pointPlacement: 0.2,
                marker: {
                    symbol: 'circle',
                    radius: 5
                }
            },
            {
                name: 'Threshold 2 - Bunaken',
                yAxis: 0,
                type: 'scatter',
                pointWidth: 1,
                pointPlacement: 0.2,
                marker: {
                    symbol: 'circle',
                    radius: 5
                }
            },
            {
                name: 'Threshold 1 - Bunaken',
                yAxis: 0,
                type: 'scatter',
                pointWidth: 1,
                pointPlacement: 0.2,
                marker: {
                    symbol: 'circle',
                    radius: 5
                }
            },
            {
                name: 'Klabat',
                yAxis: 0,
                type: 'column',
                pointPlacement: 1,
                pointWidth: 10,
                pointPlacement: -0.05,
                borderWidth: 1,
                borderColor: '#cdcdcd'
            },
            {
                name: 'Bunaken',
                yAxis: 0,
                type: 'column',
                pointPlacement: 2,
                pointWidth: 10,
                pointPlacement: 0.05,
                borderWidth: 1,
                dashStyle: 'dash',
                borderColor: 'black'
            }
            ],
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    document.location.href = "{{ URL::to('sensor/'.strtolower($sensorType->code))}}/"+this.sensor.toLowerCase();
                                }
                            }
                        },
                        marker: {
                            lineWidth: 1
                        }
                    }
                }
        });
    }

    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/".strtolower($sensorType->code)."/realtime-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        return result;
    }

    $(document).ready(function(){
        initData();
    });
</script>
@stop