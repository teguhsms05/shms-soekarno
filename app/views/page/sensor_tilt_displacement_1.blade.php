@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<style>
</style>
@stop

@section('window_title')
Tilt
@stop

@section('page_title')
Tilt
<small>Dislplacement</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Tilt</a></li>
    <li><a href="{{ URL::to('sensor/tilt/displacement') }}">Displacement</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Displacement
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Displacement</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="canvas-bridge-container">
            <div id="canvas-bridge"></div>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="https://cdn.rawgit.com/konvajs/konva/0.10.0/konva.min.js"></script>

<script type="text/javascript">
    function initCanvas () {
        var width = 980;
        var height = 290;

        var zero = [305, 160];

        var amplitude = [50, -70, 23];
        var amplitudeIdx = [1, 3, 5];

        var dot = [[]];
        dot[0] = [0,0];
        dot[1] = [90, amplitude[0]];
        dot[2] = [180,-5];
        dot[3] = [270, amplitude[1]];
        dot[4] = [360, -5];
        dot[5] = [450, amplitude[2]];
        dot[6] = [540, 0];

        var stage = new Konva.Stage({
          container: 'canvas-bridge',
          width: width,
          height: height
        });

        var layer = new Konva.Layer();

        var dotArr = [];
        for(i = 0; i < dot.length; i++){
            dotArr.push(dot[i][0]);
            dotArr.push(dot[i][1]);
        }

        var redLine = new Konva.Line({
          points: dotArr,
          stroke: 'red',
          strokeWidth: 1,
          lineCap: 'round',
          lineJoin: 'round',
          tension : 0.4
        });
        redLine.move({
            x : zero[0],
            y : zero[1]
        });
        layer.add(redLine);

        for(i = 0; i < dot.length; i++){
            var circ = new Konva.Circle({
                x: dot[i][0],
                y: dot[i][1],
                width: 8,
                height: 8,
                fill: 'red',
                id: 'rect',
                stroke: 'black',
                strokeWidth: 1
            });
            circ.move({
              x : zero[0],
              y : zero[1]
            });
            layer.add(circ);
        }

        for(x in amplitudeIdx){
            var text = new Konva.Text({
              x: dot[amplitudeIdx[x]][0],
              y: dot[amplitudeIdx[x]][1],
              text: 'x' + x,
              fontSize: 30,
              fontFamily: 'Calibri',
              fill: 'black'
            });
            text.move({
              x : zero[0],
              y : zero[1]
            });
            layer.add(text);
        }

        stage.add(layer);
    }

    $(document).ready(function(){
        initCanvas();

        $('#canvas-bridge canvas').css("background", 'url("{{ URL::to("assets/images/bridge_side.jpg") }}") no-repeat');
        $('#canvas-bridge canvas').css("background-size", '980px 290px');
    });
</script>
@stop