@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/css/realtime/lc.css'); }}">
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Cable Stress</h3>
        <div class="box-tools pull-right">
            <div id="canvas-navigator" class="btn-group p-b-5">
                <a href="{{ URL::to('sensor/lc'); }}" id="btn-side-view" class="btn btn-box-tool btn-default">Show Cable Force Graph</a>
            </div>
        </div><!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div id="my-graph"></div>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sensor</th>
                        <th>Cable Force (kN)</th>
                        <th>Cable Stress (MPa)</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

<script type="text/javascript">
    function initData () {
        Highcharts.setOptions({
                Global: {
                    useUTC: false
                }
            });
        $('#my-graph').highcharts({
            credits: {
                enabled: false
            },
            chart: {
                animation: Highcharts.svg, // don't animate in old IE
                events: {
                    load: function () {
                        data = getData();
                        var klabat = this.series[0];
                        var bunaken = this.series[1];
                        var myTable = $('#my-table').DataTable({
                            "sDom": "<'row'<'col-sm-12'f>r>" + "t" + "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "bPaginate": false,
                            "bInfo": false,
                            "bFilter": false,
                            "order": [[ 0, "asc" ]],
                            "aaData": data.aaData
                        });
                        klabat.setData(data.graphcs_1, true);
                        bunaken.setData(data.graphcs_2, true);
                        setInterval(function() {
                            data = getData();
                            series1.setData(data.graphcs_1, true);
                            series2.setData(data.graphcs_2, true);
                            // Redraw datatable
                            myTable.clear();
                            myTable.rows.add(data.aaData);
                            myTable.draw();
                        }, 60000);
                    }
                }
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'category'
            },
            yAxis: [{
                labels: {
                    format: '{value} MPa',
                    style: {
                        color: 'black'
                    }
                },
                title: {
                    text: 'Cable Stress (MPa)',
                    align: 'middle'
                },
                plotLines: [{
                    value: 3100,
                    color: 'red',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Cable Stress Threshold'
                    }
                }]
            }],
            tooltip: {
                formatter: function(){
                    return '<b>' + this.point.sensor + '</b><br/>' +
                    '<span>Cable Stress</span>: <b>' + Highcharts.numberFormat(this.point.y, 2) + ' </b>MPa<br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S.%L', this.point.datetime) + '<br/>' +
                    'Threshold: ' + this.point.threshold_1;
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Klabat',
                yAxis: 0,
                type: 'column'
            },{
                name: 'Bunaken',
                yAxis: 0,
                type: 'column'
            },],
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                document.location.href = "{{ URL::to('sensor/'.strtolower($sensorType->code))}}/"+this.sensor.toLowerCase();
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            }
        }, function (chart) {
             chart.renderer.image('{{ URL::to("assets/images/bridge_side_1.png"); }}', 87, 50, 800, 250)
                         .add();
        });
    }

    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/".strtolower($sensorType->code)."/realtime-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        return result;
    }

    $(document).ready(function(){
        initData();
    });
</script>
@stop