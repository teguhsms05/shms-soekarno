<?php

    $acc = Sensor::where('sensor_type_id', '=', 1)->get();
    $anmbi = Sensor::where('sensor_type_id', '=', 2)->get();
    $anmtri = Sensor::where('sensor_type_id', '=', 3)->get();
    $atrh = Sensor::where('sensor_type_id', '=', 4)->get();
    $lc = Sensor::where('sensor_type_id', '=', 5)->get();
    $ssmac = Sensor::where('sensor_type_id', '=', 6)->get();
    $strtr = Sensor::where('sensor_type_id', '=', 7)->get();
    $temp = Sensor::where('sensor_type_id', '=', 8)->get();
    $tilt = Sensor::where('sensor_type_id', '=', 9)->get();

?>

@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<style>
    #my-content-container {
        width: 100%;
        overflow-x: scroll;
    }
    #my-graph, #my-table {
        width: 900px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>
@stop

@section('window_title')
First Data
@stop

@section('page_title')
First Data
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-file"></i> History</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Sensor Data
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Last Data</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">

            <!-- Acc -->

            <h2>Accelerometer</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sensor Id</th>
                        <th>Datetime</th>
                        <th>X (Hz)</th>
                        <th>Y (Hz)</th>
                        <th>Z (Hz)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($acc as $row)
                        <?php
                            $firstData = DB::table($row->table_name.'_fft')->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->x, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->y, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->z, 2) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- Anmbi -->

            <h2>Anemometer Biaxial</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Speed (m/s)</th>
                        <th>Direction Azimuth (°)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($anmbi as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->speed, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->direction_azimuth, 2) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- Anmtri -->

            <h2>Anemometer Triaxial</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Speed (m/s)</th>
                        <th>Direction Azimuth (°)</th>
                        <th>Direction Elevation (°)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($anmtri as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->speed, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->direction_azimuth, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->direction_elevation, 2) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- ATRH -->

            <h2>ATRH</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Temperature (°C)</th>
                        <th>Humidity (%)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($atrh as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->temperature, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->humidity, 2) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- LC -->

            <h2>LoadCell</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Cable Force (kN)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($lc as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData ? $firstData->datetime : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->value, 2) : ''}}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- SSMAC -->

            <h2>Seismic</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Acceleration X (g)</th>
                        <th>Acceleration Y (g)</th>
                        <th>Acceleration Z (g)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($ssmac as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData ? $firstData->datetime : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->x, 8) : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->y, 8) : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->z, 8) : ''}}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- Strain -->

            <h2>Strain</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Stress Strain (MPa)</th>
                        <th>Stress Strain (MikroStrain)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($strtr as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData ? $firstData->datetime : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->value, 5) : ''}}</td>
                            <td> {{ $firstData ? AppUtil::reformatNumber($firstData->raw_value, 5) : ''}}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- Temperature -->

            <h2>Temp</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <td>Sensor</td>
                        <td>Datetime</td>
                        <th>Temperature (°C)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($temp as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->temperature, 2) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <!-- Tilt -->

            <h2>Tilt</h2>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sensor Id</th>
                        <th>Datetime</th>
                        <th>X (°)</th>
                        <th>Y (°)</th>
                        <th>Z (°)</th>
                        <th>Hires (°)</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($tilt as $row)
                        <?php
                            $firstData = DB::table($row->table_name)->orderBy('datetime', 'asc')->first();
                        ?>
                        @if($firstData)
                        <tr>
                            <td> {{ $row->code }}</td>
                            <td> {{ $firstData->datetime }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->x, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->y, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->z, 2) }}</td>
                            <td> {{ AppUtil::reformatNumber($firstData->hires, 5) }}</td>
                        </tr>
                        @else
                            No Data
                        @endif
                    @empty
                        <tr>
                            <td> No Sensor</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')
@stop