@extends('......admin.admin_template')

@section('css_plugins')
@stop

@section('window_title')
    Entity
@stop

@section('modul')
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>Settings</li>
        <li class="active">Entity</li>
    </ul>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-cog"></i> Entity
@stop

@section('toolbar')
    <div class="btn-toolbar p-b-15">
        <a href="{{ URL::to('/admin/setting/entity/create'); }}" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i> Create</a>
        <a href="javascript:window.print();" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Print</a>
    </div>
@stop

@section('content')
    <div id="my-alert" class="alert" role="alert" hidden="hidden">
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="my-data">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($entity as $row)
                    <tr>
                        <td>{{$row->code}}</td>
                        <td>{{$row->desc}}</td>
                        <td>
                            <a href="{{ URL::to('/admin/setting/entity/'.$row->id.'/edit'); }}" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                            <button type="button" class="btn btn-danger btn-delete" data-id="{{ $row->id }}"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $entity->links(); }}
@stop

@section('javascript_plugins')
<script>
    var colspan = $("#my-data").find("thead > tr > th").length;

    $( ".btn-delete" ).click(function() {
        var id = $(this).data('id');
        var myBtn = $(this);
        BootstrapDialog.show({
          type: BootstrapDialog.TYPE_WARNING,
          title: 'Confirmation',
          message: 'Do you want to delete this data?',
          buttons: [{
              label: 'No',
              hotkey: 78,
              action: function(dialog) {
                    dialog.close();
              }
          },{
              label: 'Yes',
              cssClass: 'btn-primary',
              hotkey: 89,
              autospin: true,
              action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);
                    $.ajax({
                        url: '{{ URL::to('/admin/setting/entity/'); }}/' + id + '/delete',
                        type: 'POST',
                        data: {
                            _token :'{{ csrf_token() }}'
                        },
                        success:function(msg){
                            dialog.close();
                            myBtn.closest('tr').remove();
                            showNoData('#my-data');
                            $("#my-alert").removeClass().addClass("m-b-15 alert alert-success alert-dismissible").html('<button type="button" class="close" data-hide="my-alert" id="btn-close-alert"><span aria-hidden="true">&times;</span></button> <strong>Success</strong> delete the data.').show();
                        }
                    });
              }
          }]
        });
    });

    $('#my-alert').on('click', '[data-hide]', function(){
        $(this).closest("#" + $(this).attr("data-hide")).hide();
    });

    $(document).ready(function() {
        showNoData('#my-data');
        @if(Session::get('alert_message_type'))
        $("#my-alert").removeClass().addClass("m-b-15 alert alert-{{ Session::get('alert_message_type') }} alert-dismissible").html('<button type="button" class="close" data-hide="my-alert" id="btn-close-alert"><span aria-hidden="true">&times;</span></button> {{ Session::get('alert_message') }}').show();
        @endif
    });
</script>
@stop