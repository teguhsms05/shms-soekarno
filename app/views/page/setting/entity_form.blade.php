@extends('......admin.admin_template')

@section('css_plugins')
@stop

@section('window_title')
    @if(isset($entity))
    Edit
    @else
    Create
    @endif
    Entity
@stop

@section('modul')
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>Settings</li>
        <li><a href="{{ URL::to('/admin/setting/entity'); }}">Entity</a></li>
        <li class="active">
            @if(isset($entity))
            Edit
            @else
            Create
            @endif
        </li>
    </ul>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-cog"></i>
    @if(isset($entity))
    Edit
    @else
    Create
    @endif
    Entity
@stop

@section('toolbar')
    <div class="btn-toolbar p-b-15">
        <a href="{{ URL::to('/admin/setting/entity'); }}" class="btn btn-danger"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
    </div>
@stop

@section('content')

    @if(isset($entity))
    {{ Form::model($entity, ['route' => ['admin.setting.entity.update', $entity->id], 'method' => 'patch', 'class' => 'form']) }}
    @else
    {{ Form::open(['route' => 'admin.setting.entity.store', 'class' => 'form']) }}
    @endif

    <div class="form-group">
        <label class="control-label" for="code"><span style="color: red;">*</span> Code</label>
        <div class="controls">
            {{ Form::text('code', Input::old('code'), array('class' => 'form-control', 'placeholder' => 'Code', 'autofocus' => 'autofocus')) }}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label" for="desc">Description</label>
        <div class="controls">
            {{ Form::textarea('desc', Input::old('desc'), array('class' => 'form-control', 'placeholder' => 'Description', 'rows' => 3)) }}
        </div>
    </div>

    @if($errors->has())
    <div class="form-group">
        <div class="controls">
            <div class="alert alert-danger alert-dismissable">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    <div class="form-group">
        <div class="controls">
            <input class="btn btn-primary" type="submit" value="Simpan">
            <input class="btn btn-default" type="reset" value="Reset">
        </div>
    </div>

    {{ Form::close() }}

@stop

@section('javascript_plugins')
@stop