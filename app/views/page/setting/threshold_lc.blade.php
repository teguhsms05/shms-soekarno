@extends('......template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
@stop

@section('window_title')
Load Cell Threshold
@stop

@section('page_title')
Setting
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li>Setting</li>
    <li>Load Cell Threshold and Properties</li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Load Cell Threshold and Properties
@stop

@section('content')
    {{--------------------Modification By: Muhammad F. Mudjiono-----------------------}}
    {{--Constant 1 field added: 03.05.2016--------------------------------------------}}
    {{--Constant 1 column added: 03.05.2016-------------------------------------------}}
<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Load Cell Threshold and Properties</h3>
  <div class="box-tools pull-right">
  </div>
</div><!-- /.box-header -->
<div class="box-body">

    {{ Form::open(['url' => 'setting/update-lc/', 'class' => 'form']) }}

    <div class="form-group">
        <label class="control-label" for="threshold_1">Sensor</label>
        <div class="controls">
            {{ Form::select('sensor_id',  array('' => 'All') + $sensorList , Input::old('sensor_id'), [ 'class' => 'select2 form-control', 'style' => 'width:100%;']) }}
        </div>
    </div>


    <div class="form-group">
        <label class="control-label" for="threshold_1">Level 1 Threshold</label>
        <div class="controls">
            {{ Form::text('threshold_1', Input::old('threshold_1'), array('id' => 'threshold_1', 'class' => 'form-control', 'placeholder' => '')) }}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label" for="threshold_2">Level 2 Threshold</label>
        <div class="controls">
            {{ Form::text('threshold_2', Input::old('threshold_2'), array('id' => 'threshold_2', 'class' => 'form-control', 'placeholder' => '')) }}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label" for="threshold_3">Level 3 Threshold</label>
        <div class="controls">
            {{ Form::text('threshold_3', Input::old('threshold_3'), array('id' => 'threshold_3', 'class' => 'form-control', 'placeholder' => '')) }}
        </div>
    </div>

    {{--Modified by: M. F. Mudjiono--Constant 1 Form--}}
    {{--<div class="form-group">--}}
        {{--<label class="control-label" for="threshold_3">Initial Force (kN)</label>--}}
        {{--<div class="controls">--}}
            {{--{{ Form::text('constant_1', Input::old('constant_1'), array('id' => 'constant_1', 'class' => 'form-control', 'placeholder' => '')) }}--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="form-group">
        <label class="control-label" for="threshold_3">Level 1 Threshold (MPa)</label>
        <div class="controls">
            {{ Form::text('constant_2', Input::old('constant_2'), array('id' => 'constant_2', 'class' => 'form-control', 'placeholder' => '')) }}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label" for="threshold_3">Level 2 Threshold (MPa)</label>
        <div class="controls">
            {{ Form::text('constant_3', Input::old('constant_3'), array('id' => 'constant_2', 'class' => 'form-control', 'placeholder' => '')) }}
        </div>
    </div>

    @if($errors->has())
    <div class="form-group">
        <div class="controls">
            <div class="alert alert-danger alert-dismissable">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif

    <div class="form-group">
        <div class="controls">
            <input class="btn btn-primary" type="submit" value="Update">
            <input class="btn btn-default" type="reset" value="Reset">
        </div>
    </div>

    {{ Form::close() }}

</div><!-- /.box-body -->
</div><!-- /.box -->

<div class="box box-default">
<div class="box-header with-border">
  <h3 class="box-title">Load Cell Threshold Data</h3>
  <div class="box-tools pull-right">
  </div>
</div><!-- /.box-header -->
<div class="box-body">
<table id="my-table" class="table table-bordered">
    <thead>
        <tr>
            <th>Sensor</th>
            <th>Level 1 Threshold</th>
            <th>Level 2 Threshold</th>
            <th>Level 3 Threshold</th>
            {{--Modified by M. F. Mudjiono--Constant 1 Column--}}
            {{--<th>Initial Force (kN)</th>--}}
            <th>Level 1 Threshold (MPa)</th>
            <th>Level 2 Threshold (MPa)</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sensors as $sensor)
        <tr>
            <th>{{ $sensor->code }}</th>
            <th>{{ AppUtil::reformatNumber($sensor->threshold_1) }}</th>
            <th>{{ AppUtil::reformatNumber($sensor->threshold_2) }}</th>
            <th>{{ AppUtil::reformatNumber($sensor->threshold_3) }}</th>
            {{--Modified by M. F. Mudjiono--Constant 1 Column--}}
            {{--<th>{{ AppUtil::reformatNumber($sensor->constant_1) }}</th>--}}
            <th>{{ AppUtil::reformatNumber($sensor->constant_2) }}</th>
            <th>{{ AppUtil::reformatNumber($sensor->constant_3) }}</th>
        </tr>
        @endforeach
    </tbody>
</table>
</div><!-- /.box-body -->
</div><!-- /.box -->

@stop

@section('javascript_additional')
@include('static/main_js')
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>
$(function() {
    $(".select2").select2();
    $(".select2").on("change", function (e) {
        idSensor = $(".select2").val();
        if($.isNumeric(idSensor)){
            $.getJSON( '{{ URL::to("setting/sensor-data") }}/' + idSensor, function( data ) {
                $("#threshold_1").val(data.sensor.threshold_1);
                $("#threshold_2").val(data.sensor.threshold_2);
                $("#threshold_3").val(data.sensor.threshold_3);
//                $("#constant_1").val(data.sensor.constant_1);
                $("#constant_2").val(data.sensor.constant_2);
                $("#constant_3").val(data.sensor.constant_3);
            });
        }
    });
    $('#my-table').DataTable({
        "bPaginate": false,
        "order": [[ 0, "desc" ]]
    });
});
</script>
@stop