@extends('......template')

@section('css_plugins')
@stop

@section('window_title')
    User Management
@stop

@section('page_title')
    Setting
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Setting</li>
        <li><a href="{{ URL::to('user') }}"> User Management</a></li>
    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-cog"></i> User Management
@stop

@section('content')

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">User</h3>
            <div class="box-tools pull-right">
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
                <a href="{{ URL::to('setting/user/create'); }}" class="btn btn-primary m-10 pull-right"><i class="glyphicon glyphicon-plus"></i> Create</a>
                <div class="clearfix"></div>
                <div id="my-alert" class="alert" role="alert" hidden="hidden"></div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="my-data">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $row)
                                <tr>
                                    <td>{{$row->username}}</td>
                                    <td>{{$row->role}}</td>
                                    <td>
                                        <a href="{{ URL::to('setting/user/'.$row->id.'/edit'); }}" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                                        <a href="{{ URL::to('setting/user/'.$row->id.'/destroy'); }}" class="btn btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $user->links(); }}
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop

@section('javascript_additional')
    @include('static/main_js')
    <script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function() {
            var table = $('#my-table').DataTable({
                "bPaginate": true,
                "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
                "order": [[ 0, "desc" ]]
            });

            @if(Session::get('alert_message_type'))
            $("#my-alert").removeClass().addClass("m-b-15 alert alert-{{ Session::get('alert_message_type') }} alert-dismissible").html('<button type="button" class="close" data-hide="my-alert" id="btn-close-alert"><span aria-hidden="true">&times;</span></button> {{ Session::get('alert_message') }}').show();
            @endif
        });
    </script>
@stop