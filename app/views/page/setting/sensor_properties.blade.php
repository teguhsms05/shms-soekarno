@extends('......template')

@section('css_additional')
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
    <style>
        .formulir{
            padding: 10px;
        }
    </style>
@stop

@section('window_title')
    Accelerometer Threshold
@stop

@section('page_title')
    Setting
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><i class="fa fa-dashboard"></i> Setting</li>
        <li><a href="{{ URL::to('sensor/') }}"> Accelerometer Threshold</a></li>
    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Accelerometer Threshold
@stop

@section('content')
    <div class="box box-default">
        {{ Form::open(['url' => 'setting/update-properties/', 'class' => 'form']) }}
        <div class="box-header with-border">
            <h3 class="box-title">Accelerometer Threshold Data</h3>
            <div class="box-tools pull-right">
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="my-table" class="table table-bordered">
                <thead>
                <tr>
                    <th>Sensor ID</th>
                    <th>Sensor</th>
                    <th>Sensor Code</th>
                    <th>Sensor Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sensorsProperties as $key=>$sensorsProperty)
                    <tr>
                        <th><input type="hidden" name="id[{{$key}}]" value="{{ $sensorsProperty->id }}">{{ $sensorsProperty->id }}</th>
                        <th>{{ $sensorsProperty->table_name }}</th>
                        <th><input type="text" name="code[{{$key}}]" value="{{ $sensorsProperty->code }}"></th>
                        <th><input type="text" name="desc[{{$key}}]" value="{{ $sensorsProperty->desc }}"></th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="form-group formulir">
            <div class="controls">
                <input class="btn btn-primary" type="submit" value="Save" id="button">
            </div>
        </div>
        {{ Form::close() }}

        @if($message == true)
        <div class="form-group formulir">
            <div class="controls">
                <div class="alert alert-info alert-dismissable">
                    <ul>
                        Data has been updated.
                    </ul>
                </div>
            </div>
        </div>
        @endif
    </div><!-- /.box -->
@stop

@section('javascript_additional')
    @include('static/main_js')
    <script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function() {
            var table = $('#my-table').DataTable({
                "bPaginate": true,
                "lengthMenu": [ [10, 50, 100, -1], [10, 50, 100, "All"] ],
                "order": [[ 0, "desc" ]]
            });

            $("button").click( function() {
                var data = table.$('input').serialize();
                alert(
                        "The following data would have been submitted to the server: \n\n"+
                        data.substr( 0, 120 )+'...'
                );
                return false;
            } );
        });
    </script>
@stop