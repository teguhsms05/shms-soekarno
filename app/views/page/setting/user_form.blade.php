@extends('......template')

@section('css_plugins')
@stop

@section('window_title')
    User Management
@stop

@section('page_title')
    Setting
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Setting</li>
        <li><a href="{{ URL::to('setting/user') }}"> User Management</a></li>
        <li>
            @if(isset($user))
                Edit
            @else
                Create
            @endif
        </li>
    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-cog"></i> User Management -
    @if(isset($user))
        Edit
    @else
        Create
    @endif
@stop

@section('toolbar')
    <div class="btn-toolbar p-b-15">
        <a href="{{ URL::to('setting/user/create'); }}" class="btn btn-primary"><i class="glyphicon glyphicon-plus-sign"></i> Create</a>
        <a href="javascript:window.print();" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Print</a>
    </div>
@stop

@section('content')

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
            @if(isset($user))
                Edit
            @else
                Create
            @endif
             - User</h3>
            <div class="box-tools pull-right">
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">

            @if(isset($user))
            {{ Form::model($user, ['route' => ['setting.user.update', $user->id], 'method' => 'patch', 'class' => 'form']) }}
            @else
            {{ Form::open(['route' => 'setting.user.store', 'class' => 'form']) }}
            @endif

            <div class="form-group">
                <label class="control-label" for="username"><span style="color: red;">*</span> Username</label>
                <div class="controls">
                    {{ Form::text('username', Input::old('username'), array('class' => 'form-control', 'placeholder' => 'Username', 'autofocus' => 'autofocus')) }}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="password"><span style="color: red;">*</span> Password</label>
                <div class="controls">
                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="password_confirmation"><span style="color: red;">*</span> Password Confirmation</label>
                <div class="controls">
                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Password Confirmation')) }}
                </div>
            </div>

            <div class="form-group">
                    <label class="control-label"><span style="color: red;">*</span> Role</label>
                    <div class="controls">
                        {{ Form::radio('role', 'ADMIN', (Input::old('role') == 'ADMIN'), array('id'=>'radio_admin', 'class'=>'radio_inline')) }}
                        {{ Form::label('radio_admin', 'Admin', array('class'=>'m-r-20') ) }}
                        {{ Form::radio('role', 'SUPER', (Input::old('role') == 'SUPER'), array('id'=>'radio_super', 'class'=>'radio_inline')) }}
                        {{ Form::label('radio_super', 'Super', array('class'=>'m-r-20') ) }}
                        {{ Form::radio('role', 'OPERATOR', (Input::old('role') == 'OPERATOR' || Input::old('role') == ''), array('id'=>'radio_operator', 'class'=>'radio_inline')) }}
                        {{ Form::label('radio_customer', 'Operator') }}
                    </div>
                </div>

            @if($errors->has())
            <div class="form-group">
                <div class="controls">
                    <div class="alert alert-danger alert-dismissable">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif

            <div class="form-group">
                <div class="controls">
                    <input class="btn btn-primary" type="submit" value="Simpan">
                    <input class="btn btn-default" type="reset" value="Reset">
                </div>
            </div>

            {{ Form::close() }}

        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop

@section('javascript_additional')
    @include('static/main_js')
@stop