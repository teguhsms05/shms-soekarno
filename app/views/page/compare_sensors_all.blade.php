@extends('...template')

@section('css_additional')
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
    <style>
        #my-content-container {
            width: 100%;
            overflow-x: scroll;
        }
        #my-graph, #my-table {
            width: 900px;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
@stop

@section('window_title')
    {{ $sensorType->code }}
@stop

@section('page_title')
    {{ $sensorType->code }}
    <small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Filter Data</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <a href="{{ URL::to('compare-sensors') }}" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-refresh"></i></a>
                <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <!-- form start -->
        <form action="" method="get" id="request-data">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>From</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {{
                                    Form::text('from', $from, array(
                                        'class' => 'form-control datetime pull-right',
                                        'placeholder' => 'From',
                                        'id' => 'from'
                                    ))
                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>To</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {{
                                    Form::text('to', $to, array(
                                        'class' => 'form-control datetime pull-right',
                                        'placeholder' => 'To',
                                        'id' => 'to'
                                    ))
                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Anemometer Biaxial</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'anmbi[]',
                                        $anmbi,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )
                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>Anemometer Biaxial Fields</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'anmbiFields[]',
                                        $anmbiFields,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Anemometer Triaxial</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'anmtri[]',
                                        $anmtri,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>Anemometer Triaxial Fields</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'anmtriFields[]',
                                        $anmtriFields,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Ambient Temperature and Relative Humidity (ATRH)</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'atrh[]',
                                        $atrh,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>ATRH Fields</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'atrhFields[]',
                                        $atrhFields,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Load Cell</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'lc[]',
                                        $lc,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>Load Cell Fields</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'lcFields[]',
                                        $lcFields,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Strain</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'strtr[]',
                                        $strtr,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Temperature</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'temp[]',
                                        $temp,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Tilt</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'tilt[]',
                                        $tilt,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                        <div class="col-md-6">
                            <label>Tilt Field</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                {{
                                    Form::select(
                                        'tiltFields[]',
                                        $tiltFields,
                                        array(),
                                        [
                                            'id' => 'sensors',
                                            'multiple' => 'multiple' ,
                                            'class' => 'select2',
                                            'style' => "width: 100%;"
                                        ]

                                    )

                                }}
                            </div><!-- /.input group -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <input class="btn btn-primary pull-right" type="submit" value="update">
            </div>
        </form><!-- /form -->
    </div><!-- /.box -->

    <div id="result-container">

    </div>

    <script type="text/template" id="graph-template">
        <div class="box" id="my-box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="my-content-container">
                    <div id="my-graph"></div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div><!-- /.box -->
    </script>

    @stop

    @section('javascript_additional')
    @include('static/main_js')

            <!-- select2 -->
    <script src="{{ URL::to('assets/plugins/select2/select2.min.js'); }}"></script>

    <!-- date-range-picker -->
    <script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <!-- highcharts -->
    <script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

    <script src="{{ URL::to('assets/plugins/underscore/underscore-min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/backbone/backbone-min.js') }}"></script>
    <script>

        function initGraph (graph_container, data) {
            Highcharts.setOptions({
                Global: {
                    useUTC: false
                }
            });
            $(graph_container).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    zoomType: 'x'
                },
                legend:{
//                symbolHeight: 5,
                    symbolWidth: 3,
                    symbolRadius: 50
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        millisecond: '%H:%M:%S.%L',
                        second: '%H:%M:%S',
                        minute: '%H:%M',
                        hour: '%H:%M',
                        day: '%e. %b',
                        week: '%e. %b',
                        month: '%b \'%y',
                        year: '%Y'
                    },
                    crosshair: true
                }],
                yAxis: data.yAxis,
                series: data.series,
                tooltip: {
                    shared: true,
                    xDateFormat: '%A, %b %e, %H:%M:%S.%L'
                },
                plotOptions: {
                    spline: {
//                    marker: {
//                        radius: 5,
//                        symbol: 'circle'
//                    },
                        lineWidth: 0.7,
                        states: {
                            hover: {
                                lineWidth: 0.9
                            }
                        }
                    }
                }
            });
        }

        $(document).ready(function(){
            $('.datetime').daterangepicker({
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                timePickerSeconds: true,
                autoApply: true,
                autoUpdateInput: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            });
            $(".select2").select2();
        });

        $("#request-data").submit(function(e) {
            $('#result-container').html('');

            var cont = '';
            var compiled = _.template($('#graph-template').html(), {variable: 'data'})({
                title: 'Compared Data',
                cont: cont
            });

            $('#result-container').append(compiled);

            url = "{{ URL::to("compare-sensors/data") }}";
            $.ajax({
                type: "GET",
                data: $('#request-data').serialize(),
                url: url,
                success: function(data)
                {
                    if(data != false){
                        initGraph('#my-graph', data);
                        $('#my-box .overlay').remove();
                    }else{
                        $.bootstrapGrowl("Sensor(s) can't be empty!", {
                            ele: 'body', // which element to append to
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                            align: 'center', // ('left', 'right', or 'center')
                            width: 250, // (integer, or 'auto')
                            delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: true, // If true then will display a cross to close the popup.
                            stackup_spacing: 10 // spacing between consecutively stacked growls.
                        });
                        $('#result-container').html('');
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>
@stop