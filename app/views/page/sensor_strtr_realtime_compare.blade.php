<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Filter Data</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->

    <form action="" method="get" id="request-data">
        <div class="box-body">
            <div class="form-group">
                <div class="col-md-6">
                    <label>From</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('from', $from, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'From',
                                'id' => 'from'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>To</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('to', $to, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'To',
                                'id' => 'to'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>Strain</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-balance-scale"></i>
                        </div>
                        {{
                            Form::select(
                                'sensors[]',
                                $sensorsStrtr,
                                array(),
                                [
                                    'id' => 'sensors',
                                    'multiple' => 'multiple' ,
                                    'class' => 'select2',
                                    'style' => "width: 100%;"
                                ]

                            )

                        }}
                    </div><!-- /.input group -->
                </div>
                @if($tilt)
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <label>Tilt</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-balance-scale"></i>
                            </div>
                            {{
                                Form::select(
                                    'sensors[]',
                                    $sensorsTilt,
                                    array(),
                                    [
                                        'id' => 'sensors',
                                        'multiple' => 'multiple' ,
                                        'class' => 'select2',
                                        'style' => "width: 100%;"
                                    ]

                                )

                            }}
                        </div><!-- /.input group -->
                    </div>
                    <div class="col-md-6">
                        <label>Tilt sensor(s) field</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-table"></i>
                            </div>
                            {{
                                Form::select(
                                    'field',
                                    $fields,
                                    array(),
                                    [
                                        'id' => 'sensors',
                                        'class' => 'select2',
                                        'style' => "width: 100%;"
                                    ]
                                )
                            }}
                        </div><!-- /.input group -->
                    </div>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="update">
        </div>
    </form><!-- /form -->

</div><!-- /.box -->

<div id="result-container">

</div>

<script type="text/template" id="graph-template">
    <div class="box" id="my-box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="my-content-container">
                <div id="my-graph"></div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div><!-- /.box -->
</script>