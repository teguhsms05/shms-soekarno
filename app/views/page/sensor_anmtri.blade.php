@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/css/realtime/anmtri.css'); }}">
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Realtime Monitor</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div id="my-content">
                <div id="my-canvas">
                    <a href="{{ URL::to('sensor/anmtri/anmtri02') }}">
                        <div id="my-wind-direction-chart-2"></div>
                    </a>

                    <a href="{{ URL::to('sensor/anmtri/anmtri01') }}">
                        <div id="my-wind-direction-chart-1"></div>
                    </a>
                    <div class="my-wind-speed-container" id="my-wind-speed-2">
                        <div class="title">Wind Speed - ANMTRI02</div>
                        <div class="input-group">
                            {{ Form::text('input-speed-2', 0, array('id' => 'input-speed-2', 'class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                            <span class="input-group-addon big">m/s</span>
                        </div>
                    </div>
                    <div class="my-wind-speed-container" id="my-wind-speed-1">
                        <div class="title">Wind Speed - ANMTRI01</div>
                        <div class="input-group">
                            {{ Form::text('input-speed-1', 0, array('id' => 'input-speed-1', 'class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                            <span class="input-group-addon big">m/s</span>
                        </div>
                    </div>
                    <div class="my-aoa-container my-aoa-container-2" id="my-aoa-container-2">
                        <div class="title">Angle of Attack - ANMTRI02</div>
                        <a href="{{ URL::to('sensor/anmtri/anmtri02') }}">
                            <div id="my-aoa-chart-2" class="chart"></div>
                        </a>
                    </div>
                    <div class="my-aoa-container my-aoa-container-1" id="my-aoa-container-1">
                        <div class="title">Angle of Attack - ANMTRI01</div>
                        <a href="{{ URL::to('sensor/anmtri/anmtri01') }}">
                            <div id="my-aoa-chart-1" class="chart"></div>
                        </a>
                    </div>
                   <div id="my-datetime-1" {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                       {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                   </div>
                   <div id="my-datetime-2" {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                       {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->

@stop

@section('javascript_additional')

@include('static/main_js')
<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/highcharts-more.js') }}"></script>
<!-- script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script -->

<script>
function createWindDirectionGauge(element, sensor_name, section){
    var result = new Highcharts.Chart({
        credits: {
            enabled: false
        },
        chart: {
            renderTo: element,
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            backgroundColor:'rgba(255, 255, 255, 0.1)'
        },

        title: {
            text: ''// Wind Direction
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: [{
              backgroundColor: 'rgba(255, 255, 255, 0.1)',
              borderWidth: 0,
              outerRadius: '0%',
              innerRadius: '100%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 360,
            tickWidth: 1,
            tickPosition: 'outside',
            tickLength: 10,
            tickColor: '#999',
            tickInterval: 45,
            labels: {
                rotation: 0,
                formatter:function(){
                    if(this.value == 360) { return 'N'; }
                    else if(this.value == 45) { return 'NE'; }
                    else if(this.value == 90) { return 'E'; }
                    else if(this.value == 135) { return 'SE'; }
                    else if(this.value == 180) { return 'S'; }
                    else if(this.value == 225) { return 'SW'; }
                    else if(this.value == 270) { return 'W'; }
                    else if(this.value == 315) { return 'NW'; }
                },
                style: {
                    color: "contrast",
                    fontSize: "11px",
                    fontWeight: "bold"
                }
            },
            title: {
                text: section + ' - ' + sensor_name.toUpperCase(),// Middle Text
                y: -60,
                style: {
                    color: "contrast",
                    fontSize: "14px"
                }
            }
        },

        plotOptions: {
            gauge: {
                dial: {
                    radius: '55%',
                    backgroundColor: 'red',
                    borderColor: 'red',
                    borderWidth: 1,
                    baseWidth: 1,
                    topWidth: 10,
                    baseLength: '0%', // of radius
                    rearLength: '0%'
                }
            }
        },

        series: [{
            name: 'Wind Direction ' + sensor_name.toUpperCase(),
            data: [0],
            tooltip: {
                valueSuffix: ' °'
            }
        }]
    });
    return result;
}

function createAngleOfAttackGauge(element, sensor_name, position){
    startAngle = -180;
    endAngle = 0;
    reversed = false;
    if(position == 'r'){
        startAngle = 0;
        endAngle = 180;
        reversed = true;
    }
    var result = {
        credits: {
            enabled: false
        },
        chart: {
            renderTo: element,
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            backgroundColor:'rgba(255, 255, 255, 0.1)'
        },

        title: {
            text: ''// AOA
        },

        pane: {
            startAngle: startAngle,
            endAngle: endAngle,
            background: [{
                backgroundColor: 'rgba(255, 255, 255, 0.1)',
                borderWidth: 0,
                outerRadius: '0%',
                innerRadius: '100%'
            }]
        },

        // the value axis
        yAxis: {
            reversed: reversed,
            min: -90,
            max: 90,
            tickWidth: 1,
            tickPosition: 'outside',
            tickLength: 10,
            tickColor: '#999',
            tickInterval:45,
            labels: {
                rotation: 0,
                formatter:function(){
                    if(position = 'l'){
                        if(this.value == 0) { return '0'; }
                        else if(this.value == 90) { return '90'; }
                        else if(this.value == -90) { return '-90'; }
                    }else{
                        if(this.value == 0) { return '0'; }
                        else if(this.value == 90) { return '-90'; }
                        else if(this.value == -90) { return '90'; }
                    }
                },
                style: {
                    fontWeight: 'bold',
                    color: "contrast",
                    fontSize: "11px",
                    fontWeight: "bold"
                }
            },
            title: {
                text: '' // Middle Text
            }
        },

        plotOptions: {
            gauge: {
                dial: {
                    radius: '55%',
                    backgroundColor: 'red',
                    borderColor: 'red',
                    borderWidth: 1,
                    baseWidth: 1,
                    topWidth: 10,
                    baseLength: '0%', // of radius
                    rearLength: '0%'
                }
            }
        },

        series: [{
            name: 'Angle of Attack ' + sensor_name,
            data: [0],
            tooltip: {
                valueSuffix: ' °'
            }
        }]
    };
    return result;
}

$(function () {
    var direction = ['l', 'r'];
    var myWindDirection = [];
    myWindDirection["anmtri01"] = createWindDirectionGauge('my-wind-direction-chart-1', 'anmtri01', 'S5');
    myWindDirection["anmtri02"] = createWindDirectionGauge('my-wind-direction-chart-2', 'anmtri02', 'N7');
    var myAOA = [];
    myAOA["anmtri01"] = new Highcharts.Chart(createAngleOfAttackGauge('my-aoa-chart-1', 'ANMTRI01', 'l'));
    myAOA["anmtri02"] = new Highcharts.Chart(createAngleOfAttackGauge('my-aoa-chart-2', 'ANMTRI02', 'r'));
    loadData(myWindDirection, myAOA);
    setInterval(function () {
        loadData(myWindDirection, myAOA);
    }, 60000);

    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/".strtolower($sensorType->code)."/realtime-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        return result;
    }

    function loadData(myWindDirection, myAOA){
        var data = getData();
        var sensors = ["anmtri01", "anmtri02"];
        for(i = 1; i <= 2; i++){
            idx = i-1;
            if(data.anmtri[sensors[idx]]){
                $windTmp1 = 'r';
                if(myWindDirection[sensors[idx]].series[0].points[0].y > 180){
                    $windTmp1 = 'l';
                }
                // Update highchart for azimuth direction (wind direction)
                myWindDirection[sensors[idx]].series[0].points[0].update(data.anmtri[sensors[idx]].direction_azimuth);
                // Set the position of the needle (left or right)
                $newWindPosition = 'r';
                if(data.anmtri[sensors[idx]].direction_azimuth > 180){
                    $newWindPosition = 'l';
                }
                // Reload the gauge if any change on wind direction.
                if($windTmp1 == $newWindPosition){
                    myAOA[sensors[idx]] = new Highcharts.Chart(createAngleOfAttackGauge('my-aoa-chart-' + i, sensors[idx], $newWindPosition));
                    myAOA[sensors[idx]].yAxis[0].plotLinesAndBands[0].svgElem.attr({
                        fill: data.anmtri[sensors[idx]].color
                    });
                    myAOA[sensors[idx]].series[0].points[0].update(data.anmtri[sensors[idx]].direction_elevation);
                }else{
                    myAOA[sensors[idx]] = new Highcharts.Chart(createAngleOfAttackGauge('my-aoa-chart-' + i, sensors[idx], $newWindPosition));
                    myAOA[sensors[idx]].series[0].points[0].update(data.anmtri[sensors[idx]].direction_elevation);
                    myAOA[sensors[idx]].yAxis[0].plotLinesAndBands[0].svgElem.attr({
                        fill: data.anmtri[sensors[idx]].color
                    });
                }
                // Set the color of the gauge depends on warning
                myWindDirection[sensors[idx]].yAxis[0].plotLinesAndBands[0].svgElem.attr({
                    fill: data.anmtri[sensors[idx]].color
                });
                // input field for speed
                $('#input-speed-' + i).val(data.anmtri[sensors[idx]].speed);
                // input field for datetime
                $('#my-datetime-' + i + ' input').val(data.anmtri[sensors[idx]].datetime);
            }

        }

    }

});

</script>

@stop