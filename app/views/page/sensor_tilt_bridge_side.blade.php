@extends('...template')

@section('css_additional')
    <link rel="stylesheet" href="{{ URL::to('assets/css/realtime/tilt_deck.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
    <style>
        #my-content-container {
            width: 100%;
            overflow-x: scroll;
        }
        #my-graph, #my-table {
            width: 900px;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
@stop

@section('window_title')
    {{ $sensorType->code }}
@stop

@section('page_title')
    {{ $sensorType->code }}
    <small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
        <li>Deck ({{ $section->code }})</li>
    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Realtime Monitor</h3>
            <div class="box-tools pull-right">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="my-content-container">
                <div id="my-content">
                    <div id="my-canvas">
                        <div id="my-background">
                            <a id="tilt0" class="tilt">
                                <span class="hide">TILT0</span>
                            </a>
                            <a id="tilt1" class="tilt">
                                <span class="hide">TILT1</span>
                            </a>
                        </div>


                        <div id="bunaken-desc" class="pic-desc">Bunaken<br><b>(West)</b></div>
                        <div id="klabat-desc" class="pic-desc">Klabat<br><b>(East)</b></div>
                        <div id="view-desc" class="pic-desc">{{ $section->code }}</div>

                        <!-- Strain -->

                        @for($i=0; $i<=1; $i++)

                            <div class="my-input-container" id="tilt{{$i}}-input">
                                <div class="title">TILT{{$i}}</div>
                                <div class="input-group">
                                    {{ Form::text('input-tilt'.$i, 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">°</span>
                                </div>
                            </div>

                        @endfor

                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div><!-- /.box -->
    @include('page.sensor_tilt_realtime_compare')
@stop

@section('javascript_additional')
    @include('static/main_js')

    <script src="{{ URL::to('assets/plugins/select2/select2.min.js'); }}"></script>

    <!-- date-range-picker -->
    <script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <!-- highcharts -->
    <script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

    <script src="{{ URL::to('assets/plugins/underscore/underscore-min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/backbone/backbone-min.js') }}"></script>
    <script>
        function initGraph (graph_container, data) {
            Highcharts.setOptions({
                    Global: {
                        useUTC: false
                    }
                });
            $(graph_container).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    zoomType: 'x'
                },
//            legend:{
//              symbolHeight: 5,
//              symbolWidth: 5,
//                symbolRadius: 50
//            },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        millisecond: '%H:%M:%S.%L',
                        second: '%H:%M:%S',
                        minute: '%H:%M',
                        hour: '%H:%M',
                        day: '%e. %b',
                        week: '%e. %b',
                        month: '%b \'%y',
                        year: '%Y'
                    },
                    crosshair: true
                }],
                yAxis: data.yAxis,
                series: data.series,
                tooltip: {
                    shared: true,
                    xDateFormat: '%A, %b %e, %H:%M:%S.%L'
                },
                plotOptions: {
                    spline: {
                        marker: {
                            radius: 5,
                            symbol: 'circle'
                        },
                        lineWidth: 0.7,
                        states: {
                            hover: {
                                lineWidth: 0.9
                            }
                        }
                    }
                }
            });
        }

        function rotate(el, deg){
            $(el).css('-webkit-transform', 'rotate('+ deg +'deg)');
            $(el).css('-moz-transform', 'rotate('+ deg +'deg)');
            $(el).css('-ms-transform', 'rotate('+ deg +'deg)');
            $(el).css('-o-transform', 'rotate('+ deg +'deg)');
            $(el).css('transform', 'rotate('+ deg +'deg)');
        }

        $(".tilt, .tilt").mouseenter(
                function() {
                    $(this).children('span').removeClass('hide');
                }
        ).mouseleave(
                function() {
                    $(this).children('span').addClass('hide');
                }
        );
        $(function(){
            rotate('#my-background', 0);
            loadData();
            setInterval(function () {
                loadData();
            }, 6000);

            function getData(){
                var result = function () {
                    var tmp = 0;
                    $.ajax({
                        'async': false,
                        'type': "GET",
                        'global': false,
                        'dataType': 'json',
                        'url': '{{ URL::to("sensor/tilt/deck-data/".$section->code) }}',
                        'success': function (data) {
                            tmp = data;
                        }
                    });
                    return tmp;
                }();
                return result;
            }

            function loadData(){
                var data = getData();
                var tilt = [0,1];
                for(i in tilt){
                    $('#tilt'+tilt[i]+'-input input').val(data.tilt[tilt[i]].hires);
                    $('#tilt'+tilt[i]).css('background', data.tilt[tilt[i]].color);
                    $('#tilt'+tilt[i]+' span').text(data.tilt[tilt[i]].code);
                    $('#tilt'+tilt[i]+'-input .title').text(data.tilt[tilt[i]].code);
                    $('#tilt'+tilt[i]).attr("href", "{{ URL::to('sensor/tilt') }}/" + data.tilt[tilt[i]].code.toLowerCase());
                }
            }
            $('.datetime').daterangepicker({
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                timePickerSeconds: true,
                autoApply: true,
                autoUpdateInput: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            });
            $(".select2").select2();

        });
        $("#request-data").submit(function(e) {
            $('#result-container').html('');

            var cont = '';
            var compiled = _.template($('#graph-template').html(), {variable: 'data'})({
                title: 'Compared Data',
                cont: cont
            });

            $('#result-container').append(compiled);

            url = "{{ URL::to("sensor/tilt/compare-sensors-data-json") }}";
            $.ajax({
                type: "GET",
                data: $('#request-data').serialize(),
                url: url,
                success: function(data)
                {
                    if(data != false){
                        initGraph('#my-graph', data);
                        $('#my-box .overlay').remove();
                    }else{
                        $.bootstrapGrowl("Sensor(s) can't be empty!", {
                            ele: 'body', // which element to append to
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                            align: 'center', // ('left', 'right', or 'center')
                            width: 250, // (integer, or 'auto')
                            delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: true, // If true then will display a cross to close the popup.
                            stackup_spacing: 10 // spacing between consecutively stacked growls.
                        });
                        $('#result-container').html('');
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    </script>
@stop