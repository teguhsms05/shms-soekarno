@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/css/realtime/temp.css'); }}">
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Realtime Monitor</h3>
            <div class="box-tools pull-right">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="my-content-container">
                <div id="my-content">
                    <div id="my-canvas">
                        <div id="temp-container-1" class="temp-container">
                            <div class="title">S6 - Deck - Temperature 01</div>
                            <div class="chart" id="temp-chart-1"></div>
                            <div class="temp">
                                <div class="temp-title">Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <div id="temp-container-2" class="temp-container">
                            <div class="title">S6 - Girder - Temperature 02</div>
                            <div class="chart" id="temp-chart-2"></div>
                            <div class="temp">
                                <div class="temp-title">Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <div id="temp-container-3" class="temp-container">
                            <div class="title">N8 - Girder - Temperature 03</div>
                            <div class="chart" id="temp-chart-3"></div>
                            <div class="temp">
                                <div class="temp-title">Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <div id="temp-container-4" class="temp-container">
                            <div class="title">N8 - Deck - Temperature 04</div>
                            <div class="chart" id="temp-chart-4"></div>
                            <div class="temp">
                                <div class="temp-title">Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <div id="atrh-container-1" class="atrh-container">
                            <div class="title">S3 - ATRH01</div>
                            <div class="temp">
                                <div class="temp-title">Ambient Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <div id="atrh-container-2" class="atrh-container">
                            <div class="title">N5 - ATRH02</div>
                            <div class="temp">
                                <div class="temp-title">Ambient Temperature</div>
                                <div class="input-group">
                                    {{ Form::text('temp', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                    <span class="input-group-addon big">&deg; C</span>
                                </div>
                            </div>
                        </div>
                        <a href="{{ URL::to('sensor/temp/temp03') }}" id="temp03" class="temperature">\
                            <div class="symbol-desc hide"><div class="section">N8 - Girder</div>Temperature 03</div>
                        </a>
                        <a href="{{ URL::to('sensor/temp/temp04') }}" id="temp04" class="temperature">
                            <div class="symbol-desc hide"><div class="section">N8 - Deck</div>Temperature 04</div>
                        </a>
                        <a href="{{ URL::to('sensor/temp/temp01') }}" id="temp01" class="temperature">
                            <div class="symbol-desc hide"><div class="section">S6 - Deck</div>Temperature 01</div>
                        </a>
                        <a href="{{ URL::to('sensor/temp/temp02') }}" id="temp02" class="temperature">
                            <div class="symbol-desc hide"><div class="section">S6 - Girder</div>Temperature 02</div>
                        </a>
                        <a href="{{ URL::to('sensor/atrh/atrh01') }}" id="atrh01" class="atrh">
                            <div class="symbol-desc hide"><div class="section">S3</div>ATRH 01</div>
                        </a>
                        <a href="{{ URL::to('sensor/atrh/atrh02') }}" id="atrh02" class="atrh">
                            <div class="symbol-desc hide"><div class="section">N5</div>ATRH 02</div>
                        </a>

                        <div id="temp01-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                        <div id="temp02-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                        <div id="temp03-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                        <div id="temp04-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                        <div id="atrh01-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                        <div id="atrh02-datetime"  {{ !Input::get("dt") ? 'style="display:none;"' : '' }}>
                           {{ Form::text('input-datetime', 0, array('class' => 'form-control', 'placeholder' => '', 'disabled' => 'disabled')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/jcanvas/jcanvas.min.js') }}"></script>
<script>

function temperature(container){
    var result =  new Highcharts.Chart({
        chart: {
            renderTo: container,
            marginBottom: 20,
            marginRight: 39,
            backgroundColor: 'rgba(255, 255, 255, 0.1)'
        },
        series: [{
            data: [100],
            type: 'column',
            pointWidth: 5,
            threshold: -40,
            borderWidth: 0,
            name: 'Temp',
            color: '#cc0000'
        }],
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            labels: {
                enabled: false
            },
            lineWidth: 0,
            tickWidth: 0
        },
        yAxis: {
            min: 0,
            max: 100,
            tickInterval: 25,
            minPadding: 0,
            maxPadding: 0,
            startOnTick: 0,
            endOnTick: 100,
            title: {
                text: ''
            },
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            tickWidth: 1,
            minorTickWidth: 1
        },
        title: {
            text: ''
        }

    }, function (chart) {
        //URL,LEFT, TOP, WIDTH, HEIGHT
        chart.renderer.image('{{ URL::to("assets/images/blank_thermometer.png"); }}', 45, 7, 48, 85)
                    .add();

    });

    return result;
}

$(function () {

    var temp = [];
    for(i=1; i<=4; i++){
        tmp = temperature("temp-chart-"+i);
        temp.push(tmp);
    }

    loadData(temp);
    setInterval(function () {
        loadData(temp);
    }, 6000);


    $('.temperature, .atrh').bind('touchstart', function(e) {
        $(this).children('.symbol-desc').removeClass('hide');
    }).bind('touchstart', function(e) {
        $(this).children('.symbol-desc').addClass('hide');
    });
    $(".temperature, .atrh").mouseenter(function() {
        $(this).children('.symbol-desc').removeClass('hide');
    }).mouseleave(function() {
        $(this).children('.symbol-desc').addClass('hide');
    });

    function getData(){
        var result = function () {
            var tmp = 0;
            $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': '{{ URL::to("sensor/".strtolower($sensorType->code)."/realtime-data") }}',
                    'success': function (data) {
                        tmp = data;
                    }
                });
            return tmp;
        }();
        return result;
    }

    function loadData(temp){
        var data = getData();
        for(i=0; i<temp.length; i++){
                id = i+1;
            if(data.temp[i]){
                temp[i].series[0].points[0].update(data.temp[i].temperature);
                $('#temp-container-'+id+' .temp input').val(data.temp[i].temperature);
                $('#temp0'+id).css('background', data.temp[i].color);
                $('#temp0'+id+'-datetime input').val(data.temp[i].datetime);
            }
        }
        for(i=0; i<2; i++){
            id = i+1;
            if(data.atrh[i]){
                $('#atrh-container-'+id+' input').val(data.atrh[i].temperature);
                $('#atrh0'+id).css('background', data.atrh[i].color);
                $('#atrh0'+id+'-datetime input').val(data.atrh[i].datetime);
            }
        }
    }
});
</script>

@stop