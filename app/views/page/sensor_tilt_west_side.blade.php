@extends('...template')

@section('css_additional')
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/select2/select2.min.css'); }}">
    <link rel="stylesheet" href="{{ URL::to('assets/css/realtime/tilt_side.css'); }}">
    <style>
        #my-content-container {
            width: 100%;
            overflow-x: scroll;
        }
        #my-graph, #my-table {
            width: 900px;
            margin-right: auto;
            margin-left: auto;
            margin-top: 20px;
            margin-bottom: 20px;
        }
    </style>
@stop

@section('window_title')
    {{ $sensorType->code }}
@stop

@section('page_title')
    {{ $sensorType->code }}
    <small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
        <li>Bridge View - {{ ucfirst($side) }} Side</li>

    </ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Realtime Monitor</h3>
            <div class="box-tools pull-right">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="my-content-container">
                <div id="my-content">
                    <div id="my-canvas">
                        <div id="my-background"><?php
                                $i = 0;
                            ?>
                            @foreach($sensorsSelect2 as $key=>$value)
                                <a id="{{$key}}" class="tilt" href="{{ URL::to('sensor/tilt/'.$key) }}">
                                    <span class="hide">{{$value}}</span>
                                </a>
                                <div class="my-input-container" id="{{$key}}-input">
                                    <div class="title">{{$value}}</div>
                                    <div class="input-group">
                                        {{ Form::text($key.'-input', 0, array('class' => 'form-control big', 'placeholder' => '', 'disabled' => 'disabled')) }}
                                        <span class="input-group-addon big">&deg;</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                        <div id="north-desc" class="pic-desc">North</div>
                        <div id="south-desc" class="pic-desc">South</div>
                        <div id="view-desc" class="pic-desc">{{strtoupper($side)}} View</div>

                    </div>
                </div>
            </div>

        </div>

        <div class="box-header with-border">
            <h3 class="box-title">Filter Data</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <a href="{{ URL::to('sensor/'.strtolower($sensorType->code).'/compare-sensors-data') }}" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-refresh"></i></a>
                <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <!-- form start -->
        @include('page.sensor_tilt_realtime_compare')

    @stop

    @section('javascript_additional')
    @include('static/main_js')

            <!-- select2 -->
    <script src="{{ URL::to('assets/plugins/select2/select2.min.js'); }}"></script>

    <!-- date-range-picker -->
    <script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <!-- highcharts -->
    <script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

    <script src="{{ URL::to('assets/plugins/underscore/underscore-min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/backbone/backbone-min.js') }}"></script>
    <script>
        function rotate(el, deg){
            $(el).css('-webkit-transform', 'rotate('+ deg +'deg)');
            $(el).css('-moz-transform', 'rotate('+ deg +'deg)');
            $(el).css('-ms-transform', 'rotate('+ deg +'deg)');
            $(el).css('-o-transform', 'rotate('+ deg +'deg)');
            $(el).css('transform', 'rotate('+ deg +'deg)');
        }
        $(".tilt, .strtr").mouseenter(
                function() {
                    $(this).children('span').removeClass('hide');
                }
        ).mouseleave(
                function() {
                    $(this).children('span').addClass('hide');
                }
        );
        function initGraph (graph_container, data) {
            Highcharts.setOptions({
                    Global: {
                        useUTC: false
                    }
                });
            $(graph_container).highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    zoomType: 'x'
                },
//            legend:{
//              symbolHeight: 5,
//              symbolWidth: 5,
//                symbolRadius: 50
//            },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        millisecond: '%H:%M:%S.%L',
                        second: '%H:%M:%S',
                        minute: '%H:%M',
                        hour: '%H:%M',
                        day: '%e. %b',
                        week: '%e. %b',
                        month: '%b \'%y',
                        year: '%Y'
                    },
                    crosshair: true
                }],
                yAxis: data.yAxis,
                series: data.series,
                tooltip: {
                    shared: true,
                    xDateFormat: '%A, %b %e, %H:%M:%S.%L'
                },
                plotOptions: {
                    spline: {
                        marker: {
                            radius: 5,
                            symbol: 'circle'
                        },
                        lineWidth: 0.7,
                        states: {
                            hover: {
                                lineWidth: 0.9
                            }
                        }
                    }
                }
            });
        }

        $(document).ready(function(){
            $('.datetime').daterangepicker({
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
                timePickerSeconds: true,
                autoApply: true,
                autoUpdateInput: true,
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            });
            $(".select2").select2();

            rotate('#my-background', 0);
            loadData();
            setInterval(function () {
                loadData();
            }, 6000);

            function getData(){
                var result = function () {
                    var tmp = 0;
                    $.ajax({
                        'async': false,
                        'type': "GET",
                        'global': false,
                        'dataType': 'json',
                        'url': '{{ URL::to("sensor/tilt/bridge-side-view/$side/data") }}',
                        'success': function (data) {
                            tmp = data;
                        }
                    });
                    return tmp;
                }();
                console.log(result);
                return result;
            }

            function loadData(){
                var data = getData();
                for(i in data.tilt){
                    if(i){
                        $('#'+i+'-input input').val(data.tilt[i].hires);
                        $('#'+i).css('background', data.tilt[i].color);
                        $('#'+i+' span').text(data.tilt[i].code);
                        $('#'+i+'-input .title').text(data.tilt[i].code);
                    }
                    {{--$('#tilt'+tilt[i]).attr("href", "{{ URL::to('sensor/tilt') }}/" + data.tilt[tilt[i]].code.toLowerCase());--}}
                }
            }
        });

        $("#request-data").submit(function(e) {
            $('#result-container').html('');

            var cont = '';
            var compiled = _.template($('#graph-template').html(), {variable: 'data'})({
                title: 'Compared Data',
                cont: cont
            });

            $('#result-container').append(compiled);

            url = "{{ URL::to("sensor/tilt/compare-sensors/data") }}";
            $.ajax({
                type: "GET",
                data: $('#request-data').serialize(),
                url: url,
                success: function(data)
                {
                    if(data != false){
                        initGraph('#my-graph', data);
                        $('#my-box .overlay').remove();
                    }else{
                        $.bootstrapGrowl("Sensor(s) can't be empty!", {
                            ele: 'body', // which element to append to
                            type: 'info', // (null, 'info', 'danger', 'success')
                            offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
                            align: 'center', // ('left', 'right', or 'center')
                            width: 250, // (integer, or 'auto')
                            delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                            allow_dismiss: true, // If true then will display a cross to close the popup.
                            stackup_spacing: 10 // spacing between consecutively stacked growls.
                        });
                        $('#result-container').html('');
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>
@stop