@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<style>
    .center {
        margin: auto;
    }
    .lamp-container{
        width: 100px;
    }
    .lamp {
        width: 100%;
        height: 100px;
        border: solid 1px #cdcdcd;
        border-radius: 50%;
        background: rgba(169,3,41,1);
        background: -moz-linear-gradient(left, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(169,3,41,1)), color-stop(44%, rgba(143,2,34,1)), color-stop(100%, rgba(109,0,25,1)));
        background: -webkit-linear-gradient(left, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%);
        background: -o-linear-gradient(left, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%);
        background: -ms-linear-gradient(left, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%);
        background: linear-gradient(to right, rgba(169,3,41,1) 0%, rgba(143,2,34,1) 44%, rgba(109,0,25,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329', endColorstr='#6d0019', GradientType=1 );
    }

    .lamp-on {
        background: rgba(83,219,83,1);
        background: -moz-linear-gradient(left, rgba(83,219,83,1) 0%, rgba(83,219,83,1) 1%, rgba(12,135,12,1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(83,219,83,1)), color-stop(1%, rgba(83,219,83,1)), color-stop(100%, rgba(12,135,12,1)));
        background: -webkit-linear-gradient(left, rgba(83,219,83,1) 0%, rgba(83,219,83,1) 1%, rgba(12,135,12,1) 100%);
        background: -o-linear-gradient(left, rgba(83,219,83,1) 0%, rgba(83,219,83,1) 1%, rgba(12,135,12,1) 100%);
        background: -ms-linear-gradient(left, rgba(83,219,83,1) 0%, rgba(83,219,83,1) 1%, rgba(12,135,12,1) 100%);
        background: linear-gradient(to right, rgba(83,219,83,1) 0%, rgba(83,219,83,1) 1%, rgba(12,135,12,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#53db53', endColorstr='#0c870c', GradientType=1 );
    }

    .lamp-desc {
        text-align: center;
    }

    .lamp-time {
        text-align: center;
    }

</style>
@stop

@section('window_title')
Power Source
@stop

@section('page_title')
Power Source
<small></small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Power & Rack Monitoring</a></li>
    <li><a href="#">Power Source</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Power Source
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">PLN & Genset</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div class="col-xs-6">
                <div id="lamp-pln" class="lamp-container center">
                    <div class="lamp"></div>
                    <div class="lamp-desc">PLN</div>
                    <div class="lamp-time"></div>
                </div>
            </div>
            <div class="col-xs-6">
                <div id="lamp-genset" class="lamp-container center">
                    <div class="lamp"></div>
                    <div class="lamp-desc">Genset</div>
                    <div class="lamp-time"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div><!-- /.box -->

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Filter Data</h3>
        <div class="box-tools pull-right">
            <div id="canvas-navigator" class="btn-group p-b-5">
                <a href="{{ URL::to('power-source') }}" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-refresh"></i></a>
            </div>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <!-- form start -->
    <form action="" method="get">
        <div class="box-body">
            <div class="form-group">
                <div class="col-md-6">
                    <label>From</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('from', Input::get('from') ? Input::get('from') : $from, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'From',
                                'id' => 'from'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>To</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('to', Input::get('to') ? Input::get('to') : $to, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'To',
                                'id' => 'to'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
            </div>
        </div>
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="update">
        </div>
    </form><!-- /form -->
</div><!-- /.box -->

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="my-table" class="table table-bordered">
            <thead>
                <tr>
                    <th>Datetime</th>
                    <th>Type</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div><!-- /.box -->

@stop

@section('javascript_additional')
@include('static/main_js')

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>

function lampController(id, status){
    if(status == 0){
        $(id).removeClass('lamp-on');
        $(id).addClass('lamp-off');
    }else{
        $(id).removeClass('lamp-off');
        $(id).addClass('lamp-on');
    }
}

function realtimeData () {
    $.getJSON( '{{ URL::to("power-source/realtime-data") }}', function( data ) {
        if(data.totalData > 0){
            lampController('#lamp-pln .lamp', data.pln.status);
            lampController('#lamp-genset .lamp', data.genset.status);

            $('#lamp-pln .lamp-time').text(data.pln.datetime);
            $('#lamp-genset .lamp-time').text(data.pln.datetime);
        }
        $('.overlay').remove();
    });
}

function initData () {
    var from = $('#from').val();
    var to = $('#to').val();
    temp = "?from=" + from + "&to=" + to;
    $.getJSON( '{{ URL::to("power-source/data") }}' + temp, function( data ) {
        $('#my-table').DataTable({
            "aaData": data.aaData,
            "sDom": "<'row'<'col-sm-12'f>r>" + "t" + "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "bPaginate": false,
            "bInfo": false,
            "bFilter": false,
            "order": [[ 0, "desc" ]]
        });
        $('.overlay').remove();
    });
}



function refreshData() {
    $.getJSON( '{{ URL::to("power-source/realtime-data") }}', function( data ) {
        if(data.totalData > 0){
            lampController('#lamp-pln .lamp', data.pln.status);
            lampController('#lamp-genset .lamp', data.genset.status);

            $('#lamp-pln .lamp-time').text(data.pln.datetime);
            $('#lamp-genset .lamp-time').text(data.pln.datetime);
        }
    });
}

$(document).ready(function(){
    initData();
    realtimeData();
    $('.datetime').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        autoApply: true,
        autoUpdateInput: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
    });
    var mainTimer = setInterval(function () {
        refreshData();
    }, 15000);
});

</script>
@stop