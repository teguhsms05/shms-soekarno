@extends('...template')

@section('css_additional')
<style>
.button-container {
padding: 5px;
width:100%;
margin: auto;
}
.sensor-button{
width: 80px;
height: 80px;
margin-right: 5px;
}
.sensor-button img{
width: 100%;
}
</style>
@stop

@section('window_title')
Sesmic
@stop

@section('page_title')
Sesmic
<small></small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> Dashboard
@stop

@section('toolbar')
@stop

@section('content')
<div class="box">
    <div class="box-body">
        <?php
        //include(app_path()."/views/page/database.php");
        $luk = $_SERVER['REQUEST_URI'];
        $lah = substr($luk, 22);
        $sub1 = substr($lah, 11, 8);
        $sub2 = substr($lah, 0, 10);
        $sub3 = $sub2." ".$sub1;
        //$date = new DateTime("$lah");
        //$date2 = $date->format('Y-m-d H:i:s.l');
        $kirim = "";
        $host = 'localhost'; 
        $user = 'root'; 
        $pass = '';
        $dbname = 'hmi_soekarno_tmp';
        $connect = mysql_connect($host, $user, $pass);
        $dbselect = mysql_select_db($dbname);
        $tgl = "";
        $query = mysql_query("select CONCAT(datetime) as waktu1, header_id from ssmac01_header_detail");
        while($data = mysql_fetch_array($query)){
            $conpert = AppUtil::datetimeMicrotime($data["waktu1"]);
            if($lah == $conpert){
                $tgl = $data["header_id"];
            }else{
                
            }
        }
        
        //$test = DB::table('ssmac01_header_detail')->select()->where('datetime', 'like', $sub3.'%')->first();
        $test2 = DB::table('ssmac01_header_detail')->select(DB::raw('CONCAT(datetime) as waktu, x, y, z, id'))->where('header_id', $tgl)->get();
        //foreach($test as $row){
          //      echo ($row->id)."<br>";
            //}
        //$test = DB::select("select * from ssmac01_header_detail where ssmac01_header_detail.datetime like '".$sub3."%'");
        
        //foreach($test as $row){
        //    echo $hedid = ($row->header_id)."<br>";
        //}
        //$test2 = DB::table("ssmac01_header_detail")->where('header_id', $hedid)->get();
        //foreach($test2 as $row2){
        //    var_dump($row->id);
        //}
        //var_dump($test->id);
        ?>
        
    </div>
    

    <script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

    <div class="box-body">
        <div id="disini"></div>
        <div class="panel panel-default">
            <table class="table">
                <thead class="info">
                    <th>Datetime</th>
                    <th>WE Max Acceleration (g)</th>
                    <th>NS Max Acceleration (g)</th>
                    <th>UD Max Acceleration (g)</th>
                </thead>
                <?php
                foreach($test2 as $baris){
                    $tglan = $baris->waktu;
                    $nx = round($baris->x, 8);
                    $ny = round($baris->y, 8);
                    $nz = round($baris->z, 8);
                ?>
                <tr>
                    <td><?php echo $tglan;?></td>
                    <td><?php echo number_format($nx, 8);?></td>
                    <td><?php echo number_format($ny, 8);?></td>
                    <td><?php echo number_format($nz, 8);?></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
        @section('javascript_additional')

        @include('static/main_js')
        <script type="text/javascript">
            Highcharts.chart('disini', {
            chart: {
                type: 'spline',
                zoomType: 'x'
            },
            title: {
                text: 'Sesmic Detail'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                            type: 'datetime',
                            dateTimeLabelFormats: {
                                millisecond: '%H:%M:%S.%L',
                                second: '%H:%M:%S',
                                minute: '%H:%M',
                                hour: '%H:%M',
                                day: '%e. %b',
                                week: '%e. %b',
                                month: '%b \'%y',
                                year: '%Y'
                            },
                            crosshair: true
                        }],
            yAxis: {
                title: {
                    text: 'Acceleration (G)'
                }
            },
                        tooltip: {
                            shared: true,
                            xDateFormat: '%A, %b %e, %H:%M:%S.%L'
                        },
            series: [{
                name: 'WE Max Acceleration',
                data: [
                    <?php
                    foreach($test2 as $row){
                        $tgl = AppUtil::datetimeMicrotime($row->waktu);
                        $nx = AppUtil::reformatNumber($row->x, 8);
                        echo "[".$tgl.", ".AppUtil::reformatNumber($row->x, 8)."], ";
                        //echo $row->x.",";
                    }
                    ?>
                ],
                tooltip: {
                        valueSuffix: ' (g)'
                    }
            },{
                name: "NS Max Acceleration",
                data: [
                    <?php
                    foreach($test2 as $row){
                        $tgl = AppUtil::datetimeMicrotime($row->waktu);
                        $nx = AppUtil::reformatNumber($row->x, 8);
                        echo "[".$tgl.", ".AppUtil::reformatNumber($row->y, 8)."], ";
                        //echo $row->x.",";
                    }
                    ?>
                ],
                tooltip: {
                        valueSuffix: ' (g)'
                    }
            },{
                name: "UD Max Acceleration",
                data: [
                    <?php
                    foreach($test2 as $row){
                        $tgl = AppUtil::datetimeMicrotime($row->waktu);
                        $nx = AppUtil::reformatNumber($row->x, 8);
                        echo "[".$tgl.", ".AppUtil::reformatNumber($row->z, 8)."], ";
                        //echo $row->x.",";
                    }
                    ?>
                ],
                tooltip: {
                        valueSuffix: ' (g)'
                    }
            }],
                plotOptions: {
                    spline: {
                        marker: {
                          radius: 0.7
                        },
                        lineWidth: 0.7,
                        states: {
                          hover: {
                              lineWidth: 0.9
                          }
                        }
                    }
                }
        });
        </script>
@stop
        
    </div>
</div>
@stop