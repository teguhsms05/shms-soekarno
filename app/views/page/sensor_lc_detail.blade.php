@extends('...template')

@section('css_additional')
<link rel="stylesheet" href="{{ URL::to('assets/plugins/daterangepicker/daterangepicker-bs3.css'); }}">
<link rel="stylesheet" href="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.css'); }}">
<style>
    #my-content-container {
        width: 100%;
        overflow-x: scroll;
    }
    #my-graph, #my-table {
        width: 900px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>
@stop

@section('window_title')
{{ $sensor->code }}
@stop

@section('page_title')
{{ $sensor->code }}
<small>{{ $sensor->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensor->sensorType->code)) }}">{{ $sensor->sensorType->code }}</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensor->sensorType->code).'/'.strtolower($sensor->code)) }}">{{ $sensor->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensor->code }}
@stop

@section('content')


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Filter Data</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <a href="{{ URL::to('sensor/'.strtolower($sensor->sensorType->code).'/'.strtolower($sensor->code)) }}" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-refresh"></i></a>
            <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <!-- form start -->
    <form action="" method="get">
        <div class="box-body">
            <div class="form-group">
                <div class="col-md-6">
                    <label>From</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('from', Input::get('from') ? Input::get('from') : $from, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'From',
                                'id' => 'from'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>To</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('to', Input::get('to') ? Input::get('to') : $to, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'To',
                                'id' => 'to'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
            </div>
        </div>
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="update">
        </div>
    </form><!-- /form -->
</div><!-- /.box -->

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $sensor->parameter->desc }}</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div id="my-content-container">
            <div id="my-graph"></div>
            <table id="my-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Datetime</th>
                        <th>Cable Force (kN)</th>
                        <th>Cable Stress (MPa)</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div><!-- /.box -->
@stop

@section('javascript_additional')

@include('static/main_js')

<!-- date-range-picker -->
<script src="{{ URL::to('assets/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- datatables -->
<script src="{{ URL::to('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<!-- highcharts -->
<script src="{{ URL::to('assets/plugins/highcharts/highcharts.js') }}"></script>
<script src="{{ URL::to('assets/plugins/highcharts/modules/exporting.js') }}"></script>

<script type="text/javascript">
    function initData () {
        var from = $('#from').val();
        var to = $('#to').val();
        temp = "?from=" + from + "&to=" + to;
        $.getJSON( '{{ URL::to("sensor/".strtolower($sensor->sensorType->code)."/".strtolower($sensor->code)."/data") }}' + temp, function( data ) {
            Highcharts.setOptions({
                    Global: {
                        useUTC: false
                    }
                });
            $('#my-graph').highcharts({
                credits: {
                    enabled: false
                },
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: [{
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        millisecond: '%H:%M:%S.%L',
                        second: '%H:%M:%S',
                        minute: '%H:%M',
                        hour: '%H:%M',
                        day: '%e. %b',
                        week: '%e. %b',
                        month: '%b \'%y',
                        year: '%Y'
                    },
                    crosshair: true
                }],
                yAxis: [{
                    labels: {
                        format: '{value} ' + data.x.unitOfCount,
                        style: {
                            color: data.x.color
                        }
                    },
                    title: {
                        text: data.x.code + ' (' + data.x.unitOfCount+ ')',
                        align: 'middle',
                        style: {
                            color: data.x.color
                        }
                    },
                    plotLines: [{
                        label: {
                            text: 'Threshold Level 1',
                            x: 25,
                            style: {
                                color: 'green'
                            }
                        },
                        color: 'green',
                        width: 2,
                        value: data.x.threshold_1,
                        dashStyle: 'longdashdot'
                    },
                    {
                        label: {
                            text: 'Threshold Level 2',
                            x: 25,
                            style: {
                                color: 'orange'
                            }
                        },
                        color: 'orange',
                        width: 2,
                        value: data.x.threshold_2,
                        dashStyle: 'longdashdot'
                    },
                    {
                        label: {
                            text: 'Threshold Level 3',
                            x: 25,
                            style: {
                                color: 'red'
                            }
                        },
                        color: 'red',
                        width: 2,
                        value: data.x.threshold_3,
                        dashStyle: 'longdashdot'
                    }]
                },
                {
                    labels: {
                        format: '{value} ' + data.y.unitOfCount,
                        style: {
                            color: data.y.color
                        }
                    },
                    title: {
                        text: data.y.code + ' (' + data.y.unitOfCount+ ')',
                        align: 'middle',
                        style: {
                            color: data.y.color
                        }
                    },
                    plotLines: [{
                        label: {
                            text: 'Threshold Level 1',
                            x: 25,
                            style: {
                                color: 'green'
                            }
                        },
                        color: 'green',
                        width: 2,
                        value: 593,
                        dashStyle: 'longdashdot'
                    },
                    {
                        label: {
                            text: 'Threshold Level 2',
                            x: 25,
                            style: {
                                color: 'orange'
                            }
                        },
                        color: 'orange',
                        width: 2,
                        value: 596,
                        dashStyle: 'longdashdot'
                    },
                    {
                        label: {
                            text: 'Threshold Level 3',
                            x: 25,
                            style: {
                                color: 'red'
                            }
                        },
                        color: 'red',
                        width: 2,
                        value: data.y.threshold_3,
                        dashStyle: 'longdashdot'
                    }],
                    opposite: true
                }],
                series: [{
                     name: data.x.code,
                     type: data.x.mode,
                     data: data.x.timeseries,
                     tooltip: {
                         valueSuffix: ' ' + data.x.unitOfCount
                     },
                     color: data.x.color,
                     negativeColor: data.x.negativeColor,
                     threshold: data.x.threshold,
                    visible: false
                 },{
                    name: data.y.code,
                    type: data.y.mode,
                    data: data.y.timeseries,
                    tooltip: {
                        valueSuffix: ' ' + data.y.unitOfCount
                    },
                    color: data.y.color,
                    negativeColor: data.y.negativeColor,
                    threshold: data.y.threshold,
                     yAxis:1,
                     visible: true,
                     zones:[{
                         value: 593,
                         color: 'green'
                     },{
                         value: 596,
                         color: 'orange'
                     },{
                         color: 'red'
                     }]
                },],
                tooltip: {
                    shared: true,
                    xDateFormat: '%A, %b %e, %H:%M:%S.%L'
                },
                plotOptions: {
                    spline: {
                        marker: {
                          radius: 0.7
                        },
                        lineWidth: 0.7,
                        states: {
                          hover: {
                              lineWidth: 0.9
                          }
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function(event) {
                                var seriesIndex = this.index;
                                var series = this.chart.series;

                                for (var i = 0; i < series.length; i++)
                                {
                                    if (series[i].index != seriesIndex)
                                    {
                                        series[i].hide();
                                    }else{
                                        series[i].show()
                                    }
                                }

                                return false;
                            }
                        }
                    }
                }
            });
            $('#my-table').DataTable({
                "aaData": data.aaData,
                "sDom": "<'row'<'col-sm-12'f>r>" + "t" + "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                "bPaginate": false,
                "bInfo": false,
                "bFilter": false,
                "order": [[ 0, "desc" ]]
            });
            $('.overlay').remove();
        });
    }

    $(document).ready(function(){
        $('.datetime').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: true,
            autoApply: true,
            autoUpdateInput: true,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        });
        initData();
    });
</script>
@stop