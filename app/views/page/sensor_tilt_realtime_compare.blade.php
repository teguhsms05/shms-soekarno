<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Filter Data</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            <a href="{{ URL::to('sensor/'.strtolower($sensorType->code).'/compare-sensors-data-json') }}" id="btn-side-view" class="btn btn-box-tool p-5"><i class="fa fa-refresh"></i></a>
            <button class="btn btn-box-tool" onclick="printDiv('print-wrapper')"><i class="fa fa-print"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <!-- form start -->
    <form action="" method="get" id="request-data">
        <div class="box-body">
            <div class="form-group">
                <div class="col-md-6">
                    <label>From</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('from', $from, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'From',
                                'id' => 'from'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>To</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        {{
                            Form::text('to', $to, array(
                                'class' => 'form-control datetime pull-right',
                                'placeholder' => 'To',
                                'id' => 'to'
                            ))
                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>Sensor(s)</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-balance-scale"></i>
                        </div>
                        {{
                            Form::select(
                                'sensors[]',
                                $sensorsSelect2,
                                array(),
                                [
                                    'id' => 'sensors',
                                    'multiple' => 'multiple' ,
                                    'class' => 'select2',
                                    'style' => "width: 100%;"
                                ]

                            )

                        }}
                    </div><!-- /.input group -->
                </div>
                <div class="col-md-6">
                    <label>Field to compare</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-table"></i>
                        </div>
                        {{
                            Form::select(
                                'field',
                                $fields,
                                array(),
                                [
                                    'id' => 'sensors',
                                    'class' => 'select2',
                                    'style' => "width: 100%;"
                                ]
                            )
                        }}
                    </div><!-- /.input group -->
                </div>
            </div>
        </div>
        <div class="box-footer">
            <input class="btn btn-primary pull-right" type="submit" value="update">
        </div>
    </form><!-- /form -->
</div><!-- /.box -->

<div id="result-container">

</div>

<script type="text/template" id="graph-template">
    <div class="box" id="my-box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="my-content-container">
                <div id="my-graph"></div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div><!-- /.box -->
</script>