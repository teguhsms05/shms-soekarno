@extends('...template')

@section('css_additional')
<style>
    #my-content-container {
        width: 100%;
        overflow-x: scroll;
        overflow-y: hidden;
        text-align: center;
    }
    #my-content {
        width: 952px;
        margin-right: auto;
        margin-left: auto;
    }
    .my-gallery-box{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .my-gallery-box:hover{
    }
    .my-gallery-thumb{
    }
    .my-gallery-thumb img{
        width: 100%;
    }
</style>
@stop

@section('window_title')
{{ $sensorType->code }}
@stop

@section('page_title')
{{ $sensorType->code }}
<small>{{ $sensorType->desc }}</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{ URL::to('sensor/'.strtolower($sensorType->code)) }}">{{ $sensorType->code }}</a></li>
</ol>
@stop

@section('page_title')
    <i class="glyphicon glyphicon-dashboard"></i> {{ $sensorType->code }}
@stop

@section('content')
<div id="my-content-container">
   <div id="my-content">
        <div class="row">
            <div class="col-xs-3 my-gallery-box">
                <a class="my-gallery-thumb" href="{{ URL::to('sensor/strtr/pylon/south-view') }}">
                    <img src="{{ URL::to('assets/images/strain_pylon_front.jpg') }}" class="">
                    <span>Pylon South View</span>
                </a>
            </div>
            <div class="col-xs-3 my-gallery-box">
                <a class="my-gallery-thumb" href="{{ URL::to('sensor/strtr/pylon/west-view') }}">
                    <img src="{{ URL::to('assets/images/strain_pylon_side.jpg') }}" class="">
                    <span>Pylon West View (Bunaken)</span>
                </a>
            </div>
            <div class="col-xs-3 my-gallery-box">
                <a class="my-gallery-thumb" href="{{ URL::to('sensor/strtr/pylon/east-view') }}">
                    <img src="{{ URL::to('assets/images/strain_pylon_side.jpg') }}" class="">
                    <span>Pylon East View (Klabat)</span>
                </a>
            </div>
            @foreach($deck as $i)
            <div class="col-xs-3 my-gallery-box">
                <a class="my-gallery-thumb" href="{{ URL::to('sensor/strtr/deck/'.$i) }}">
                    <img src="{{ URL::to('assets/images/strain_deck.jpg') }}" class="">
                    <span>Deck {{$i}}</span>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop

@section('javascript_additional')
@include('static/main_js')
@stop