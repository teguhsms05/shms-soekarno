<?php
    $sensorTypeList = SensorType::all();
?>
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Kementerian Pekerjaan Umum dan Perumahan  -->
        <div class="brand">
            <div class="brand-image">
                <img src="{{ URL::to('assets/images/logo/logo-pu.jpg') }}">
            </div>
            <div class="brand-text">
                KEMENTERIAN PEKERJAAN UMUM <br> DAN PERUMAHAN RAKYAT <br>
                REPUBLIK INDONESIA
            </div>
        </div>

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::to('assets/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li><a href="{{ URL::to('') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @if(Auth::user()->role=="SUPER" || Auth::user()->role=="ADMIN")
            <li class="header">Settings</li>
            @if(Auth::user()->role=="SUPER")
            <li><a href="{{ URL::to('setting/user') }}"><span>User Management</span></a></li>
            @endif
            <li class="treeview">
                <a href="#"> <span>Threshold and Properties</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @foreach($sensorTypeList as $sensorType)
                    <li><a href="{{ URL::to('setting/'.strtolower($sensorType->code)) }}"><i class="fa fa-cog"></i><span>{{ $sensorType->desc }}</span></a></li>
                    @endforeach
                    <!--
                    <li><a href="{{ URL::to('setting/displacement') }}">DISPLACEMENT</a></li>
                    -->
                </ul>
            </li>
            @endif
            <li class="header">Sensors</li>

            @foreach($sensorTypeList as $sensorType)
            <li class="treeview">
                <a href="#"><span>{{ $sensorType->desc }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php
                        $realtime = array('ANMBI', 'ANMTRI', 'ATRH', 'LC', 'STRTR', 'TEMP', 'TILT');
                        $compare = array('ACC', 'ANMTRI', 'ATRH', 'LC', 'STRTR', 'TILT', 'TEMP');
                        $sensors = Sensor::where('sensor_type_id', '=', $sensorType->id)->orderBy('code', 'asc')->distinct()->get();
                    ?>
                    @if(in_array($sensorType->code, $realtime))
                    <li><a href="{{ URL::to('sensor/'.(strtolower($sensorType->code))) }}"><i class="fa fa-tv"></i>Realtime Monitor</a></li>
                    @endif
                    @if(in_array($sensorType->code, $compare))
                    <li><a href="{{ URL::to('sensor/'.(strtolower($sensorType->code)).'/compare-sensors') }}"><i class="fa fa-balance-scale"></i>Compare Sensors Data</a></li>
                    @endif
                    @foreach($sensors as $sensor)
                    <li><a href="{{ URL::to('sensor/'.(strtolower($sensorType->code)).'/'.(strtolower($sensor->code))) }}"><i class="fa fa-line-chart"></i>{{ $sensor->code }}</a></li>
                    @endforeach
                </ul>
            </li>
            @endforeach
            <li class="header">Analyze Data</li>
            <li><a href="{{ URL::to('compare-sensors') }}"><span>Compare Sensors</span></a></li>
            <!--li class="header">Processed</li>
            <li><a href="{{ URL::to('sensor/tilt/displacement') }}"><span>Displacement</span></a></li-->
            <li class="header">Power & Rack Monitoring</li>
            <li><a href="{{ URL::to('racktemp') }}"><span>Rack Temperature</span></a></li>
            <li><a href="{{ URL::to('power-source') }}"><span>Power Source</span></a></li>
            <li><a href="{{ URL::to('ups') }}"><span>UPS</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>