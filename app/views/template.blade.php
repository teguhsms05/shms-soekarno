<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SHMS Soekarno Bridge | @section('window_title')@show</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/bootstrap/css/bootstrap.min.css'); }}">
    <!--
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/bootstrap/css/bootstrap-theme.min.css'); }}">
    -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('assets/font-awesome-4.4.0/css/font-awesome.min.css'); }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::to('assets/ionicons/2.0.1/css/ionicons.min.css'); }}">
    <!-- Pace -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/pace/templates/pace-theme-flash.tmpl.css'); }}">

    @section('css_additional')
    @show

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('assets/dist/css/AdminLTE.min.css'); }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="{{ URL::to('assets/dist/css/skins/skin-blue-light.min.css'); }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link  rel="stylesheet" href="{{ URL::to('assets/css/style.css') }}">
    <link  rel="stylesheet" href="{{ URL::to('assets/css/generics.css') }}">
    <script src="{{ URL::to('assets/plugins/jQuery/jQuery-2.1.4.min.js'); }}"></script>
    <style>
        @media screen
        {
            .no-print{}
            .no-screen{display:none;}
        }
        @media print {

          a[href]:after {
            content: none !important;
          }

          .brand-print{
            display:block !important;
            width: 100%;
            height: 55px;
            /*background-color: #1a4567 !important;*/
            -webkit-print-color-adjust: exact;
          }

          .brand-print .brand-image-print {
              float: left;
              padding: 2px 5px 2px;
          }

          .brand-print .brand-text-print {
              padding: 2px 0;
              font-size: 9px;
              font-weight: bold;
              line-height: 17px;
              color: #000000 !important;
              -webkit-print-color-adjust: exact;
          }

          .no-print, .btn{display:none;}
          .no-screen{}

          .content-header {
            display: block !important;
          }

          .content-header .breadcrumb {
            display: none !important;
          }
        }
    </style>

</head>
<!-- sidebar-collapse -->
<body class="hold-transition skin-blue-light fixed">

<div class="wrapper">

<!-- Main Header -->
@include('navigation/header')

<!-- Left side column. contains the logo and sidebar -->
@include('navigation/sidebar')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" id="print-wrapper">
    <div class="brand-print no-screen">
        <div class="brand-image-print">
            <img src="{{ URL::to('assets/images/logo/logo-pu.jpg') }}">
        </div>
        <div class="brand-text-print">
            KEMENTERIAN PEKERJAAN UMUM <br> DAN PERUMAHAN RAKYAT <br>
            REPUBLIK INDONESIA
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @section('page_title')
            @show
        </h1>
        @section('breadcrumb')
        @show
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->
        @section('content')
        @show

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2015 <a href="http://www.ubs-indonesia.net">Upajiwa Business Solution Indonesia</a>.</strong> All rights reserved.
</footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->


<script src="{{ URL::to('assets/plugins/bootstrap/js/bootstrap.min.js'); }}"></script>
<script src="{{ URL::to('assets/plugins/pace/pace.min.js'); }}"></script>
<script src="{{ URL::to('assets/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js'); }}"></script>
<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>

@section('javascript_additional')
<script>
document.onmousedown=disableclick;
status="Right Click Disabled";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;
   }
}
</script>
@show

</body>
</html>