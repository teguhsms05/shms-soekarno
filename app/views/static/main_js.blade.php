<script src="{{ URL::to('assets/plugins/underscore/underscore-min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/backbone/backbone-min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::to('assets/dist/js/app.min.js') }}"></script>