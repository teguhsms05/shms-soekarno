<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SHMS Soeakrno | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::to('assets/font-awesome-4.4.0/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ URL::to('assets/ionicons/2.0.1/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::to('assets/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/iCheck/square/blue.css') }}">

    <style>
        .brand {
            padding: 0px;
        }

        .brand .brand-image {
            padding: 0px 5px 0px;
        }

        .brand .brand-text {
            padding: 2px 0;
            font-size: 12px;
            font-weight: bold;
            line-height: 17px;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">

        <div class="brand">
            <div class="brand-image">
                <img src="{{ URL::to('assets/images/logo/logo-pu.jpg') }}">
            </div>
            <div class="brand-text">
                KEMENTERIAN PEKERJAAN UMUM <br> DAN PERUMAHAN RAKYAT <br>
                REPUBLIK INDONESIA
            </div>
        </div>
        <div>
            <img src="{{ URL::to('assets/images/logo/logo-ubs.png') }}">
        </div>

        <a href="#"><b>SHMS</b>SoekarnoBridge</a>

      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="{{ URL::to('login'); }}" method="post">
          <div class="form-group has-feedback">
            <input type="username" class="form-control" placeholder="Username" value="{{ Input::old('username') }}" name="username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              {{--<div class="checkbox icheck">--}}
                {{--<label>--}}
                  {{--{{ Form::checkbox('remember', '1', Input::old('remember')) }} Remember Me--}}
                {{--</label>--}}
              {{--</div>--}}
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
          @if($errors->has())
          <div class="alert alert-info" style="margin-top:20px;">
              <span>Error: </span>
              <ul>
              @foreach ($errors->all() as $error)
                  <li>
                      {{ $error }}
                  </li>
              @endforeach
              </ul>
          </div>
          @endif
        </form>
        <!--
        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>
        -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ URL::to('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ URL::to('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ URL::to('assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>