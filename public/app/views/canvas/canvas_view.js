define([
    'jquery',
    'underscore',
    'backbone',
    'konva',
    'models/canvas_model',
    'collections/sections_collection',
    'views/section/section_view'
], function ($, _, Backbone, Konva, CanvasModel, SectionCollection, SectionView) {

    CanvasView = Backbone.View.extend({
        initialize: function (options) {
            this.model = new CanvasModel(options);
            parameterId = this.model.get('parameter_id');
            sensorTypeId = this.model.get('sensor_type_id');
            var stage = new Konva.Stage({
                container: 'canvas-bridge',
                width: this.model.get('canvas_width'),
                height: this.model.get('canvas_height')
            });

            var layer = new Konva.Layer();
            stage.add(layer);

            $('#canvas-bridge').css('margin', 'auto');
            $('#canvas-bridge').css('width', this.model.get('canvas_width') + 'px');

            $('#canvas-bridge div').css("height", this.model.get('canvas_height') + 'px');
            $('#canvas-bridge div').css("text-align", "center");

            $("#canvas-bridge canvas").attr("width", this.model.get('canvas_width') + 'px');
            $("#canvas-bridge canvas").attr("height", this.model.get('canvas_height') + 'px');

            $('#canvas-bridge canvas').css("background", 'url("assets/images/' + this.model.get('canvas_background') + '")');
            $('#canvas-bridge canvas').css("background-size", this.model.get('canvas_width') + 'px');

            mycollection = new SectionCollection({url: this.model.get('data_url')});
            console.log('canvas_view' + sensorTypeId);
            mycollection.fetch({
                success: function () {
                    console.log("section collection", parameterId);
                    view = new SectionView({
                        layer: layer,
                        collection: mycollection,
                        parameter_id: parameterId,
                        sensor_type_id: sensorTypeId
                    });
                    view.render();
                    $('.overlay').remove();
                }
            });
        }
    });
    return CanvasView;

});