define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/section/section_by_sensor_type_template.html'
], function ($, _, Backbone, sectionBySensorTypeTemplate) {
    var SectionDetail = Backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            console.log('view', this.model);
            var col = this.model.toJSON();
            console.log('col', col);
            var compiled = _.template($(sectionBySensorTypeTemplate).html(), {variable: 'data'})({
                section: col.section,
                sensor_types: col.sensorTypes
            });
            this.$el.html(compiled);
        }
    });
    return SectionDetail;
});

/*
 define([
 'jquery',
 'underscore',
 'backbone',
    'text!templates/section/section_template.html'
], function ($, _, Backbone, parameterListTemplate) {
    var SectionDetail = Backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            console.log('view', this.model);
            var col = this.model.toJSON();
            var compiled = _.template($(parameterListTemplate).html(), {variable: 'data'})({
                section: col.section,
                parameters: col.parameters
            });
            this.$el.html(compiled);
        }
    });
    return SectionDetail;
});
 */