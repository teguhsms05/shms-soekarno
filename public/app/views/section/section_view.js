define([
    'underscore',
    'backbone',
    'konva',
    'konvaview',
    'models/section_parameter_list_model',
    'models/section_sensor_type_list_model',
    'views/section/section_detail_view'
], function (_, Backbone, Konva, KonvaView, SectionParameterListModel, SectionSensorTypeListModel, SectionDetailView) {
    var SectionView = Backbone.KonvaView.extend({
        initialize: function (params) {
            this.layer = params.layer;
            this.collection = params.collection;
            this.parameterId = params.parameter_id;
            this.sensorTypeId = params.sensor_type_id;
            console.log('section view', this.parameterId);
            console.log('section view', this.sensorTypeId);
        },
        el: function () {
            var group = new Konva.Group();
            this.collection.each(function (section) {
                var color = 'lime';
                var linePosition = [0, 0, 0, 0];
                var textPosition = [{
                    x: 0,
                    y: 0
                }];
                switch (section.get('status')) {
                    case 'on' :
                        color = 'lime';
                        break;
                    case 'off' :
                        color = '#f0f0f0';
                        break;
                    case 'warning':
                        color = 'yellow';
                        break;
                }
                switch (section.get('position_desc')) {
                    case 't' :
                        linePosition = [section.get('position_x'), section.get('position_y') - 20, section.get('position_x'), section.get('position_y')];
                        textPosition.x = section.get('position_x') - 8;
                        textPosition.y = section.get('position_y') - 35;
                        break;
                    case 'r' :
                        linePosition = [section.get('position_x'), section.get('position_y'), section.get('position_x') + 15, section.get('position_y')];
                        textPosition.x = section.get('position_x') + 15;
                        textPosition.y = section.get('position_y') - 6;
                        break;
                    case 'b' :
                        linePosition = [section.get('position_x'), section.get('position_y'), section.get('position_x'), section.get('position_y') + 20];
                        textPosition.x = section.get('position_x') - 8;
                        textPosition.y = section.get('position_y') + 20;
                        break;
                    case 'l' :
                        linePosition = [section.get('position_x') - 20, section.get('position_y'), section.get('position_x'), section.get('position_y')];
                        textPosition.x = section.get('position_x') - 40;
                        textPosition.y = section.get('position_y') - 6;
                        break;
                }
                var circ = new Konva.Circle({
                    x: section.get('position_x'),
                    y: section.get('position_y'),
                    width: 8,
                    height: 8,
                    fill: color,
                    id: 'rect',
                    stroke: 'black',
                    strokeWidth: 1,
                    name: 'section-point',
                    sectionCode: section.get('code'),
                    sectionId: section.get('id'),
                    sectionCode: section.get('desc'),
                    sectionStatus: section.get('status')
                });
                var line = new Konva.Line({
                    points: linePosition,
                    stroke: 'red',
                    tension: 0
                });
                var text = new Konva.Text({
                    x: textPosition.x,
                    y: textPosition.y,
                    text: section.get('code'),
                    fontSize: 12,
                    fontFamily: 'Calibri',
                    fill: 'black'
                });
                var subGroup = new Konva.Group();
                subGroup.add(line).add(text).add(circ);
                group.add(subGroup);
            }, this)
            return group;
        },
        events: {
            'click .section-point': 'onClickSection',
            'tap .section-point': 'onClickSection',
            'mouseover .section-point': 'onMouseOverSection',
            'mouseout .section-point': 'onMouseOutSection'
        },
        onClickSection: function (e) {
            console.log('Clicked or tapped', e.target.attrs.sectionId);
            var sectionSensorTypeList = new SectionSensorTypeListModel({
                id: e.target.attrs.sectionId,
                sensorTypeId: this.sensorTypeId
            });
            sectionSensorTypeList.fetch({
                success: function () {
                    console.log('after fetch section parameter list', sectionSensorTypeList.toJSON());
                    console.log('after fetch section sensor type list', sectionSensorTypeList.toJSON());
                    var sectionDetailView = new SectionDetailView({
                        el: $("#content-container"),
                        model: sectionSensorTypeList
                    });
                }
            });
        },
        onMouseOverSection: function (e) {
            console.log('Mouse over');
            console.log(e.target.attrs.code);
            document.body.style.cursor = "pointer";
        },
        onMouseOutSection: function (e) {
            console.log('Mouse leave');
            console.log(e.target.attrs.code);
            document.body.style.cursor = "default";
        },
        render: function () {
            this.layer.add(this.el);
            var myGroup = this.layer.children[0].getChildren();
            var warning = false;
            var randomNumber = Math.floor((Math.random() * 10) + 1);
            var myTimer = 1000 * 60 * 14;
            var errorBlinkerTimer = new Array();

            for (i = 0; i < myGroup.length; i++) {
                if (myGroup[i].children[2].attrs.sectionStatus == 'warning') {
                    tweenR = new Konva.Tween({
                        node: myGroup[i].children[2],
                        duration: 1,
                        fill: 'red',
                        onFinish: function () {
                            this.reverse();
                        }
                    });
                    var myTemp = i;
                    errorBlinkerTimer[i] = setInterval(function (y) {
                        console.log('myTemp di dalam', myTemp);
                        y.play();
                        console.log('timer warna' + randomNumber);
                    }, 2000, tweenR);
                    warning = true;
                }
            }

            setTimeout(function () {
                for (i = 0; i < myGroup.length; i++) {
                    clearInterval(errorBlinkerTimer[i]);
                }
            }, myTimer);

            this.layer.draw();

            if (warning === true) {
                var audio = new Audio('assets/audio/alarm.mp3');
                var errorBuzzerTimer = setInterval(function () {
                    audio.play();
                    console.log('timer bunyi' + randomNumber);
                }, 2000);
                /*
                setTimeout(function () {
                    clearInterval(errorBuzzerTimer);
                }, myTimer);
                 */
            }
        }
    });

    return SectionView;
});