define([
    'jquery',
    'underscore',
    'backbone',
    'text!templates/sidebar/sidebar_template.html'
], function ($, _, Backbone, sidebarTemplate) {
    var SectionDetail = Backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            var sidebarData = this.model.toJSON();
            var sidebar = _.template($(sidebarTemplate).html())({data: sidebarData});
            $('.sidebar-menu').append(sidebar);
        }
    });
    return SectionDetail;
});