define([
    'underscore',
    'backbone',
    'konva',
    'konvaview',
    'models/section_parameter_list_model',
    'views/section/section_detail_view'
], function (_, Backbone, Konva, KonvaView, SectionParameterListModel, SectionDetailView) {
    var SectionView = Backbone.KonvaView.extend({
        initialize: function (params) {
            for (var i = 1; i < 100; i++) {
                window.clearInterval(i);
            }
            this.layer = params.layer;
            this.collection = params.collection;
        },
        el: function () {
            var group = new Konva.Group();
            this.collection.each(function (section) {
                var color = 'lime';
                var linePosition = [0, 0, 0, 0];
                var textPosition = [{
                    x: 0,
                    y: 0
                }];
                switch (section.get('status')) {
                    case 'on' :
                        color = 'lime';
                        break;
                    case 'off' :
                        color = 'black';
                        break;
                    case 'warning':
                        color = 'yellow';
                        break;
                }
                switch (section.get('desc_position')) {
                    case 't' :
                        linePosition = [section.get('x'), section.get('y') - 20, section.get('x'), section.get('y')];
                        textPosition.x = section.get('x') - 8;
                        textPosition.y = section.get('y') - 35;
                        break;
                    case 'r' :
                        linePosition = [section.get('x'), section.get('y'), section.get('x') + 15, section.get('y')];
                        textPosition.x = section.get('x') + 15;
                        textPosition.y = section.get('y') - 6;
                        break;
                    case 'b' :
                        linePosition = [section.get('x'), section.get('y'), section.get('x'), section.get('y') + 20];
                        textPosition.x = section.get('x') - 8;
                        textPosition.y = section.get('y') + 20;
                        break;
                    case 'l' :
                        linePosition = [section.get('x') - 20, section.get('y'), section.get('x'), section.get('y')];
                        textPosition.x = section.get('x') - 40;
                        textPosition.y = section.get('y') - 6;
                        break;
                }
                var circ = new Konva.Circle({
                    x: section.get('x'),
                    y: section.get('y'),
                    width: 8,
                    height: 8,
                    fill: color,
                    id: 'rect',
                    stroke: 'black',
                    strokeWidth: 1,
                    name: 'section-point',
                    sectionCode: section.get('code'),
                    sectionId: section.get('id'),
                    sectionCode: section.get('desc'),
                    sectionStatus: section.get('status')
                });
                var line = new Konva.Line({
                    points: linePosition,
                    stroke: 'red',
                    tension: 0
                });
                var text = new Konva.Text({
                    x: textPosition.x,
                    y: textPosition.y,
                    text: section.get('code'),
                    fontSize: 12,
                    fontFamily: 'Calibri',
                    fill: 'black'
                });
                var subGroup = new Konva.Group();
                subGroup.add(line).add(text).add(circ);
                group.add(subGroup);
            }, this)
            return group;
        },
        events: {
            'click .section-point': 'onClickSection',
            'tap .section-point': 'onClickSection',
            'mouseover .section-point': 'onMouseOverSection'
        },
        onClickSection: function (e) {
            console.log('Clicked or tapped', e.target.attrs.sectionId);
            var sectionParameterList = new SectionParameterListModel({
                id: e.target.attrs.sectionId
            });
            sectionParameterList.fetch({
                success: function () {
                    console.log('after fetch section parameter list', sectionParameterList.toJSON());
                    var sectionDetailView = new SectionDetailView({
                        el: $("#content-container"),
                        model: sectionParameterList
                    });
                }
            });
        },
        onMouseOverSection: function (e) {
            console.log('Mouse over');
            console.log(e.target.attrs.code);
            $(e.target.attrs.id).css('cursor', 'pointer');
        },
        render: function () {
            this.layer.add(this.el);
            var myGroup = this.layer.children[0].getChildren();
            var warning = false;
            var randomNumber = Math.floor((Math.random() * 10) + 1);

            for (i = 0; i < myGroup.length; i++) {
                if (myGroup[i].children[2].attrs.sectionStatus == 'warning') {
                    tweenR = new Konva.Tween({
                        node: myGroup[i].children[2],
                        duration: 1,
                        fill: 'red',
                        onFinish: function () {
                            this.reverse();
                        }
                    });
                    this.errorBlinkerTimer = setInterval(function (y) {
                        y.play();
                        console.log('timer warna' + randomNumber);
                    }, 2000, tweenR);
                    warning = true;
                }
            }
            this.layer.draw();
            if (warning === true) {
                var audio = new Audio('assets/audio/alarm.mp3');
                this.errorBuzzerTimer = setInterval(function () {
                    //audio.play();
                    console.log('timer bunyi' + randomNumber);
                }, 2000);
            }
        }
    });

    return SectionView;
});