define([
    'jquery',
    'underscore',
    'backbone',
    'router'
], function ($, _, Backbone, Router, sidebarTemplate) {
    var initialize = function () {
        Router.initialize();
    }
    return {
        initialize: initialize
    };
});