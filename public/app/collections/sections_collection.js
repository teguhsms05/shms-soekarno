define([
    'underscore',
    'backbone',
    'models/section_model'
], function (_, Backbone, SectionModel) {
    var SectionCollection = Backbone.Collection.extend({
        model: SectionModel,
        initialize: function (options) {
            this.url = options.url;
        }
    });
    return SectionCollection;
});