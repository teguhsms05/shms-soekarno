define([
    'jquery',
    'underscore',
    'backbone',
    'models/canvas_model',
    'views/canvas/canvas_view',
    'text!templates/util/loader_template.html',
    'models/parameter_model',
    'models/section_model',
    'models/sensor_type_model'
], function ($, _, Backbone, CanvasModel, CanvasView, Loader, ParameterModel, SectionModel, SensorTypeModel) {
    var AppRouter = Backbone.Router.extend({
        initialize: function () {
            this.routesHit = 0;
            Backbone.history.on('route', function () {
                this.routesHit++;
            }, this);
        },

        routes: {
            "": "mainView",
            "parameter/:parameterid": "parameterView",
            "sensor-type/:sensortypeid": "sensorTypeView",
            "section/:sectionid/parameter/:parameterid": "sectionParameterView",
            "section/:sectionid/parameter/:parameterid/sensor/:sensorid": "sectionParameterSensorView",
            "*actions": "mainView"
        },

        // ""
        // Main view
        mainView: function () {
            for (var i = 1; i < 99999; i++) {
                window.clearInterval(i);
            }

            $('#content-container').html('');
            loader = _.template($(Loader).html());
            $('#main-box').append(loader);
            $('#main-box .box-title').html('Main View');
            canvasView = new CanvasView({
                width: 980,
                height: 290,
                data_url: "app/data-section-all"
            });
            canvasView.render();

            var mainTimer = setInterval(function () {
                $('#content-container').html('');
                loader = _.template($(Loader).html());
                $('#main-box').append(loader);
                $('#main-box .box-title').html('Main View');
                canvasView = new CanvasView({
                    width: 980,
                    height: 290,
                    data_url: "app/data-section-all"
                });
                canvasView.render();
            }, 1000 * 60 * 15);
        },

        // "sensor-type/:sensortypeid"
        // Main view filtered by sensor type
        sensorTypeView: function (sensorTypeId) {
            for (var i = 1; i < 99999; i++) {
                window.clearInterval(i);
            }

            $('#content-container').html('');
            loader = _.template($(Loader).html());
            $('#main-box').append(loader);
            var sensorType = new SensorTypeModel({id: sensorTypeId});
            sensorType.fetch({
                update: true,
                success: function () {
                    $('#main-box .box-title').html(sensorType.get('desc'));
                    canvasView = new CanvasView({
                        width: 980,
                        height: 290,
                        data_url: "app/data-section-by-sensor-type/" + sensorTypeId,
                        sensor_type_id: sensorTypeId
                    });
                    canvasView.render();
                }
            });

            var mainTimer = setInterval(function () {
                $('#content-container').html('');
                loader = _.template($(Loader).html());
                $('#main-box').append(loader);
                var sensorType = new SensorTypeModel({id: sensorTypeId});
                sensorType.fetch({
                    update: true,
                    success: function () {
                        $('#main-box .box-title').html(sensorType.get('desc'));
                        canvasView = new CanvasView({
                            width: 980,
                            height: 290,
                            data_url: "app/data-section-by-sensor-type/" + sensorTypeId,
                            sensor_type_id: sensorTypeId
                        });
                        canvasView.render();
                    }
                });
            }, 1000 * 60 * 15);
        },

        defaultAction: function (actions) {
            console.log('No route:', actions);
        },

        back: function () {
            if (this.routesHit > 1) {
                window.history.back();
            } else {
                this.navigate('app/', {trigger: true, replace: true});
            }
        }
    });

    var initialize = function () {
        var appRouter = new AppRouter;
        Backbone.history.start();
    };

    return {
        initialize: initialize
    };
});