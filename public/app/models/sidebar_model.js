define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SidebarModel = Backbone.Model.extend({
        initialize: function (option) {
            this.section = option;
        },
        urlRoot: "sidebar/data"
    });
    return SidebarModel;
});