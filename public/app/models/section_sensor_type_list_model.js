define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SectionSensorTypeListModel = Backbone.Model.extend({
        initialize: function (option) {
            this.id = option.id;
            this.sensorTypeId = option.sensor_type_id;
        },
        urlRoot: "app/data-section-detail",
        url: function () {
            var url = this.urlRoot + "/" + this.id;
            if (sensorTypeId != 0) {
                url = url + "/" + sensorTypeId;
                console.log('sensorTypeListId', url);
            }
            return url;
        }
    });
    return SectionSensorTypeListModel;
});