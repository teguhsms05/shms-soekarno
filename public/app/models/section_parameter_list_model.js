define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SectionParameterListModel = Backbone.Model.extend({
        initialize: function (option) {
            this.id = option.id;
            this.parameterId = option.parameter_id;
        },
        urlRoot: "app/data-section-detail",
        url: function () {
            var url = this.urlRoot + "/" + this.id;
            if (parameterId != 0) {
                url = url + "/" + parameterId;
                console.log('parameterListID', url);
            }
            return url;
        }
    });
    return SectionParameterListModel;
});