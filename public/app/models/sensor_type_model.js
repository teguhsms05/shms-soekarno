define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SensorTypeModel = Backbone.Model.extend({
        defaults: {
            code: 'Sensor Type Code',
            desc: 'Sensor Type Desc'
        },
        urlRoot: "app/data-sensor-type"
    });
    return SensorTypeModel;
});