define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SensorModel = Backbone.Model.extend({
        defaults: {
            code: 'Section Code',
            desc: 'Section Desc',
            position_x: 10,
            position_y: 10,
            position_desc: 't',
            status: 'off'
        },
        urlRoot: "app",
        url: function () {
            var url = this.urlRoot + "/" + this.id;
            if (this.get("parameter_id")) {
                url = this.urlRoot + "/data-section-with-parameter/" + this.id + "/" + this.get("parameter_id");
            }
            return url;
        }
    });
    return SensorModel;
});