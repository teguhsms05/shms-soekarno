define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var CanvasModel = Backbone.Model.extend({
        defaults: {
            container: "canvas-bridge",
            canvas_background: "bridge.jpg",
            canvas_width: 980,
            canvas_height: 290,
            data_url: "assets/dummy/main_view_position.json",
            parameter_id: 0,
            sensor_type_id: 0
        }
    });
    return CanvasModel;
});