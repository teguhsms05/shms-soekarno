define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var SectionModel = Backbone.Model.extend({
        defaults: {
            code: 'Section Code',
            desc: 'Section Desc',
            position_x: 10,
            position_y: 10,
            position_desc: 't',
            status: 'off',
            canvas_background: 'bridge_side.jpg',
            canvas_width: 980,
            canvas_height: 290,
            sensors: []
        },
        urlRoot: "app",
        url: function () {
            var url = this.urlRoot + "/" + this.id;
            if (this.get("parameter_id")) {
                url = this.urlRoot + "/data-section-with-parameter/" + this.id + "/" + this.get("parameter_id");
            }
            if (this.get("sensor_type_id")) {
                url = this.urlRoot + "/data-section-with-sensor-type/" + this.id + "/" + this.get("sensor_type_id");
            }
            return url;
        }
    });
    return SectionModel;
});