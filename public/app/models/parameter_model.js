define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var ParameterModel = Backbone.Model.extend({
        defaults: {
            code: 'Parameter Code',
            desc: 'Parameter Desc'
        },
        urlRoot: "app/data-parameter"
    });
    return ParameterModel;
});