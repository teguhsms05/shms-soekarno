require.config({
    paths: {
        jquery: '../assets/plugins/jQuery/jQuery-2.1.4.min',
        underscore: '../assets/plugins/underscore/underscore-min',
        backbone: '../assets/plugins/backbone/backbone-min',
        adminlte: '../assets/dist/js/app.min',
        slimscroll: '../assets/plugins/slimScroll/jquery.slimscroll.min',
        konva: '../assets/plugins/konvajs/konva.min',
        konvaview: '../assets/plugins/konvajs/backbone.KonvaView',

    }
});

if (typeof jQuery === 'function') {
    define('jquery', function () {
        return jQuery;
    });
}

require([
    'app',
    'slimscroll',
    'adminlte'
], function (App) {
    App.initialize();
});